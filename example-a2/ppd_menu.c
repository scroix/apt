/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : EDIT HERE
 * Student Number   : EDIT HERE
 * Course Code      : EDIT HERE
 * Program Code     : EDIT HERE
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_menu.h"


/**
 * @file ppd_menu.c handles the initialised and management of the menu 
 * array
 **/

/**
 * @param menu the menu item array to initialise
 **/
void init_menu( struct menu_item* menu, int i)
{

switch(i)
{
	case 0 :
	strcpy(menu->name,"Display Items");
	menu->function = display_items;
	break;
	case 1 :
	strcpy(menu->name,"Purchase Items");
	menu->function = purchase_item;
	break;
	case 2 :
	strcpy(menu->name,"Save and Exit");
	menu->function = save_system;
	break;
	case 3 :
	strcpy(menu->name,"Add Item");
	menu->function = add_item;
	break;
	case 4 :
	strcpy(menu->name,"Remove Item");
	menu->function = remove_item;
	break;
	case 5 :
	strcpy(menu->name,"Display Coins");
	menu->function = display_coins;
	break;
	case 6 :
	strcpy(menu->name,"Reset Stock");
	menu->function = reset_stock;
	break;
	case 7 :
	strcpy(menu->name,"Reset Coins");
	menu->function = reset_coins;
	break;
	case 8 :
	strcpy(menu->name,"Abort Program");
	menu->function = abort_program;
	break;

}



}

int run_menu( struct menu_item menu[])
{
int i = 0, j = 0, k = 0;

printf("Main Menu:\n");
for( i = 0, j = 1; i < NUM_OF_MENUITEMS; i++, j++)
{
	if( j == 4)
	{
	printf("Administrator-Only Menu:\n");
	}
	
printf("\t %d. %s \n",j,menu[i].name);

}
printf("Select your option (1-9):");

getInteger(&k, 1, "", 1, 9);

return k - 1;


}
