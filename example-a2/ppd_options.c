/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Christopher Heuvel
 * Student Number   : s3491161
 * Course Code      : COSC1076
 * Program Code     : BP094
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_options.h"

/**
 * @file ppd_options.c this is where you need to implement the main 
 * options for your program. You may however have the actual work done
 * in functions defined elsewhere. 
 * @note if there is an error you should handle it within the function
 * and not simply return FALSE unless it is a fatal error for the 
 * task at hand. You want people to use your software, afterall, and
 * badly behaving software doesn't get used.
 **/

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this particular function should never fail.
 **/
BOOLEAN display_items(struct ppd_system * system)
{
    struct ppd_node * ptr = system->item_list->head;
	int i = 0;
	double price = 0.0;
	
	printf("\n\nDisplay Items\n\n");
	
	printf("Items Menu");
	printf("\n­­­­­­­­­­\n");
	printf("%-6s|%-20s|%-10s|%-21s\n","ID","Name","Available","Price");
	printf("­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­­\n");
	for(i = 0; i < system->item_list->count; i++)
	{
	price = (double)((ptr->data->price.dollars * 100) + (double)(ptr->data->price.cents)) / 100;
	printf("%-6s|%-20s|%-10d|$ %-21.2f\n",ptr->data->id,ptr->data->name,ptr->data->on_hand,price);
	ptr = ptr->next;
	}
	printf("\n");
    return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a purchase succeeds and false when it does not
 **/
BOOLEAN purchase_item(struct ppd_system * system)
{
    struct ppd_node * ptr = system->item_list->head;
	int i = 0;
	BOOLEAN valid = FALSE;
	char input[IDLEN];
	double price = 0.0;
	int inputmoney = 0;
	
	
	printf("\n\nPurchase Items");
    printf("\n­­­­­­­­­­\n");
	
	do
	{	
	
	 if(readStringInput(input,IDLEN, "Please enter the id of the item you wish to purchase:", "Error; invalid input") == FALSE)
	 {
		 return FALSE;
	 }
	 
	 for (i = 0; i < system->item_list->count; i++)
	 {
		 if ( strcmp(ptr->data->id,input) == 0)
		 {
			 break;
		 }
		 ptr = ptr->next;
	 }
	
	if (i == system->item_list->count)
	{
		ptr = system->item_list->head;
		printf("\nitem not found\n");
		continue;
	}
	
	if(ptr->data->on_hand == 0)
	{
		ptr = system->item_list->head;
		printf("\nSorry; product is sold out\n");
		continue;
	}
	
	valid = TRUE;
	
	} while (!valid);
	
	price = (double)((ptr->data->price.dollars * 100) + (double)(ptr->data->price.cents)) / 100;
	
	printf("\nYou have selected: %s - %s this will cost you $%.2f.\n",ptr->data->name,ptr->data->desc,price);
	
	printf("Please hand over the money – type in the value of each note/coin in cents.\n");
	
	printf("Press enter or ctrl­d on a new line to cancel this purchase:\n");
	
	valid = FALSE;
	
	while (price > 0)
	{
		printf("You still need to give us $%.2f: ",price);
		
		do
		{
			if(getInteger(&inputmoney, 4, "", 5, 1000) == FALSE)
			{
				return FALSE;
			}
			
			if(inputmoney != 5 || inputmoney != 10 ||
			  inputmoney != 20 || inputmoney != 50 ||
			  inputmoney != 100 || inputmoney != 200 ||
			  inputmoney != 500 || inputmoney != 1000)
			  {
				printf("\nError you entered %d; please enter correct denominations only aka 5,10,100,500\n",inputmoney);
				continue;
			  }
			
			valid = TRUE;
			
		} while (!valid);
	}
	
	
	
	
	
	
	return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a save succeeds and false when it does not
 **/
BOOLEAN save_system(struct ppd_system * system)
{
   
	printf("\n\nSave and Exit\n\n");
    
	return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when adding an item succeeds and false when it does not
 **/
BOOLEAN add_item(struct ppd_system * system)
{
    printf("\n\nAdd Item\n\n");
	
    return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when removing an item succeeds and false when it does not
 **/
BOOLEAN remove_item(struct ppd_system * system)
{
    printf("\n\nRemove Item\n\n");
	 
    return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_stock(struct ppd_system * system)
{
	
	printf("\n\nReset Stock\n\n");
   
    return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_coins(struct ppd_system * system)
{
	printf("\n\nReset Coins\n\n");
   
   
    return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail
 **/
BOOLEAN display_coins(struct ppd_system * system)
{
   
	printf("\n\nDisplay Coins\n\n");
	
    return FALSE;
}

BOOLEAN abort_program(struct ppd_system * system)
{
	
	printf("\n\nAbort Program\n\n");

	return TRUE;
}
