/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Summer 2006 Assignment #2 - Academic Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows - sdb@cs.rmit.edu.au
****************************************************************************/

#ifndef AMS_OPTIONS_H
#define AMS_OPTIONS_H

/* Function prototypes. */
void displaySummary(AMSType* system);
void addCourse(AMSType* system);
void addResult(AMSType* system);
void deleteCourse(AMSType* system);
void deleteResult(AMSType* system);
void courseReport(AMSType* system);
void saveData(AMSType* system, char* courseFile, char* resultFile);

#endif
