#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "container.h"

struct node {
    int data;
    struct node *left;
    struct node *right;
};

struct container {
    struct node *root;
    unsigned int size;
};

struct container *container_new ()
{
    struct container *new_container;

    new_container = malloc(sizeof(struct container));
    if (new_container == NULL) {
        perror("container_new: malloc");
        exit(EXIT_FAILURE);
    }

    new_container->root = NULL;
    new_container->size = 0;

    return new_container;
}

int container_insert (struct container *container, int data)
{
    struct node *new_node;

    struct node *parent;
    struct node *curr;

    assert(container != NULL);

    new_node = malloc(sizeof(struct node));
    if (new_node == NULL) {
        perror("container_insert: malloc");
        exit(EXIT_FAILURE);
    }

    new_node->data = data;
    new_node->left = NULL;
    new_node->right = NULL;

    /* binary tree insertion (week 9 tutorial) */

    parent = NULL;
    curr = container->root;

    while (curr != NULL) {
        parent = curr;

        if (data < curr->data)
            curr = curr->left;
        else /* data >= curr->data */
            curr = curr->right;
    }

    if (parent == NULL)
        container->root = new_node;
    else if (data < parent->data)
        parent->left = new_node;
    else /* data >= parent->data */
        parent->right = new_node;

    container->size++;

    return 0;
}

static void node_print (struct node *node)
{
    if (node == NULL)
        return;

    node_print(node->left);

    printf("%d ", node->data);

    node_print(node->right);
}

void container_print (struct container *container)
{
    assert(container != NULL);

    node_print(container->root);

    printf("\n");
}

static void node_free (struct node *node)
{
    if (node == NULL)
        return;

    node_free(node->left);

    node_free(node->right);

    free(node);
}

void container_free (struct container *container)
{
    assert(container != NULL);

    node_free(container->root);

    free(container);
}

unsigned int container_size (struct container *container)
{
    assert(container != NULL);

    return container->size;
}

