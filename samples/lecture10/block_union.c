#include <stdlib.h>
#include <stdio.h>
#include <string.h>

enum block_type {
    ptr_type,
    text_type,
    ntypes
};

#define NPTRS 3
#define NBYTES 12 /* sizeof(*) * NPTRS (assuming 32bit arch) */

struct block {
    enum block_type type;
    union {
        struct block *ptr[NPTRS];
        char text[NBYTES];
    } data;
};

int main (/*int argc, char **argv*/)
{
    struct block block_ptr;
    struct block block_text;

    block_ptr.type = ptr_type;
    block_ptr.data.ptr[0] = NULL;
    block_ptr.data.ptr[1] = NULL;
    block_ptr.data.ptr[2] = NULL;

    block_text.type = text_type;
    strcpy(block_text.data.text, "oahai");

    /*
    struct block block_ptr = {
        .type = ptr_type,
        .data.ptr = {0}
    };

    struct block block_text = {
        .type = text_type,
        .data.text = {0}
    };
    */

    return EXIT_SUCCESS;
}
