#include <stdlib.h>
#include <stdio.h>

#include "container.h"

int main (int argc, char **argv)
{
    struct container *container;
    int i;

    if (argc < 2) {
        fprintf(stderr, "usage: %s <sequence of integers ...>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    container = container_new();

    for (i = 1; i < argc; i++)
        container_insert(container, atoi(argv[i]));

    container_print(container);

    container_free(container);

    return EXIT_SUCCESS;
}
