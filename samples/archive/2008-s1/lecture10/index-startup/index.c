/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 24/1/2007
* 
* index.c
* Practical example of constructing a simple index using a linked list.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 80
#define WORD_LEN 20
#define SUCCESS 1
#define FAILURE 0
#define FILENAME "data.txt"
#define DELIM " \n\r`1234567890-=[]\\;',./~!@#$%^&*()_+{}:\"<>?"

typedef struct wordStruct
{
   char word[WORD_LEN + 1];
   struct wordStruct* next;
   unsigned count;
} WordType;

typedef struct indexStruct
{
   WordType* head;
   unsigned totalSize;
   unsigned uniqueWords;
} IndexType;

void initIndex(IndexType* index);
int loadIndex(IndexType* index, char* filename);
int addWord(IndexType* index, char* word);
void displayIndex(IndexType* index);

int main(void)
{
   IndexType index;
  
   initIndex(&index);
 
   if (loadIndex(&index, FILENAME) == FAILURE)
   {
      return EXIT_FAILURE;
   }
   
   displayIndex(&index);
   
   return EXIT_SUCCESS;
}

void initIndex(IndexType* index) 
{
   index->head = NULL;
   index->totalSize = 0;
   index->uniqueWords = 0;
}

int loadIndex(IndexType* index, char* filename)
{
   FILE* fp;
   char line[LINE_LEN + 1];
   char* tok;

   if ((fp = fopen(filename, "r")) == NULL)
   {
      fprintf(stderr, "Could not open '%s'.\n", filename);
      return FAILURE;  
   } 

   while (fgets(line, LINE_LEN + 1, fp) != NULL)
   {
      tok = strtok(line, DELIM); 

      while (tok != NULL)
      {
         addWord(index, tok);        
         tok = strtok(NULL, DELIM);
      }
   }

   fclose(fp);

   return SUCCESS;
}

int addWord(IndexType* index, char* word)
{
   /*printf("Current word is '%s'.\n", word);*/
   WordType *new, *current;

   current = index->head;

   while ((current != NULL) && (strcmp(current->word, word) != 0))
   {
      current = current->next;
   }

   if (current != NULL)
   {
      current->count++;
   }
   else
   {
      if ((new = malloc(sizeof(WordType))) == NULL)
      {
         fprintf(stderr, "Could not allocate %d bytes.\n",
                 sizeof(WordType));
         exit(EXIT_FAILURE); 
      }

      strcpy(new->word, word);
      new->count = 1;

      new->next = index->head;
      index->head = new;

      index->uniqueWords++;
   }

   index->totalSize++;

   return SUCCESS;
}

void displayIndex(IndexType* index)
{
   WordType* current = index->head;

   while (current != NULL)
   {
      printf("Word: %-20s Count: %2d\n", current->word,
             current->count);
      current = current->next;
   }

   printf("Total words:  %3d\n", index->totalSize);
   printf("Unique words: %3d\n", index->uniqueWords);
}
