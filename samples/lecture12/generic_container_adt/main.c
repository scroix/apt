#include <stdlib.h>
#include <stdio.h>

#include "container.h"
#include "person.h"

int main()
{
    struct container *container;
    struct person *person;

    container = container_new(person_cmp, person_free, person_print);

    person = person_new("Chris", 42);

    printf("Inserting: "); person_print(person);

    container_insert(container, person);

    person = person_new("Alice", 24);

    printf("Inserting: "); person_print(person);

    container_insert(container, person);

    person = person_new("Bob", 44);

    printf("Inserting: "); person_print(person);

    container_insert(container, person);

    person = person_new("Guam", 22);

    printf("Inserting: "); person_print(person);

    container_insert(container, person);

    printf("\nPrinting\n");

    container_print(container);

    printf("\n"); /* pretty print */

    printf("Deleting: "); person_print(person);

    container_delete(container, person); /* delete {Guam, 22} */

    printf("\nPrinting\n");

    container_print(container);

    container_free(container);

    return EXIT_SUCCESS;
}
