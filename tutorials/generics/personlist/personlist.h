#include "list.h"
#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#define LINELEN 80
#define EXTRACHARS 2
#define NAMELEN 20
struct person 
{
    char fname[NAMELEN+1];
    char lname[NAMELEN+1];
    unsigned age;
};
struct person* make_person(char *, char *, unsigned);
void read_rest_of_line(void);
void print_list(struct list * list);
/* int person_cmp(const struct person*, const struct person*); */
int person_cmp(const void * data, const void * data);
