/* Computing the distance of a marathon in kilometres */
#include <stdio.h>

int main(int argv, char* argc[])
{
    int miles, yards;
    float kilometres;

    yards = 26;
    miles = 385;

    kilometres = 1.609 * (miles + yards / 1760);

    printf("%d miles, %d yards = %f kilometres\n", miles, yards, kilometres);

    return 0;
}
