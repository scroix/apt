/*
 * an example showing problems with using scanf
 */
#include <stdio.h>

int main(void)
{
  char str[80];
  printf("Enter a string:");
  scanf("%s", str);
  printf("Here is your string: %s\n", str);
  return 0; 
}

