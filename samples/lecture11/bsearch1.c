#include <stdio.h>
#include <stdlib.h>

void print_array(int *array, size_t n)
{
    size_t i;

    for(i = 0; i < n; i++)
        printf("%d ", array[i]);

    printf("\n");
}

int integer_compare(const void *vptr1, const void *vptr2)
{
    const int a = *((int*)vptr1);
    const int b = *((int*)vptr2);

    return a - b;
}

int main(int argc, char **argv)
{
#define N 10
    int array[N] = { 5, 4, 9, 2, 1, 6, 6, 8, 9, 1 };
    int target;
    int *ptr;

    if (argc != 2) {
        fprintf(stderr, "usage: %s <target integer>\n", argv[0]);
        return EXIT_FAILURE;
    }

    target = atoi(argv[1]);

    print_array(array, N);

    qsort(array, N, sizeof(int), integer_compare);

    print_array(array, N);

    ptr = bsearch(&target, array, N, sizeof(int), integer_compare);

    if (ptr)
        printf("Found %d.\n", *ptr);
    else
        printf("No match found.\n");

    return EXIT_SUCCESS;
}
