/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 10/1/2006
* 
* matrix.c
* Two-dimensional array example.
* Citation: Adapted from slide 4-15 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define ROWS 3
#define COLS 4

int main(void)
{
   int matrix[ROWS][COLS] = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
   int *cellA, *cellB;
   int (*rowA)[COLS], (*rowB)[COLS];
   int i, j;

   cellA = &matrix[1][2];
   cellB = cellA + 3;

   rowA = &matrix[1];
   rowB = rowA + 1;

   (*rowB)[2] = *cellA;
   
   /* Check answers. */
   printf("cellA: %d\n", *cellA);
   printf("cellB: %d\n", *cellB);
   printf("rowA : %d\n", **rowA);
   printf("rowB : %d\n", **rowB);
   printf("other: %d\n", (*rowB)[2]);
   printf("contents of 2d array: ");
   for (i = 0; i < ROWS; i++)
   {
      for (j = 0; j < COLS; j++)
      {
         printf("%d ", matrix[i][j]);
      }
   }
   printf("\n");
   
   printf("**matrix: %d\n", **matrix);
   printf("**(matrix+1): %d\n", **(matrix+1)); 
   printf("*matrix[1]: %d\n", *matrix[1]); 
   return EXIT_SUCCESS;
}
