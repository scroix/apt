/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Julien de-Sainte-Croix
 * Student Number   : s3669242
 * Course Code      : COSC1076
 * Program Code     : BP096
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_utility.h"
/**
 * @file ppd_utility.c contains implementations of important functions for
 * the system. If you are not sure where to implement a new function, 
 * here is probably a good spot.
 **/

void read_rest_of_line(void)
{
    int ch;
    /* keep retrieving characters from stdin until we
     * are at the end of the buffer
     */
    while(ch = getc(stdin), ch!='\n' && ch != EOF)
        ;
    /* reset error flags on stdin */
    clearerr(stdin);
}

/**
 * @param system a pointer to a @ref ppd_system struct that contains
 * all the data in the system we are manipulating
 * @param coins_name the name of the coins file
 * @param stock name the name of the stock file
 **/
BOOLEAN load_data(struct ppd_system * system , const char * coins_name, 
const char * stock_name)
{
    FILE * fpCoins;
    FILE * fpStock;
    
    /* set our pointers to the files */
    if((fpCoins = fopen(coins_name, "r")) == NULL)
    {
        perror("failed to open file");
        exit(EXIT_FAILURE);
    }
    if((fpStock = fopen(stock_name, "r")) == NULL)
    {
        perror("failed to open file");
        exit(EXIT_FAILURE);
    }

    /* load the files and populate lists */
    if(!load_stock(system, fpStock))
        return FALSE;
    if(!load_coins(system, fpCoins))
        return FALSE;

    sort_list(system);

    /* close the files */
    fclose(fpStock);
    fclose(fpCoins);
    
    return TRUE;
}

BOOLEAN load_stock(struct ppd_system * system, FILE * fpStock)
{ 
    char line[STOCKWIDTH+EXTRASPACES], linecpy[STOCKWIDTH+EXTRASPACES];
    char id[IDLEN + EXTRACHAR];
    char name[NAMELEN + EXTRACHAR];
    char desc[DESCLEN + EXTRACHAR];
    unsigned entrycount = 0;
    unsigned dollars, cents, onhand;
    char * token, * pEnd; 
    StockNode *curr, *new;

    curr = NULL;
    new = NULL;

    /*read in each line from the file, ensuring there is space to store
     * STOCK */
    while(fgets(line, STOCKWIDTH+EXTRASPACES, fpStock) != NULL &&
        entrycount < STOCKDIRSIZE)
    {
        /* keep a count of the current token */
        stock_numtok curtoken=0;

        /* remove the newline from the line read in so it does not get
         * stored in the last token
         */
        line[strlen(line)-EXTRACHAR]='\0';
        /* make a copy of the line in case we need to use it in error
         * messages
         */
        strcpy(linecpy,line);

        /* start string tokenization */
        token = strtok(line, stock_delim);
        while(token != NULL)
        {
            /* check which token we are up to and copy the data into the
             * appropriate string
             */
            switch(curtoken)
            {
                case ID:
                    strcpy(id, token);
                    /* ensure correct amount of characters for entry */
                    if (strlen(id) != IDLEN) {
                        fprintf(stderr, "Error: the id token is not valid\n");
                        return FALSE;
                    }
                    break;
                case NAME:
                    strcpy(name, token);
                    if (strlen(name) >= NAMELEN) {
                        fprintf(stderr, "Error: the name token is not valid\n");
                        return FALSE;
                    }
                    break;
                case DESC:
                    strcpy(desc, token);
                    if (strlen(desc) >= DESCLEN) {
                        fprintf(stderr, "Error: the desc token is not valid\n");
                        return FALSE;
                    }
                    break;
                case DOLLARS:
                    /* check for string to long conversion error */
                    if( !(dollars = strtol(token, &pEnd, 10)) && (strcmp(token, "0") != 0 )) {
                        fprintf(stderr, "Error: the dollar token is not valid\n");
                        return FALSE;
                    }
                    break;
                case CENTS:
                    if( !(cents = strtol(token, &pEnd, 10)) && (strcmp(token, "0") != 0 )){
                        fprintf(stderr, "Error: the cents token is not valid\n");
                        return FALSE;
                    }
                    break;
                case ONHAND:
                    if( !(onhand = strtol(token, &pEnd, 10)) && (strcmp(token, "0") != 0 )){
                        fprintf(stderr, "Error: the onhand token is not valid\n");
                        return FALSE;
                    }
                    break;
                default:
                    /* if there are too many tokens, display an error */
                    fprintf(stderr, "invalid line in file: %s\n", linecpy);
                    return EOF;
            }
            /* get the next token */
            token = strtok(NULL, stock_delim);
            ++curtoken;
        } 

        /* allocate memory for new node */
        if ((new = malloc(sizeof(StockNode))) == NULL)
        {
            fprintf(stderr, "Could not allocate %lu bytes.\n", sizeof(StockNode));
            return 0;
        }

        if ((new->data = malloc(sizeof(StockData))) == NULL)
        {
            fprintf(stderr, "Could not allocate %lu bytes.\n", sizeof(StockData));
            return 0;
        }
        
        /* copy contents of tokenization to new node */
        strcpy(new->data->id, id);
        strcpy(new->data->name, name);
        strcpy(new->data->desc, desc);
        new->data->price.dollars = dollars;
        new->data->price.cents = cents;
        new->data->on_hand = onhand;
        new->next = NULL;
        
        
        /* If this is the first node assign the head to the new node and make 
         * this node the working node. */
        if (curr == NULL)
        {
            curr = new;
            system->item_list->head = new;
        /* We have already encountered an item before here so now assign the next node
         * of current to the new node and move the pointer of current to point to the 
         * newly created node. */    
        } else {
            curr->next = new;
            curr = curr->next;
        }

        system->item_list->count++;
    }
    return TRUE;
}

BOOLEAN load_coins(struct ppd_system * system, FILE * fpCoins)
{
    char line[COINSWIDTH+EXTRASPACES], linecpy[COINSWIDTH+EXTRASPACES];
    unsigned count;
    char * token; 
    unsigned entrycount = 0;

    while(fgets(line, COINSWIDTH+EXTRASPACES, fpCoins) 
                            != NULL && entrycount < COINSDIRSIZE)
    {
        /* keep count of the current token */
        coins_numtok curtok = 0;

        /* remove the newline from the line read in so it does not get
         * stored in the last token
         */
        line[strlen(line)-EXTRACHAR]='\0';
        /* make a copy of the line in case we need to use it in error
         * messages
         */
        strcpy(linecpy,line);

        /* start string tokenization */
        token = strtok(line, coins_delim);
        while(token != NULL)
        {
            /* check which token we are up to and copy the data into the
             * appropriate string
             */
            switch(curtok)
            {
                case DENOM:
                    system->cash_register[entrycount].denom = entrycount;
                    break;
                case COUNT:
                    count = (unsigned) strtol(token, NULL, 10);
                    break;
                default:
                /* if there are too many tokens, display an error */
                fprintf(stderr, "invalid line in this file %s\n", linecpy);
                return EOF;
            }
            /* get the next token */
            token = strtok(NULL, coins_delim);
            ++curtok;
        }

        system->cash_register[entrycount].count = count;
        entrycount++;
    }

    return TRUE;
}

BOOLEAN sort_list(struct ppd_system * system)
{
    int sorted = 1;
    StockNode *curr, *prev;

    curr = system->item_list->head;
    do {

        /* identify nodes to swap */
        if(curr->next != NULL)
        {
            prev = curr;
            curr = curr->next;

            if(prev->data->name[0] > curr->data->name[0]) {
                node_swap(prev, curr);
            }
        }

        /* check if list is sorted */
        if(!check_sort(system)) {
            sorted = 0;
        }
        else
        {
            sorted = 1;
        }

        /* reset pointers if we've reached the end of the list */
        if (curr->next == NULL) {
            curr = system->item_list->head;
        }

    } while(!sorted);

    return FALSE;
}

void node_swap(StockNode *a, StockNode *b)
{
    void *temp = a->data;
    a->data = b->data;
    b->data = temp;
}

BOOLEAN check_sort(struct ppd_system * system)
{
    int count = 0;
    StockNode *curr, *prev;

    curr = system->item_list->head;
    while(curr->next != NULL)
    {
        prev = curr;
        curr = curr->next;

        if(prev->data->name[0] > curr->data->name[0]) {
            count++;
        }
    }

    if(count > 0) {
        return FALSE;
    }

    return TRUE;

}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
BOOLEAN system_init(struct ppd_system * system)
{      
    int i;

    for (i = 0; i < NUM_DENOMS; i++)
    {
        system->cash_register[i].denom = 0;
        system->cash_register[i].count = 0;
    }

    if(!(system->item_list = malloc(sizeof(StockList)))) {
        return FALSE;
    }
    system->item_list->head = NULL;
    system->item_list->count = 0;

    return TRUE;
}

/**
 * @param system a pointer to a @ref ppd_system struct that holds all 
 * the data for the system we are creating
 **/
void system_free(struct ppd_system * system)
{
    StockNode * curr, * next;

    assert(system->item_list != NULL);

    curr = system->item_list->head;

    /* Navigate through the link list freeing memory */
    while (curr != NULL) {
        next = curr->next;
        free(curr->data);
        free(curr);
        curr = next;
    }
    
    free(system->item_list);

}

/*******************************
* Original code adapted from:
*
* Steven Burrows
* sdb@cs.rmit.edu.au
* getString-basic.c
********************************/
int getString(char* string, unsigned length, char* prompt)
{
   int finished = FALSE;
   char tempString[STRING_TEMP_LENGTH + EXTRASPACES];

   /* Continue to interact with the user until the input is valid. */
   do
   {
       /* Provide a custom prompt. */
       printf("%s", prompt);

       /* Accept input. "+2" is for the \n and \0 characters. */
       if (fgets(tempString, length + EXTRASPACES, stdin) == NULL) {
           return RTM;
       }

       /* A string that doesn't have a newline character is too long */
       if (tempString[0] == '\n')
       {
           return RTM;
       }
       else if (tempString[strlen(tempString) - EXTRACHAR] != '\n')
       {
           printf("Input was too long.\n");
           read_rest_of_line();
       }
       else
       {
           finished = TRUE;
       }

   } while (finished == FALSE);

   /* Overwrite the \n character with \0. */
   tempString[strlen(tempString) - EXTRACHAR] = '\0';

   /* Make the result string available to calling function. */
   strcpy(string, tempString);

   return SUCCESS;
}

/**********************************
* Original code adapted from:
*
* Steven Burrows
* sdb@cs.rmit.edu.au
* getInteger-basic.c
***********************************/
int getInteger(int* integer, unsigned length, char* prompt, int min, int max)
{
    int finished = FALSE;
    char tempString[INT_TEMP_LENGTH + EXTRASPACES];
    int tempInteger = 0;
    char* endPtr;

    /* Continue to interact with the user until the input is valid. */
    do
    {
        /* Provide a custom prompt. */
        printf("%s", prompt);

        /* Accept input. "+2" is for the \n and \0 charaters. */
        if(fgets(tempString, length + EXTRASPACES, stdin) == NULL) {
            return RTM;
        }

        /* A string that doesn't have a newline character is too long. */
        if (tempString[0] == '\n')
        {
            return RTM;
        }
        else if (tempString[strlen(tempString) - EXTRACHAR] != '\n')
        {
            printf("Input was too long.\n");
            read_rest_of_line();
        }
        else
        {
            /* Overwrite the \n character with \0. */
            tempString[strlen(tempString) - EXTRACHAR] = '\0';

            /* Convert string to an integer. */
            tempInteger = (int) strtol(tempString, &endPtr, 10);

            /* Validate integer result. */
            if (strcmp(endPtr, "") != 0)
            {
                printf("Input was not numeric.\n");
            }
            else if (tempInteger < min || tempInteger > max)
            {
                printf("Input was not within range %d - %d.\n", min, max);
            }
            else
            {
                finished = TRUE;
            }
        }
    } while (finished == FALSE);

    /* Make the result integer available to calling function. */
    *integer = tempInteger;

    return SUCCESS;
}
