/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 24/1/2007
* 
* index.c
* Practical example of constructing a simple index using a linked list.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 80
#define WORD_LEN 20
#define SUCCESS 1
#define FAILURE 0
#define FILENAME "data.txt"
#define DELIM " \n\r"

typedef struct wordStruct
{
   /* What goes here? */
} WordType;

typedef struct indexStruct
{
   /* What goes here? */
} IndexType;

void initIndex(IndexType* index);
int loadIndex(IndexType* index, char* filename);
int addWord(IndexType* index, char* word);
void displayIndex(IndexType* index);

int main(void)
{
   IndexType index;
  
   initIndex(&index);
 
   if (loadIndex(&index, FILENAME) == FAILURE)
   {
      return EXIT_FAILURE;
   }
   
   displayIndex(&index);
   
   return EXIT_SUCCESS;
}

void initIndex(IndexType* index) 
{
   /* What goes here? */
}

int loadIndex(IndexType* index, char* filename)
{
   /* What goes here? */
}

int addWord(IndexType* index, char* word)
{
   /* What goes here? */
}

void displayIndex(IndexType* index)
{
   /* What goes here? */
}
