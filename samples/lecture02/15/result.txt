
Line      Assignment Scores    Exam      Total
Number      1          2       Score     Score
======    =================    ======    =====
     1        70        75         80    76.50     D <- good score!
     2        50        60         70    63.00     C <- ok score!
     3        90        95        100    96.50     H <- good score!
     4        30        50         70    56.00     P <- ok score!
     5        10        30         50    36.00     N <- bad score!
          ================    =======   ======
Averages:  50.00     62.00      74.00    65.60    Total no. of students = 5
