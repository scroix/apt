/* Modified example from slide 3-9. */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   float table[5];
   int i = 1, j = 2;

   table[2] = 3.24;
   table[i] = table[2] + 1;
   table[i+j] = 18.0;
   table[--i] = table[j] - 2;
   
   /* These lines violate the array boundaries. */
   table[5] = 12.2;
   table[-1] = 0.78;
   for (i = -1; i < 6; i++)
   {
      printf("table[%d] = %f\n", i, table[i]);
   }
   
   return EXIT_SUCCESS;
}
