#include <stdio.h>

char *strcpy(char *, const char *);

int main(void)
{
  char s1[10]; /* make sure its size is larger than s2 */
  char s2[]="abcde";

  strcpy(s1, s2);
  printf("s1 is %s\n", s1);
  return 0;
}

char *strcpy(char *s1, const char *s2)
{
   char *p = s1;

   while (*p++ = *s2++)
      ;
   return s1;
}
