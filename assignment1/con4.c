/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include "con4.h"

/**
 * @file con4.c contains the main function which is the entry point into the 
 * application and manages the main memory.
 **/

int main(void)
{
    /* the scoreboard for keeping track of winners */
    scoreboard scores;
    /* the two players and a pointer to who won the last game */
    struct player human_player, computer_player, *winner;
    /* variables related to menu */
    int finished = FALSE;
    int choice = 0;
    char prompt[PROMPT_LENGTH + 1];
    int min = 1, max = 3;

    /* initialise the scoreboard */
    init_scoreboard(scores);

    /*display menu and get menu choice until the user chooses to quit */
    do
    {   
        system("clear");

        printf("Welcome to Connect 4\n");
        printf("--------------------\n");
        printf("1. Play Game\n");
        printf("2. Display High Scores\n");
        printf("3. Quit\n");
        printf("--------------------\n");
        sprintf(prompt, "Your choice... ");
        
        /* utilise min/max length with fgets to input validation */
        getInteger(&choice, INT_INPUT_LENGTH, prompt, min, max);
        
        switch(choice) {
        case 1 : /* PLAY THE GAME */
            /* prevent do/while continueing if player exits prematurely with RTM */
            if(get_human_player(&human_player) == FAILURE){
                break;
            }
            get_computer_player(&computer_player);
            
            /* play game, retrieve winner */
            winner = play_game(&human_player, &computer_player);
            
            /* if there was not a draw, add player to the scoreboard in 
             * order by number of tokens in play
             */
            if (winner != NULL) {
                 add_to_scoreboard(scores, winner);
            }
            break;
        case 2 :
            /* display the scoreboard option */
            display_scores(scores);
            break;
        case 3 :
            /* quit the program option */
            printf("\nGoodbye...");
            finished = TRUE;
            break;
        default :
            printf("You've selected and invalid option, please try again.\n");
        }
    }  while (!finished);

    return EXIT_SUCCESS;
}
