/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 11/1/2006
* 
* a2.c
* Summer 2005 Exam Question A2 (25 marks)
*****************************************************************************
* Write a program to:
*
* Get the filename (source_file) from the user using command line arguments
* and then read the contents of source_file and print every second character
* of every line in reverse to a new file target_file. For example, if the
* content of source_file is
* 
* Hello there
* I am writing my
* Exam today
* 
* Then the content of the target_file should be
* 
* rh le
* mgiiwm
* ao aE
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LEN 80

/* Assumption: We are not using command line arguments this time. */
int main(void)
{
   char* source_file = "data.txt";
   char* target_file = "output.txt";  
   FILE* fpIn;
   FILE* fpOut;
   char line[MAX_LINE_LEN + 1];
   int i;

   fpIn = fopen(source_file, "r");
   if (fpIn == NULL)
   {
      fprintf(stderr, "Could not open file '%s'.\n", source_file);
      return EXIT_FAILURE; 
   }

   if ((fpOut = fopen(target_file, "w")) == NULL)
   {
      fprintf(stderr, "Could not open file '%s'.\n", target_file);
      return EXIT_FAILURE; 
   }

   while (fgets(line, MAX_LINE_LEN + 1, fpIn) != NULL)
   {
      for (i = strlen(line) - 3; i >= 0; i-=2)
      { 
         /*fputc(line[i], fpOut);*/
         fprintf(fpOut, "%c", line[i]);
      }
      fprintf(fpOut, "\n");
   } 
 
   fclose(fpIn);
   fclose(fpOut);
  
   return EXIT_SUCCESS;
}
