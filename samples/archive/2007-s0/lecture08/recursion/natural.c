/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 17/1/2006
* 
* natural.c
* Example of recursion that computes the sum of natural numbers.
* Citation: Adapted from slide 8-14 of PP2A/PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int natSumRecursion(int num);
int natSumIteration(int num);

int main(void)
{
   printf("natSumRecursion(1000000) = %d\n", natSumRecursion(1000000));

/*   printf("natSumIteration(10000000) = %d\n", natSumIteration(10000000));
*/
   return EXIT_SUCCESS;
}

int natSumRecursion(int num)
{
   if (num == 1)
   {
      return 1;
   }
   else
   {
      return (num + natSumRecursion(num - 1));
   }
}

int natSumIteration(int num)
{
   int i;
   int sum = 0;

   for (i = 1; i <= num; i++)
   {
      sum += i; 
   } 

   return sum;
}
