/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 11/1/2006
* 
* strtok.c
* Minimal example of how to use the strtok() function.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
   char myArray[] = "one:two:three";
   char *tok1, *tok2, *tok3;

   tok1 = strtok(myArray, ":");
   tok2 = strtok(NULL, ":");
   tok3 = strtok(NULL, "\0");

   printf("%s\n%s\n%s\n", tok1, tok2, tok3);

   return EXIT_SUCCESS;
}
