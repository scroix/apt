/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 25/1/2006
* 
* index.c
* Practical example of constructing a simple index using a linked list.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 80
#define WORD_LEN 20
#define SUCCESS 1
#define FAILURE 0
#define FILENAME "data.txt"
#define DELIM " \n\r"
#define PAGE "<PAGE>"

typedef struct pageStruct 
{
   unsigned count;
   unsigned pageNum;
   struct pageStruct* nextPage;
} PageType;

typedef struct wordStruct
{
   unsigned count;
   char word[WORD_LEN + 1];
   struct wordStruct* next; 
   struct pageStruct* headPage;
} WordType;

typedef struct indexStruct
{
   unsigned distinctWords;
   unsigned totalWords;
   struct wordStruct* head; 
} IndexType;

int loadIndex(IndexType* index, char* filename);
void displayIndex(IndexType* index);
int addWord(IndexType* index, char* word, unsigned pageNum);
int addPage(WordType* wordPtr, unsigned pageNum);

int main(void)
{
   IndexType index;

   index.head = NULL;
   index.totalWords = 0;
   index.distinctWords = 0;
   
   if (loadIndex(&index, FILENAME) == FAILURE)
   {
      return EXIT_FAILURE;
   }
   
   displayIndex(&index);
   
   return EXIT_SUCCESS;
}

int loadIndex(IndexType* index, char* filename)
{
   FILE* fp;
   char line[LINE_LEN + 1];
   char* tok;
   unsigned pageNum = 1;

   if ((fp = fopen(filename, "r")) == NULL)
   {
      fprintf(stderr, "Could not open file %s.\n", filename);
      return FAILURE;
   }    

   while (fgets(line, LINE_LEN + 1, fp) != NULL)
   {
      tok = strtok(line, DELIM);

      while (tok != NULL)
      {
         if (strcmp(PAGE, tok) == 0)
         {
            pageNum++;
         }
         else
         {
            addWord(index, tok, pageNum); 
         }
         tok = strtok(NULL, DELIM);
      }
   } 

   fclose(fp);

   return SUCCESS;
}

int addWord(IndexType* index, char* word, unsigned pageNum)
{
   WordType *curr, *prev = NULL, *new = NULL;

   curr = index->head;

   while (curr != NULL && strcmp(curr->word, word) != 0)
   {
      prev = curr;
      curr = curr->next; 
   }

   index->totalWords++; 

   if (curr != NULL)
   {
      curr->count++;
      /* *** Add new page for current word. *** */
      addPage(curr, pageNum);
   }
   else
   {
      if ((new = malloc(sizeof(WordType))) == NULL)
      {
         fprintf(stderr, "Could not allocate %d bytes of memory.\n",
                 sizeof(WordType));
         return FAILURE; 
      }  

      new->next = NULL;
      strcpy(new->word, word);
      new->count = 1;

      if (prev == NULL)
      {
         index->head = new;
      } 
      else
      {
         prev->next = new;
      }
      
      /* *** Add new page for new word. *** */
      addPage(new, pageNum);
   }

   return SUCCESS;
}

int addPage(WordType* wordPtr, unsigned pageNum)
{
   PageType *currPage = wordPtr->headPage;
   PageType *prevPage = NULL;
   PageType *new = NULL;

   while (currPage != NULL)
   {
      prevPage = currPage;
      currPage = currPage->nextPage; 
   }   

   if (prevPage == NULL)
   {
      if ((new = malloc(sizeof(PageType))) == NULL)
      {
         fprintf(stderr, "Could not allocate %d bytes of memory.\n",
                 sizeof(PageType));
         return FAILURE; 
      }
      new->nextPage = NULL;
      new->pageNum = pageNum;   
      new->count = 1;

      wordPtr->headPage = new;
   }
   else if (pageNum > prevPage->pageNum)
   {
      if ((new = malloc(sizeof(PageType))) == NULL)
      {
         fprintf(stderr, "Could not allocate %d bytes of memory.\n",
                 sizeof(PageType));
         return FAILURE; 
      }
      new->nextPage = NULL;
      new->pageNum = pageNum;   
      new->count = 1;

      prevPage->nextPage = new;
   }
   else
   {
      prevPage->count++;
   }
 
   return SUCCESS;
}

void displayIndex(IndexType* index)
{
   WordType* curr = index->head;
   PageType* currPage = NULL;

   while (curr != NULL)
   {
      printf("Count: %2d.  Word: %-*s\n   -> Pages: ", 
              curr->count, WORD_LEN, curr->word); 
     
      currPage = curr->headPage; 
      while (currPage != NULL)
      {
         printf("%d (%d) ", 
                 currPage->pageNum, currPage->count);
         currPage = currPage->nextPage;  
      }
      putchar('\n'); 
      curr = curr->next;
   } 
}

