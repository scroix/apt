/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 2/1/2006
* 
* sideEffects.c
* Experimenting with multiple assignments using the same variables.
* Citation: Adapted from slide 1-32 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
 
int main(void)
{
   int s, x, y;
    
   s = 1;
   y = 2;
   x = (y = 6) - (s = 3);        /*  OK     */
   printf("result 1: %d\n", x);
   
   s = 1;
   y = 2;
   x = (y *= y) - (s -= y);      /* yikes!  */
   printf("result 2: %d\n", x);
   
   s = 1;
   y = 2;
   x = y++ + ++y;                /* legal but yikes2 !!  */
   printf("result 3: %d\n", x);

   return EXIT_SUCCESS;
}
