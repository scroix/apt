#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int test_chars(char *, int (*)(int));

int main()
{
    char *string = "ab1cdef";

    if (test_chars(string, isalpha))
        printf("isalpha -- yes\n");
    else
        printf("isalpha -- no\n");

    if (test_chars(string, isalnum))
        printf("isalnum -- yes\n");
    else
        printf("isalnum -- no\n");

    if (test_chars(string, isprint))
        printf("isprint -- yes\n");
    else
        printf("isprint -- no\n");

    return EXIT_SUCCESS;
}

int test_chars(char *s, int (*fn)(int))
{
    while (*s) {
        if (fn(*s) == 0)
            return 0;

        s++;
    }

    return 1;
}
