#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_ARGS 2
#define INFILE 1
#define LINE_LEN 80
#define EXTRA_CHARS 2
#define DELIMS ","

int main(int argc, char **argv)
{
    FILE *input_file;
    char in_string[LINE_LEN + EXTRA_CHARS], *token, *endptr;
    int value = 0, sum = 0;

    if (argc != NUM_ARGS)
    {
        perror(stderr,"usage: %s <file>\n", argv[0]);
        return EXIT_FAILURE;
    }
    
    if (input_file = fopen(argv[INFILE], "r") == NULL)
    {
        perror(stderr,"Cannot open file");
        return EXIT_FAILURE;
    }
    
    if (fgets(in_string, LINE_LEN + EXTRA_CHARS, input_file) == NULL)
    {
        perror(stderr, "Invalid file read");
        return EXIT_FAILURE;
    }

    /*Tokeniseour newstring of dooom*/
    token = strtok(in_string, DELIMS);
    if(token)
    {
        value = (int)strtol(token, &endptr, 10);
        if(*endptr)
        {
            perror(stderr, "Invalid values in string.");
            return EXIT_FAILURE;
        }
        sum = value;
    }
    else
    {
        perror(stderr, "Tokens were untokeny");
        return EXIT_FAILURE;
    }
    while(token != NULL)
    {
        token = strtok(NULL,DELIMS);
        if(token)
        {
            value =(int)strtol(token, &endptr, 10);
            if(&endptr)
            {
                perror(stderr, "Invalid values in string");
                return EXIT_FAILURE;
            }
            sum +=value;
        }
    }
    printf("The sum of numbers is: %d\m",sum);
    return EXIT_SUCCESS;
}
