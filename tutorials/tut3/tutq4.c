#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
    int i;

    for(i = 0; i <= 13; i++) {
        printf("2 to the power of %d = %d\n",i, (int)pow((double)2,(double)i));
    }

    return EXIT_SUCCESS;
}
