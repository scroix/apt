/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 20/1/2006
* 
* union.c
* Example of a union.
* Citation: Adapted from slide 10-17 of PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
   int numCylinders;
   int capacity;
} CarType;

typedef struct
{
   int maxPass;
   char class[20];
} BusType;

typedef union  {
   CarType car;
   BusType bus;
} BusOrCar;

typedef struct  {
   char manufacturer[30];
   int engineNum;
   char vehicleCode; /* 'C'-car, 'B'-bus  */
   BusOrCar other;
} VehicleType;

int main(void)
{
   VehicleType v1;

   /* Initialise the vehicle as a car. */
   strcpy(v1.manufacturer, "Holden");
   v1.engineNum = 12345;
   v1.vehicleCode = 'C';
   v1.other.car.numCylinders = 6;
   v1.other.car.capacity = 2800;

   /* Overwrite the vehicle as a bus. */
   strcpy(v1.manufacturer, "Volvo");
   v1.engineNum = 98765;
   v1.vehicleCode = 'B';
   v1.other.bus.maxPass = 48;
   strcpy(v1.other.bus.class, "Commercial");

   /* Print the variables stored in the shared memory. */
   printf("car.numCylinders: %d\n", v1.other.car.numCylinders);
   printf("car.capacity:     %d\n", v1.other.car.capacity);
   printf("bus.maxPass:      %d\n", v1.other.bus.maxPass);
   printf("bus.class:        %s\n", v1.other.bus.class);

   printf("*** %d\n", sizeof(BusOrCar));

   v1.other.car.capacity++; 
   printf("bus.class:        %s\n", v1.other.bus.class);

   return EXIT_SUCCESS;
}
