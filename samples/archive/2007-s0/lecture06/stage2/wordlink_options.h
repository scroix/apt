/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #2 - word link program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows
****************************************************************************/

#ifndef WORDLINK_OPTIONS_H
#define WORDLINK_OPTIONS_H

/* Function prototypes. */
void onePlayerGame(DictionaryType* dictionary);
void twoPlayerGame(DictionaryType* dictionary);
void dictionarySummary(DictionaryType* dictionary);
void searchDictionary(DictionaryType* dictionary);
void addDictionaryWord(DictionaryType* dictionary);
int saveDictionary(DictionaryType* dictionary, char* filename);

#endif
