/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #1 - tic tac toe program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows
****************************************************************************/

/* Header files. */
#include <stdio.h>
#include <stdlib.h>

/* Constants. */
#define ROW_LEN 3
#define COL_LEN 3
#define TRUE 1
#define FALSE 0
#define NAUGHT 'O'
#define CROSS 'X'
#define CELL_MIN 1
#define CELL_MAX 9

/* Function prototypes. */
void init(char tttGrid[ROW_LEN][COL_LEN]);
void display(char tttGrid[ROW_LEN][COL_LEN]);
void playerGuess(char tttGrid[ROW_LEN][COL_LEN]);
void computerGuess(char tttGrid[ROW_LEN][COL_LEN]);
int isGameOver(char tttGrid[ROW_LEN][COL_LEN]);
void scoreBoard(char tttGrid[ROW_LEN][COL_LEN], int pWin, int cWin, int tie);
int playAgain();
void readRestOfLine();
