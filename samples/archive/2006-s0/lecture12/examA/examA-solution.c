/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 1/2/2006
* 
* examA.c
* Sample exam question.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SUCCESS 1
#define FAILURE 0

int interleave(char* fname1, char* fname2, char* fnameOut);
void outputLine(FILE* fpOut, char* line);

int main(void)
{
   interleave("file1.txt", "file2.txt", "file3.txt");

   return EXIT_SUCCESS;
}

/* MARK ALLOCATION FOR interleave() FUNCTION: */

/* 1 mark. */
int interleave(char* fname1, char* fname2, char* fnameOut)
{
   /* 4 marks. */
   FILE* fpIn1;
   FILE* fpIn2;
   FILE* fpOut;
   char line[80 + 1]; /* Assumption: Max line length 80 characters. */
   
   /* 2 x 3 = 6 marks. */
   if ((fpIn1 = fopen(fname1, "r")) == NULL)
   {
      fprintf(stderr, "Cannot open %s.\n", fname1); /* Not asked for. */
      return FAILURE;
   }
   if ((fpIn2 = fopen(fname2, "r")) == NULL)
   {
      fprintf(stderr, "Cannot open %s.\n", fname2); /* Not asked for. */
      return FAILURE;
   }
   if ((fpOut = fopen(fnameOut, "w")) == NULL)
   {
      fprintf(stderr, "Cannot open %s.\n", fnameOut); /* Not asked for. */
      return FAILURE;
   }
   
   /* 4 marks. */
   while (!feof(fpIn1) || !feof(fpIn2))
   {
      /* 3 marks. */
      if (fgets(line, 80 + 1, fpIn1) != NULL)
      {
         outputLine(fpOut, line);
      }

      /* 3 marks. */
      if (fgets(line, 80 + 1, fpIn2) != NULL)
      {
         outputLine(fpOut, line);
      }
   }
   
   /* 3 marks. */
   fclose(fpIn1);
   fclose(fpIn2);
   fclose(fpOut);
   
   /* 1 mark. */
   return SUCCESS;
}

/* MARK ALLOCATION FOR outputLine() FUNCTION: */

/* 1 mark */
void outputLine(FILE* fpOut, char* line)
{
   /* 5 marks */
   unsigned length;
   unsigned words = 0;
   char* tok;
   char tempLine[80 + 1];
   strcpy(tempLine, line);

   /* 1 mark */
   length = strlen(line);

   /* 4 marks */
   tok = strtok(tempLine, " \n");
   while (tok != NULL)
   {
      words++;
      tok = strtok(NULL, " \n");
   }

   /* 1 mark */
   fprintf(fpOut, "%d %d %s", length - 1, words, line);
}
