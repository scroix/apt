/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 7/1/2006
* 
* arrayParameter.c
* Passing arrays as parameters example.
* Citation: Adapted from slide 4-13 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define SIZE 5

int add1(int* nums);
int add2(int nums[]);

int main(void)
{
   int numList[SIZE] = {1, 2, 3, 4, 5};
   int sum;
   
   sum = add1(numList);
   printf("Sum is: %d\n", sum);
   
   sum = add2(numList);
   printf("Sum is: %d\n", sum);
   
   return EXIT_SUCCESS;
}

int add1(int* nums)
{
   int total = 0, j;
   
   for (j = 0; j < SIZE; j++)
   {
      total += *(nums + j);
   }
   
   return total;
}

int add2(int nums[])
{
   int total = 0, j;
   
   for (j = 0; j < SIZE; j++)
   {
      total += nums[j];
   }
   
   return total;
}

int double1(int nums[])
{
   int total = 0, j;
   
   for (j = 0; j < SIZE; j++)
   {
      nums[j] *= 2;
      nums[j] = nums[j] * 2;
      *(nums + j) *= 2;
      *(nums + j) = *(nums + j) * 2; 
   }
   
   return total;
}
