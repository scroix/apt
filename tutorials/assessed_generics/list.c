#include "list.h"
#include <string.h>

/**************************************************************************
 * initialise the list to a safe state where all values start from known
 * values. 
 *************************************************************************/
void list_init(struct list * list, int (*insert_cmp)(void*,void*), 
                int (*del_cmp)(void*, char*), void (*print)(void*), 
                void(*datafree)(void*))
{
        assert(list);
        /* set the content of the list to have all 0 values */
        memset(list, 0, sizeof(struct list));
        list->insert_cmp = insert_cmp;
        list->del_cmp = del_cmp;
        list->print = print;
        list->datafree = datafree;
}

/**************************************************************************
 * list_add() - implements the algorithm to insert into a list in sorted 
 * order. If you think back to the sorting algorithms covered earlier in 
 * this course this is an insertion sort.
 *************************************************************************/
BOOLEAN list_add(struct list * list, void * data)
{
        /* make sure to initialise your prev pointer to NULL */
        struct list_node * current, *prev=NULL;
        struct list_node * new;

        /* ensure our incoming pointers are valid */
        assert(list);
        assert(data);

        /* allocate space for the node */
        new = malloc(sizeof(struct list_node));
        if(!new)
        {
                return FALSE;
        }
        /* initialise the next and data pointers to the appropriate values*/
        /* setting next to NULL handles when we add to the end of the list*/
        new->next=NULL;
        new->data=data;

        /* if we are at the begging of the list just assign the new node 
         * to the head of the list */
        if(list->head == NULL)
        {
                list->head = new;
                /* increment the count of items in the list */ 
                ++list->count;
                /* job done */
                return TRUE;
        }
        /* grab the beginning of the list and find the insertion point 
         * for our data. Remember that cmp functions work essentially
         * like subtraction - is current data less than data in our 
         * sort order?
         **/ 
        current = list->head;
        while(current != NULL && list->insert_cmp(current->data, data) < 0)
        {
                /* grab the current pointer and assign it to previous so we
                 * can insert data between two nodes
                 */
                prev=current;
                current=current->next;
        }
        /* insertion at the beginning of the list */
        if(prev == NULL)
        {
                new->next=list->head;
                list->head=new;
        }
        /* insertion at the end - redundant if statement because of the 
         * else statement further on
         **/
        else if(!current)
        {
                new->next=NULL;
                prev->next=new;
        }
        else
        {
                prev->next=new;
                new->next=current;
        }
        /* increment the count of items in the list */
        ++list->count;
        /* job done */
        return TRUE;
}

/**************************************************************************
 * free all the items in the list
 *************************************************************************/
void list_free(struct list * list)
{
        /* grab the beginning of the list */
        struct list_node * current = list->head;
        /* iterate over all the list elements */
        while(current != NULL)
        {
                struct list_node * next;
                /* grab the current element so we can free it */
                next = current;
                /* move on to the next element */
                current = current->next;
                /* free the data */
                list->datafree(next->data);
                /* free the list node */
                free(next);
        }
}

/**************************************************************************
 * Iterates over the list, printing out each element. The actual printing
 * is handled by a function pointer.
 *************************************************************************/
void list_print(struct list * list)
{
        struct list_node * current = list->head;
        while(current != NULL)
        {
                list->print(current->data);
                current = current->next;
        }
}

/**************************************************************************
 * This function should search for the id specified and when it is found, 
 * remove the item from the list. 
 *************************************************************************/
BOOLEAN list_remove(struct list * list, char * id)
{
        struct list_node * curr, * prev, * remove;

        get_string("Please enter the id of an element to remove", id, NAMELEN);

        curr = list->head;
        /* search through list to find matching id */
        while(curr != NULL && strcmp(curr->data->id, id) < 0)
        {
            previous = curr;
            curr = curr->next;
        }
        /* item not found */
        if(curr == NULL || strcmp(curr->data->id, id) > 0)
        {
            /* item isn't in the list */
            fprintf(stderr, "Error: the item was not found. \n\n");
            return FALSE;
        }
        /* item to be deleted located at beginning of the list */
        if(previous == NULL)
        {
            data = list->head->data;
            remove = list->head;
            list->head = list->head->next;
        }
        else
        {
            /* delete from else, end included */
            remove = curr;
            data = remove->data;
            previous->next = curr->next;
        }

        /* inform user */
        printf("\n%s has been removed from the system.\n\n", remove->data->id);

        /* free the data and reduce list size */
        free(remove);
        list->count--;

        return TRUE;
}

