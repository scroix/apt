README for function pointer example
-----------------------------------

Created by Paul Miller for COSC1076 - Advanced Programming 
Techniques.

This is meant to be a simple example of using function 
pointers in C. While there is a fair amount of code here, the 
important part is in main. 

If you look at main, I create two arrays - one for the function
pointers and one for the strings. I pass these strings into 
a function that creates a menu based on them. 

The index we get back is then used to index into the array of 
function pointers and we call the function at that index.

Please feel free to ask any questions about this example in the
blackboard discussion forum.

Cheers

Paul
