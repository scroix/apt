/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 4/1/2006
* 
* swap.c
* Simple examples of call-by-reference and call-by-value functions.
* Citation: Adapted from slides 2-34 and 2-36 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

void swap1(int a, int b);
void swap2(int* a, int* b);

int main(void)
{
   int x = 10, y = 4;

   swap1(x, y);
   printf("x: %2d  y: %2d\n", x, y);
   swap2(&x, &y);
   printf("x: %2d  y: %2d\n", x, y);

   return EXIT_SUCCESS;
}

void swap1(int a, int b)
{
   int temp;

   temp = a;
   a = b;
   b = temp;
}

void swap2(int *a, int *b)
{
   int temp;

   temp = *a;
   *a = *b;
   *b = temp;
}
