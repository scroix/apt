/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #2 - word link program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows
****************************************************************************/

#ifndef WORDLINK_UTILITY_H
#define WORDLINK_UTILITY_H

/* Function prototypes. */
void readRestOfLine();
void systemInit(DictionaryType* dictionary);
int loadData(DictionaryType* dictionary, char* filename);
void systemFree(DictionaryType* dictionary);

#endif
