/* Excercise 15 
 * this example is taken from page 143 of "A Book on C"
 */

#include <stdio.h>

int main(void)
{
   char c = 'A';
   
   printf("sizeof(c)  = %u\n", sizeof(c));
   printf("sizeof('A')  = %u\n", sizeof('A'));
   printf("sizeof(c + c) = %u\n", sizeof(c + c));
   printf("sizeof(c = 'A') = %u\n", sizeof(c = 'A'));
   return 0; 
}
