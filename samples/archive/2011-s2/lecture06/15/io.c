/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 13/1/2006
* 
* io.c
* Example of makefiles.
* Citation: Slide 6-16 of PP2A/PT course notes.
****************************************************************************/

#include "io.h"
extern int i;

void get()
{
   /* ... */ printf("this is: %d\n",i);
}

void put()
{
   /* ... */
}
