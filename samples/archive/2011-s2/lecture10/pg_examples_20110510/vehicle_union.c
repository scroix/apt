#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TEXT_MAX 30

struct car {
    int number_cylinders;
    int engine_capacity;
};

struct bus {
    int max_passengers;
    char class[TEXT_MAX + 1];
};

enum vehicle_type {
    car_type,
    bus_type
};

struct vehicle {
    enum vehicle_type vehicle_type;

    union {
        struct car car;
        struct bus bus;
    } data;

    char manufacturer[TEXT_MAX + 1];
    int engine_number;
};

int main(void)
{
    struct vehicle v1;

    /* Initialise the v1 as a car. */
    v1.vehicle_type = car_type;

    v1.data.car.number_cylinders = 6;
    v1.data.car.engine_capacity = 2800;

    strncpy(v1.manufacturer, "Holden", TEXT_MAX);
    v1.engine_number = 12345;

    /* Overwrite the v1 as a bus. */
    v1.vehicle_type = bus_type;

    v1.data.bus.max_passengers = 48;
    strncpy(v1.data.bus.class, "Commercial", TEXT_MAX);

    strncpy(v1.manufacturer, "Volvo", TEXT_MAX);
    v1.engine_number = 98765;

    printf("cat.number_cylinders and bus.max_passengers share the same\n"
           "location in memory.\n\n");

    printf("car.number_cylinders: %d (%p)\n",
           v1.data.car.number_cylinders,
           (void*)&v1.data.car.number_cylinders);

    printf("bus.max_passengers:   %d (%p)\n\n",
           v1.data.bus.max_passengers,
           (void*)&v1.data.bus.max_passengers);

    printf("cat.engine_capacity and bus.class also share the same\n"
           "location in memory, however, they are different types.\n\n");

    printf("car.engine_capacity:  %d (%p)\n",
           v1.data.car.engine_capacity,
           (void*)&v1.data.car.engine_capacity);

    printf("bus.class:            %s (%p)\n\n",
           v1.data.bus.class,
           (void*)&v1.data.bus.class);

    printf("The size of the union in bytes is size of the largest\n"
           "element in the union.\n\n");

    printf("sizeof(v1.data):      %d\n\n", sizeof(v1.data));

    printf("v1.data.car.engine_capacity++ will treat its memory location\n"
           "as a 4 byte integer. Incrementing that integer on a big-endian\n"
           "byte ordered architecture will increment its fourth byte.\n\n");

    v1.data.car.engine_capacity++;

    printf("bus.class:            %s\n", v1.data.bus.class);
    printf("                         ^\n");

    return EXIT_SUCCESS;
}
