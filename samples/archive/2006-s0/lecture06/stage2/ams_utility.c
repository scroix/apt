/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Summer 2006 Assignment #2 - Academic Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows - sdb@cs.rmit.edu.au
****************************************************************************/

#include "ams.h"
#include "ams_options.h"
#include "ams_utility.h"

/****************************************************************************
* Function readRestOfLine() is used for buffer clearing. Source: 
* https://inside.cs.rmit.edu.au/~sdb/teaching/C-Prog/CourseDocuments/
* FrequentlyAskedQuestions/
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}

/****************************************************************************
* Initialises the system to a safe empty state.
****************************************************************************/
void systemInit(AMSType* system)
{
}

/****************************************************************************
* Loads all data into the system.
****************************************************************************/
void loadData(AMSType* system, char* courseFile, char* resultFile)
{
}

/****************************************************************************
* Deallocates memory used in the program.
****************************************************************************/
void systemFree(AMSType* system)
{
}
