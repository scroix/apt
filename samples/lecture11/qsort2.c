#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void print_array(char **array, size_t n)
{
    size_t i;

    for(i = 0; i < n; i++)
        printf("%s ", array[i]);

    printf("\n");
}

int string_compare(const void *vptr1, const void *vptr2)
{
    const char *a = *(const char**)vptr1;
    const char *b = *(const char**)vptr2;

    return strcmp(a, b);
}

int main(int argc, char **argv)
{
    if (argc < 3) {
        fprintf(stderr, "usage: %s <word1> ... <wordn>\n", argv[0]);
        return EXIT_FAILURE;
    }

    print_array(argv + 1, argc - 1);

    qsort(argv + 1, argc - 1, sizeof(char*), string_compare);

    print_array(argv + 1, argc - 1);

    return EXIT_SUCCESS;
}
