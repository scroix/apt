/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 4/1/2006
* 
* return.c
* Simple alternative to using the return keyword (not necessarily
* better). 
* Citation: Adapted from slide 2-40 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

void sum(int a, int b, int* sum);

int main(void)
{
   int x, y, z;
   
   x = 10;
   y = 4;
   sum(x, y, &z);
   printf("%d + %d = %d\n", x, y, z);

   return EXIT_SUCCESS;
}

void sum (int a, int b, int* sum)
{
   *sum = a + b;
}
