/* Most machines use the two's complement representation to store integers.
 * On these machines, the value -1 stored in an integral type turns all bits
 * on. Assuming that your system does this, here is one way to determine
 * whether a char is equivalent to a signed char or to an unsigned char.
 */

#include <stdio.h>

int main(void)
{
   char c = -1;
   signed char s = -1;
   unsigned char u = -1;

   printf("c = %d   s = %d   u = %d\n", c, s, u);
   
   return 0;
}
