/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 20/1/2006
* 
* example2.c
* Example of infamous fibonacci sequence. 
* Challenge question: Is this problem solvable in a non-recursive fashion?
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int fibonacci(int num);

int main(void)
{
   int i;
   
   /* Print the first 10 numbers of the fibonacci sequence. */
   for (i = 1; i <= 10; i++)
   {
      printf("fibonacci(%d) = %d\n", i, fibonacci(i));
   }

   return EXIT_SUCCESS;
}

int fibonacci(int num)
{
   if (num <= 1)
   {
      return num;
   }
   else
   {
      return (fibonacci(num - 1) + fibonacci(num - 2));
   }
}
