/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* Created December 2005
* COSC1098/1283/1284 Summer 2006 Exam Solutions
* Question 1 - 11 marks
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isPalindrome(char* string);

int main(void)
{
   printf("%d\n", isPalindrome("steven"));
   printf("%d\n", isPalindrome("burrows"));
   printf("%d\n", isPalindrome("deed"));
   printf("%d\n", isPalindrome("racecar"));
   
   return EXIT_SUCCESS;
}

/* Function definition. (1 mark) */
int isPalindrome(char* string)
{
   /* Necessary variables. (2 marks) */
   int i, j;

   /* Looping construct. (5 marks) */
   for (i = 0, j = strlen(string) - 1; i < strlen(string) / 2; i++, j--)
   {
      /* Branching statement. (2 marks) */
      if (string[i] != string[j])
      {
         return 0;
      }
   }
   
   /* Return on success. (1 mark) */
   return 1;
}
