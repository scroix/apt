#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    char string[BUFSIZ];
    int integer;

    printf("Enter an integer: ");

    scanf("%d", &integer);

    /* readRest... */

    printf("Enter a string: ");

    fgets(string, BUFSIZ, stdin);

    printf("You entered '%d' and '%s'\n", integer, string);

    return EXIT_SUCCESS;
}
