/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 20/1/2006
* 
* enum.c
* Example of an enumeration.
* Citation: Adapted from slide 8-9 of PP2A/PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

typedef enum enumDay
{
   SUN, MON, TUE, WED, THU, FRI, SAT
} DayType;

DayType nextDay(DayType day);

int main(void)
{
   DayType today, tomorrow;
   
   today = MON;
   tomorrow = nextDay(today);
   
   printf("Today is %d.\n", today);
   printf("Tomorrow is %d.\n", tomorrow);

   return EXIT_SUCCESS;
}

DayType nextDay(DayType day)
{
   if (day == SAT)
   {
      return SUN;
   }
   else
   {
      return day + 1;
   }
}
