#include <stdio.h>

int main(void)
{
  int x[2][3] = 
      {
        {4, 5, 2}, 
        {7, 6, 9}
      };

  int (*p)[3] = &x[1];
  int (*q)[3] = x;

  printf("%d %d %d\n", (*p)[0], (*p)[1], (*p)[2]);
  printf("%d %d\n", *q[0], *q[1]);

  return 0;
}
