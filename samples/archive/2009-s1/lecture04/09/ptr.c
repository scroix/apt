/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 7/1/2006
* 
* ptr.c
* Pointer arithmetic example.
* Citation: Adapted from slide 4-9 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   float table[4] = {2.24, 4.24, 3.24, -2.1};
   float *fp = table;
   float sum = 0.0;
   int j;

   for (j = 0; j < 4; j++)
   {
      sum += fp[j];
   }

   /*
   for (j = 0; j < 4; j++)
   {
      sum += *(fp + j);
   }
   */

   printf("The sum is: %.2f\n", sum);
   
   return EXIT_SUCCESS;
}
