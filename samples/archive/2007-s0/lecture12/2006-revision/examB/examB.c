/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 4/2/2006
* 
* examB.c
* Sample exam question.
****************************************************************************/

#include "ams.h"
#include <string.h>

void addSomeData(AMSType* system);
void perfectExamStudents(AMSType* system, 
                 char studentNumbers[10][STUDENTID_MAX + 1]);

int main(void)
{
   AMSType system;
   char studentNumbers[10][STUDENTID_MAX + 1];
   int i;

   addSomeData(&system);

   perfectExamStudents(&system, studentNumbers);

   for (i = 0; i < 10 && strcmp(studentNumbers[i],"") != 0; i++)
   {
      printf("Perfect exam student %d is %s.\n", i + 1, studentNumbers[i]);
   }

   return EXIT_SUCCESS;
}



/* IMPLEMENTATION OF perfectExamStudents() FUNCTION GOES HERE (20 marks): */
void perfectExamStudents(AMSType* system, 
                 char studentNumbers[10][STUDENTID_MAX + 1])
{
   CourseType* currentCourse = system->headCourse;
   StudentResultType* currentResult = NULL;
   unsigned numResults = 0;

   while (currentCourse != NULL)
   {
      currentResult = currentCourse->headResult;

      while (currentResult != NULL)
      {
         if (currentResult->examScore == currentCourse->examWeight)
         {
            strcpy(studentNumbers[numResults], currentResult->studentID);
            numResults++;

            if (numResults == 10)
            {
               return;  
            }
         }

         currentResult = currentResult->nextResult;
      }

      currentCourse = currentCourse->nextCourse;
   }

   for ( ; numResults < 10; numResults++)
   {
      strcpy(studentNumbers[numResults], ""); 
   }  
}


void addSomeData(AMSType* system)
{
   CourseType *c1, *c2, *c3, *c4, *c5;
   StudentResultType *c1s1;
   StudentResultType *c2s1, *c2s2;
   StudentResultType *c3s1, *c3s2, *c3s3;
   StudentResultType *c4s1, *c4s2, *c4s3, *c4s4;
   StudentResultType *c5s1, *c5s2, *c5s3, *c5s4, *c5s5;

   /* Do some dynamic memory allocation. */
   /* Deliberately ignore return values of malloc() for now. */
   c1 = malloc(sizeof(CourseType));
   c2 = malloc(sizeof(CourseType));
   c3 = malloc(sizeof(CourseType));
   c4 = malloc(sizeof(CourseType));
   c5 = malloc(sizeof(CourseType));
   c1s1 = malloc(sizeof(StudentResultType));
   c2s1 = malloc(sizeof(StudentResultType));
   c2s2 = malloc(sizeof(StudentResultType));
   c3s1 = malloc(sizeof(StudentResultType));
   c3s2 = malloc(sizeof(StudentResultType));
   c3s3 = malloc(sizeof(StudentResultType));
   c4s1 = malloc(sizeof(StudentResultType));
   c4s2 = malloc(sizeof(StudentResultType));
   c4s3 = malloc(sizeof(StudentResultType));
   c4s4 = malloc(sizeof(StudentResultType));
   c5s1 = malloc(sizeof(StudentResultType));
   c5s2 = malloc(sizeof(StudentResultType));
   c5s3 = malloc(sizeof(StudentResultType));
   c5s4 = malloc(sizeof(StudentResultType));
   c5s5 = malloc(sizeof(StudentResultType));

   /* Populate course structures in a minimal way. */
   c1->examWeight = 10;
   c2->examWeight = 20;
   c3->examWeight = 30;
   c4->examWeight = 40;
   c5->examWeight = 50;

   /* Populate student structures in a minimal way. */
   /* Assume that every student got full marks on the exam.  :-)  */
   c1s1->examScore = 10;
   c2s1->examScore = 20;
   c2s2->examScore = 20;
   c3s1->examScore = 30;
   c3s2->examScore = 30;
   c3s3->examScore = 0;
   c4s1->examScore = 0;
   c4s2->examScore = 0;
   c4s3->examScore = 0;
   c4s4->examScore = 0;
   c5s1->examScore = 0;
   c5s2->examScore = 0;
   c5s3->examScore = 0;
   c5s4->examScore = 0;
   c5s5->examScore = 0;
   strcpy(c1s1->studentID, "c1s1????");
   strcpy(c2s1->studentID, "c2s1????");
   strcpy(c2s2->studentID, "c2s2????");
   strcpy(c3s1->studentID, "c3s1????");
   strcpy(c3s2->studentID, "c3s2????");
   strcpy(c3s3->studentID, "c3s3????");
   strcpy(c4s1->studentID, "c4s1????");
   strcpy(c4s2->studentID, "c4s2????");
   strcpy(c4s3->studentID, "c4s3????");
   strcpy(c4s4->studentID, "c4s4????");
   strcpy(c5s1->studentID, "c5s1????");
   strcpy(c5s2->studentID, "c5s2????");
   strcpy(c5s3->studentID, "c5s3????");
   strcpy(c5s4->studentID, "c5s4????");
   strcpy(c5s5->studentID, "c5s5????");

   /* Join the list of courses together. */
   system->headCourse = c1;
   c1->nextCourse = c2;
   c2->nextCourse = c3;
   c3->nextCourse = c4;
   c4->nextCourse = c5;
   c5->nextCourse = NULL;

   /* Initialise the head of each student result list. */
   c1->headResult = c1s1;
   c2->headResult = c2s1;
   c3->headResult = c3s1;
   c4->headResult = c4s1;
   c5->headResult = c5s1;

   /* Join the rest of the student result nodes together. */
   c1s1->nextResult = NULL;
   c2s1->nextResult = c2s2;
   c3s1->nextResult = c3s2;
   c4s1->nextResult = c4s2;
   c5s1->nextResult = c5s2;

   c2s2->nextResult = NULL;
   c3s2->nextResult = c3s3;
   c4s2->nextResult = c4s3;
   c5s2->nextResult = c5s3;

   c3s3->nextResult = NULL;
   c4s3->nextResult = c4s4;
   c5s3->nextResult = c5s4;

   c4s4->nextResult = NULL;
   c5s4->nextResult = c5s5;

   c5s5->nextResult = NULL;
}
