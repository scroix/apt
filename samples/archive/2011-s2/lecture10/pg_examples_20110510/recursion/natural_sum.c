#include <stdlib.h>
#include <stdio.h>

int natural_sum_r(int n)
{
    if (n == 1)
        return 1;

    return n + natural_sum_r(n - 1);
}

int main (int argc, char **argv)
{
    if (argc != 2) {
        fprintf(stderr, "usage: %s <n>\n", argv[0]);
        return EXIT_FAILURE;
    }

    printf("natural_sum: %d\n", natural_sum_r(atoi(argv[1])));

    return EXIT_SUCCESS;
}
