/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Summer 2006 Assignment #1 - Poker simulation
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows - sdb@cs.rmit.edu.au
****************************************************************************/

/* Header files. */
#include <stdio.h>
#include <stdlib.h>

/* Constants. */
#define HAND_SIZE 5
#define DECK_SIZE 52
#define NUM_SUITS 4
#define NUM_PIPS 13
#define MAX_PIP_LEN 2
#define DEFAULT_CHIPS 20
#define MIN_BET_CHIPS 1
#define FINISHED 1
#define NOT_FINISHED 0

/* Structures. */
typedef struct
{
   unsigned value;             /* 1 = Two of Spades .. 52 = Ace of Hearts. */
   char pip[MAX_PIP_LEN + 1];  /* 2, 3, 4, 5, 6, 7, 8, 9, 10, J, Q, K, A.  */
   char suit;                  /* (S)pades, (C)lubs, (D)iamonds, (H)earts. */
} CardType;

typedef struct
{
   CardType hand[HAND_SIZE];
   unsigned chips;             /* To bet with. */
} PlayerType;

typedef struct
{
   CardType cards[DECK_SIZE];
   unsigned cardsDealt;
} DeckType;

/* Function prototypes. */
void init(DeckType* deck, PlayerType* contestant, PlayerType* dealer);
void shuffle(DeckType* deck);
void deal(DeckType* deck, PlayerType* contestant, PlayerType* dealer);
void improveContestantHand(DeckType* deck, PlayerType* player);
void improveDealerHand(DeckType* deck, PlayerType* player);
void contestantBet(PlayerType* player, int* cBet, int* dBet, int* oppChips);
int dealerBet(PlayerType* player, int* cBet, int* dBet, int* oppChips);
int determineWinner(PlayerType* contestant, PlayerType* dealer, 
                    int* cBet, int*  dBet);
void displayDeck(DeckType* deck);
void displayHand(PlayerType* player, char* playerName);
void readRestOfLine();
