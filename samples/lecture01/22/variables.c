#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int a = 077, b = 0x3f, c = 3U;
   char d = '\n', e = '\004', f = '\0';
   float g = -1.1F, h = 1.1e-02f;
   double i = -1.1, j = 1.1e-02;
   
   printf("a = %d, b = %d, c = %d\n\n", a, b, c);
   printf("d = '%c', e = '%c', f = '%c'\n\n", d, e, f);
   printf("g = %f, h = %f\n\n", g, h);
   printf("i = %f, j = %f\n\n", i, j);

   return EXIT_SUCCESS;
}
