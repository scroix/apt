#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

void add4_copy_by_value(int x)
{
    x = x + 4;
}

void add4_copy_by_reference(int *x)
{
    *x = *x + 4;
}

int main()
{
    int integer = 0;

    printf("integer: %d\n", integer);

    add4_copy_by_value(integer);

    printf("post add4_copy_by_value: %d\n", integer);

    add4_copy_by_reference(&integer);

    printf("post add4_copy_by_reference: %d\n", integer);

    return EXIT_SUCCESS;
}
