/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 24/1/2006
* 
* linkedlist.c
* Example of a simple linked list of strings.
* Citation: Adapted from slides 9-17 - 9-21 of PP2A/PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAME_LEN 20

typedef struct personStruct
{
   char name[NAME_LEN + 1];
   struct personStruct *next;
} PersonType;

void printList1(PersonType* head);
void printList2(PersonType* head);
void printList3(PersonType* head);

int main(void)
{
   PersonType *head = NULL;
   PersonType *curr = NULL;
   /*PersonType *prev = NULL;*/
   char name[NAME_LEN + 1];

   printf("Enter some names (one per line, CTRL-D to finish):\n");

   while (fgets(name, NAME_LEN + 1, stdin) != NULL)
   {
      if ((curr = malloc(sizeof(PersonType))) == NULL)
      {
         /* Code to deal with error.� */
         fprintf(stderr, "Could not allocate %d bytes of memory.\n", 
                 sizeof(PersonType));
         return EXIT_FAILURE;
      }
      else
      {
         strcpy(curr->name, name);
         curr->next = head;
         head = curr;
      }
   }
   
   printf("Calling printList1 (iteration):\n");
   printList1(head);
   printf("Calling printList2 (recursion):\n");
   printList2(head);
   printf("Calling printList3 (recursion):\n");
   printList3(head);
   
   return EXIT_SUCCESS;
}

void printList1(PersonType *list)
{
   while (list != NULL)
   {
      printf("%s\n", list->name);
      list = list->next;
   }
}

void printList2(PersonType *list)
{
   if (list != NULL)
   {
      printf("%s\n", list->name);
      printList2(list->next);
   }
}

void printList3(PersonType *list)
{
   if (list != NULL)
   {
      printList3(list->next);
      printf("%s\n", list->name);
   }
}
