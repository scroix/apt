#include <stdlib.h>
#include <stdio.h>

void print_int(int i)
{
    printf("%d ", i);
}

void function_pointers_part_one()
{
    /* An integer type. */
    int i;

    /* A pointer to an integer type. */
    int *iptr;

    /* A pointer to a function that accepts an integer and returns void. */
    void (*print_int_fnptr)(int);

    i = 3;

    iptr = &i;

    print_int_fnptr = print_int;

    /* Calling a normal funciton. */
    print_int(i);

    printf("\n");

    /* Calling a function through a function pointer. */
    print_int_fnptr(i);

    printf("\n");
}

/*
  A funciton prototype that accepts three arguments. A funciton
  pointer that accepts an integer and returns a void, a pointer to an
  integer and a single integer.

  Because this is a function prototype we do not have to specify
  variable names, only define the funciton type signature. Note the
  function pointer type signature.
*/
void map_ints(void (*)(int), int*, size_t);

/*
  The funciton implementation will apply the function pointer fn to
  each element of in the array of n integers.
*/
void map_ints(void (*fn)(int), int *array, size_t n)
{
    size_t i;
    for (i = 0; i < n; i ++)
        fn(array[i]);

    printf("\n");
}

#define N 10
void function_pointers_part_two()
{
    int array[N] = { 3, 1, 4, 1, 5, 9, 2, 6, 5, 3 };

    void (*print_int_fnptr)(int) = print_int;

    map_ints(print_int, array, N);

    map_ints(print_int_fnptr, array, N);
}

int main()
{
    function_pointers_part_one();

    function_pointers_part_two();

    return EXIT_SUCCESS;
}
