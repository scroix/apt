#!/bin/bash

sequence="40 5 3 2 6 33 4 6 123 55 3 0 0"

for container in container_array container_linked_list container_binary_tree
do
   echo
   echo "./$container $sequence"
         ./$container $sequence
   echo
done
