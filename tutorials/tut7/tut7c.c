#include <stdio.h>
#include <stdlib.h>

int main()
{
    char words[5][8] = { "the", "cat", "in", "the", "hat"};
    char *wp[5];
    int i;

    for (i = 0; i < 5; i++)
    {
        wp[i] = words[i];
    }

    puts(*wp);
    puts(*(wp+2));
    puts(*(wp+3)+1);
    putchar(**(wp+1));

    return EXIT_SUCCESS;
}
