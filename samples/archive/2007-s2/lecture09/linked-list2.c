/****************************************************************
* Linked list examples used in COSC1283/1284 week 9 lecture 
* Comments provided below should be self-explanatory.
* Suggest you to draw the diagrams as shown in the lectures
* to see how the pointers are decided (Xiaodong Li, 12/09/2007).
* Acknowledgement: insertNode and deleteNode follow examples in 
* "Computer Science - A Structural Programming Approach Using C"
* by Forouzan and Gilberg (2001).
*****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 20

typedef struct PersonStruct {
  char *name; /* will be dynamically allocated */
  struct PersonStruct *next;
}PersonType;


/*================= insertNode ================== 
This function inserts a single node into a linked list.
head is pointer to the list; may be null.
prev points to new node's logical predecessor.
name contains person's name to be inserted.
The function returns the head pointer.
*/
PersonType *insertNode(PersonType *head, PersonType *prev, char *name)
{
    /* local variable */
    PersonType *p;
    
    /* allocate storage */
    if ((p = (PersonType *)malloc(sizeof(PersonType))) == NULL)
    {
        printf("Memory overflow in insertNode\n");
        exit(1);
    }

    /* below is just a simple example. You may use fgets() to get the string */
    /* note that you need to allocate "strlen(string) + 1" bytes to include the  */
    /* extra '\0' which marks the end of the string. */	
    if ((p->name = malloc(20)) == NULL)    
    {
        printf("***Memory insufficient***");
        exit(1);
    }    
    
    strcpy(p->name, name);   
    if (prev == NULL)
    {
        /* inserting before first node or to empty list */
        p->next = head;
        head = p;
    }/* if prev */
    else
    {
        /* inserting in middle or at end */
        p->next = prev->next;
        prev->next = p;
    }
    return head;
} /* insertNode */


/* =============== deleteNode ==================
This function deletes a single node from the link list.
head is a pointer to the head of the list.
prev points to node before the delete node
curr points to the node to be deleted.
when finished, free curr, and returns the head pointer.
*/
PersonType *deleteNode(PersonType *head, PersonType *prev, PersonType *curr)
{
    if (prev == NULL)
        /* deleting first node */
        head = curr->next;
    else
        /* deleting other nodes */
        prev->next = curr->next;
        
    free (curr);
    return head;
} /* deleteNode */


/* Traverse and print a linked list.
p points to the beginning of a linked list.
*/
void printList(PersonType *p)
{   
    printf("\nList contains:\n");
    while (p != NULL)
    {
        printf("%s -> ", p->name);
        p = p->next;
    } /* while */
    printf("\n");
    return;
} /* printList */


/* test driver */
int main(void)
{
    /* local variables */
    PersonType *head=NULL;
    PersonType *prev=NULL;
    PersonType *curr;
    
    /* create the first node */
    /* head will be no longer NULL */
    head = insertNode(head, prev, "David");
    printList(head);
    
    /* insert another node at the begining of the list */
    head = insertNode(head, prev, "John");
    printList(head);
    
    /* now assign prev the address of the node for "David" */
    prev = head->next;
   
    /* insert now the new node "Elizabeth" after "David" */
    head = insertNode(head, prev, "Elizabeth");
    printList(head);
    return 1;
}
