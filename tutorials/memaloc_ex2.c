#include <stdio.h>
#include <stdlib.h>

int list_add(List *list, Person * person)
{
	Node * new_node, current, previous = NULL;
	assert(list != NULL);
	assert(person != NULL);

	new_node = malloc(sizeof(Node));
	if(new_node == NULL)
	{
		fprintf(stderr, "Error, failed to allocate memory\n");
		return FALSE;
	}
	new_node->date = person;
	new_node->next = NULL; // safest to do this, rather than checking if it's last element first
	current = list->head;

	if(current == NULL)
	{
		/* The list is super empty */
		list->head = new_node;
		list->size++;
		return TRUE;
	}

	while(current != NULL && strcmp(current->data->lname, person->lname) < 0)
	{
		previous = current;
		current = current->next;
	}
	/* final node*/
	if(current == NULL)
	{
		/* adding to the end of list */
		previous-> = new_node;
		/* in this case we've already set next to null, so we don't have to do it here */
	}
	else if (previous == NULL)
	{
		/* add to the beginning of the list */
		new_node -> next = list->head;
		list->head = new_node;
	}
	else
	{
		/* insert in the middle of the list somewhere */
		previous->next = new_node;
		new_node->next = current;
	}
	list->size++;
	return TRUE;

}

int main(void)
{
	unsigned age;
	char line[LINELEN+EXTRA_CHARS];
	Person * new_person;
	int finished = FALSE;
	List list;
	int valid = FALSE;

	list_init(&list);
	while(!finished)
	{
		valid = FALSE;
		new_person = malloc(sizeof(Person));
		if(new_person == NULL)
		{
			fprint(stderr, "NO PERSON FOR YOU");
			return EXIT_FAILURE;
		}
		while(!valid)
		{
			get_input("Enter the first name", line);
		}
		if(*line == 0)
		{
			finished = TRUE;
			break;
		}
		strcpy(new_person->fname, line)
		valid = FALSE;
		while(!valid)
		{
			get_input("Enter the surname", line);
		}
		if(*line == 0)
		{
			finished = TRUE;
			break;
		}
		strcpy(new_person->lname, line);
		valid = FALSE;
		while(!valid)
		{
			get_int("Enter the age: ", *age);
		}
		new_person->age = age;

		if(!list_add(&list, new_person))
		{
			fprintf(stderr, "FAILED TO ADD TO LIST");
			return EXIT_FAILURE;
		}

		list_print(&list);
		list_free(&list);
	}
}