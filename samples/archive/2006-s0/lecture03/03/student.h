/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 5/1/2006
* 
* student.h
* A simple but poor example of how to use structures.
* Citation: Adapted from slide 3-13 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define STNUM_LEN 8

struct studentRecord
{
   char  stNum[STNUM_LEN + 1];
   int   ass1, ass2, exam;
   float total;
   char  grade;
};

float calcTotal(int, int, int);
char  calcGrade(float); 
