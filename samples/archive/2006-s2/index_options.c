/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Semester 2 2006 Assignment #2 - index program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows & Daryl D'Souza
****************************************************************************/

#include "index.h"
#include "index_options.h"
#include "index_utility.h"

/****************************************************************************
* Menu option #1: Index Summary
* Displays a high level summary of index contents.
****************************************************************************/
void indexSummary(IndexType* index)
{
}


/****************************************************************************
* Menu option #2: Search Index
* Allows the user to make a new report file for a chosen category.
****************************************************************************/
void searchIndex(IndexType* index)
{
}


/****************************************************************************
* Menu option #3: Index Report
* Creates an external file with statistics of all words in the index.
****************************************************************************/
void indexReport(IndexType* index)
{
}


/****************************************************************************
* Menu option #4: Stemming Report
* Creates an external file with statistics of all words in the index that
* have common suffixes.
****************************************************************************/
void stemmingReport(IndexType* index)
{
}


/****************************************************************************
* Menu option #5: Stopping Report
* Creates an external file with statistics of some common words in the index.
****************************************************************************/
void stoppingReport(IndexType* index)
{
}


/****************************************************************************
* Menu option #6: Delete Stop Words
* Removes common words from the index supplied from an external file.
****************************************************************************/
void deleteStopWords(IndexType* index)
{
}
