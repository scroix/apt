Introduction to gdb
===================

The purpose of a debugger such as gdb is to allow you to see what is going 
on "inside" your program while it executes.

gdb is a "symbolic" debugger which means that you can examine your program 
during execution using the symbols (variable and function names etc) of 
your source code.  These symbols usually do not get saved by compilation 
so you need to compile your program with the -g option in order for symbols 
to be saved in your executable file - e.g.

	gcc -g -o myprog myprog.c

will compile source file 'myprog.c' into executable file 'myprog' including 
in file 'myprog' the symbols (names) from 'myprog.c'.

Once you've compiled (without errors) your source code in this way you can 
then run it using gdb with the command:

	gdb myprog

If your program failed with a core dump then you can use gdb to examine the
core dump (e.g. where it was and variable values etc when it crashed) by:

	gdb myprog core

After you start up gdb you can issue commands to gdb to control, monitor
and modify your program's execution.  There are a large number of commands
available, and an extensive help facility, but the following commands are
sufficient for most purposes (the abbreviation for each command is shown
in square brackets):

    [l]ist - display source code
		
    [br]eak 'function' | linenum 
	- sets a breakpoint at 'function' or at linenum of your source code
	- when the program reaches this breakpoint, execution is suspended
	  and you can examine your program using other gdb commands

	e.g. br myFunc		sets breakpoint at function 'myFunc'
	     br 120		sets breakpoint at line no. 120 

    [r]un [arglist]
	- starts your program (with arglist, if specified).
	- you will probably want to set some breakpoints before running
	  your program

	e.g. run myprog
	     r myprog < in.data > out.data
	     r myprog -a arg1 -b arg2 > out.data

    print expr
	- display the value of an expression
	- this is pretty clever command for displaying variables - it is
	  recommended that you get familiar with how to display complex
	  variable (e.g. arrays, structs etc) using this command

	e.g.  print myVar
	      print myVar1 + myVar2
	      print myArr[5]

    printf "format-string", arg1, arg2, ..., argn
	- like print but gives you formatting similar to C's printf

    c    
	- continue running your program - i.e. to resume execution after
	  stopping at a breakpoint

    [n]ext 
	- execute next statement
	- steps over any function calls in that statement (i.e. executes 
	  the function but will not 'step' into that function - see below)

    [st]ep 
	- execute next statement
	- steps into any function calls in that statement

    help [name]
	- show information about gdb command 'name', or general information 
	  about using gdb

    [q]uit 
	- exit from gdb

Other commands that can be useful - use help for more info - include:

    clear - to clear breakpoints
    set   - to modify variables

    conditional breakpoints - e.g. breakpoints that only suspend execution
			      when certain conditions occur

... but the vast bulk of what you need when debugging can be achieved with
just the commands above.

Have a browse through the (extensive) help - many of the commands may not
make much sense to you yet ... if so, then you probably don't need them.

I'm a big fan of symbolic debuggers (wouldn't consider doing serious
software development without one) and believe time spent getting to know
your debugger pays you back 10-fold (at least!).
