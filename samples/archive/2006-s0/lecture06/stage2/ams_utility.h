/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Summer 2006 Assignment #2 - Academic Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows - sdb@cs.rmit.edu.au
****************************************************************************/

#ifndef AMS_UTILITY_H
#define AMS_UTILITY_H

/* Function prototypes. */
void readRestOfLine();
void systemInit(AMSType* system);
void loadData(AMSType* system, char* courseFile, char* resultFile);
void systemFree(AMSType* system);

#endif
