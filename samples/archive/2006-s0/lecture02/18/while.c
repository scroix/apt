/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 3/1/2006
* 
* while.c
* Simple while-loop examples.
* Citation: Adapted from slide 2-18 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int j;
   
   j = 10;
   while (j > 0)
   {
      printf("%2d ", j);
      j--;
   }
   printf("\n");

   j = 10;
   while (j-- > 0)
   {
      printf("%2d ", j);
   }
   printf("\n");
   
   j = 10;
   while (j--)
   {
      printf("%2d ", j);
   }
   printf("\n");

   return EXIT_SUCCESS;
}
