/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Summer 2006 Assignment #2 - Academic Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows - sdb@cs.rmit.edu.au
****************************************************************************/

#include "ams.h"
#include "ams_options.h"
#include "ams_utility.h"

/****************************************************************************
* Menu option #1: Display Summary
* Allows the user to display a summary of all courses and results in the 
* system.
****************************************************************************/
void displaySummary(AMSType* system)
{
}

/****************************************************************************
* Menu option #2: Course Report
* Allows the user to make a new report file for a chosen course
****************************************************************************/
void courseReport(AMSType* system)
{
}

/****************************************************************************
* Menu option #3: Add Course
* Allows the user to add a new course record to the linked list.
****************************************************************************/
void addCourse(AMSType* system)
{
}

/****************************************************************************
* Menu option #4: Add Result
* Allows the user to add a new result record to an existing course. An error 
* message is returned if the course doesn't exist.
****************************************************************************/
void addResult(AMSType* system)
{
}

/****************************************************************************
* Menu option #5: Delete Course
* Deletes a course and all corresponding results.
****************************************************************************/
void deleteCourse(AMSType* system)
{
}

/****************************************************************************
* Menu option #6: Delete Result
* Deletes a single result from a particular course.
****************************************************************************/
void deleteResult(AMSType* system)
{
}

/****************************************************************************
* Menu option #7: Save and Exit
* Saves all system data back to the original files. This function does not
* terminate the program - this is left to the main() function instead.
****************************************************************************/
void saveData(AMSType* system, char* courseFile, char* resultFile)
{
}
