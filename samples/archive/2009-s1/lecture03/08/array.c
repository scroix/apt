/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 5/1/2006
* 
* array.c
* A simple example of using an array.
* Citation: Adapted from slide 3-8 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   float table[5];
   int i = 1, j = 2;

   table[2] = 3.24;
   table[i] = table[2] + 1;
   table[i+j] = 18.0;
   table[--i] = table[j] - 2;
   
   for (i = 0; i < 5; i++)
   {
      printf("table[%d] = %f\n", i, table[i]);
   }
   
   return EXIT_SUCCESS;
}
