/* This program demonstrates the use of strstr() function */

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h>

int main(void) {
  const char source[] = "the cat sat on the mat";
  const char * target[] = { "cat", "sat", "fred", "on the", NULL };
  char * result;
  int i;

  for(i=0; target[i] != NULL; i++)
  {
     result = strstr(source, target[i]);
     if (result) /* implied != NULL */
     {
        printf("\"%s\" found starting at \"%s\"\n", target[i], result);
     }
     else
     {
        printf("\"%s\" not found in \"%s\"\n", target[i], source);
     }
  }

  return EXIT_SUCCESS;
}
