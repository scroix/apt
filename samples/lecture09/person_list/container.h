#ifndef CONTAINER_H
#define CONTAINER_H

struct container;

struct container *container_new();

int container_insert(struct container*, const char*, int);

void container_print(struct container*);

void container_free(struct container*);

size_t container_size(struct container*);

#endif /* CONTAINER_H */
