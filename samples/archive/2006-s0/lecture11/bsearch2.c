/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 30/1/2006
* 
* bsearch1.c
* Example of bsearch() function.
* Citation: Slides 11-11 to 11-13 of PP2A/PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int keyCompare(const void*, const void*);

typedef struct
{
  char *processor;
  unsigned clockRateMHz;
} ComputerRecord;

int main(void)
{
   ComputerRecord machines[] = { { "386SX", 25 },
                                 { "486DX", 33 },
                                 { "Pentium", 166 },
                                 { "Pentium II", 266 },
                                 { "Celeron", 400 },
                                 { "Alpha 21164PC", 500} };
   unsigned Nmachines = sizeof(machines) / sizeof(ComputerRecord);
   ComputerRecord target;
   ComputerRecord *pCR;

   printf("Please enter search target (eg. 25, 33, 166, 266, 400, 500): ");
   
   if (scanf("%u", &target.clockRateMHz) == 1)
   {
      if ((pCR = bsearch(&target, &machines[0], Nmachines,
                 sizeof(ComputerRecord), keyCompare)))
      {
         printf("%s %u.\n", pCR->processor, pCR->clockRateMHz);
      }
      else
      {
         printf("No match found.\n");
      }
   }
   else
   {
      fprintf(stderr, "Failed to fetch search target from user.\n");
   }

   return EXIT_SUCCESS;
}

int keyCompare(const void* p1, const void* p2)
{
   const unsigned key1 = ((const ComputerRecord*)p1)->clockRateMHz;
   const unsigned key2 = ((const ComputerRecord*)p2)->clockRateMHz;

   return (key1 == key2) ? 0 : ((key1 < key2) ? -1 : 1 );
}

