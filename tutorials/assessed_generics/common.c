#include "common.h"
/*
 * clear the buffer - this function is static as it is only needed
 * by other functions in this file
 **/
static void read_rest_of_line(void)
{
        int ch;
        while(ch = getchar(), ch != EOF && ch != '\n')
                ;
        clearerr(stdin);
}

/*
 * drop in replacement for c99 strdup(). It mallocs a chunk of memory the
 * same size as the string passed in and then copies the string, returning
 * the new pointer
 **/
char * my_str_dup(const char * src)
{
        char * ret = ( char* ) malloc( strlen ( src ) + 1 );
        if(!ret)
        {
                return NULL;
        }
        strcpy(ret, src);
        return ret;
}

/*
 * reads a line from the FILE pointer passed in up to the length 
 * specified into the char pointer line which is assumed to point
 * to the beginning of an array
 **/
enum io_result read_line(FILE* fp, int length, /* out */char * line)
{
        /* fgets returns NULL on a read error but we may or may not be
         * at the end of the file.
         */
        if(!fgets( line, length + EXTRA_CHARS, fp) )
        {
                /* if we are at the end of the file return an indicator
                 * of this fact
                 */
                if(feof(fp))
                {
                        return R_EOF;
                }
                /* otherwise, print a message indicating what the error
                 * was and return a failure indicator.
                 */
                perror("failed to read from file");
                return R_FAILURE;
        }
        /* was the line too long? */
        if(line[strlen(line)-1] != '\n')
        {
                fprintf(stderr, "Error: the following line was too "
                                "long: %s. The length was %ld and "
                                "the expected length wwas %d\n", line, 
                                strlen(line), length);
                return R_FAILURE;
        }
        /* otherwise the line was of the allowed length so all good - 
         * remove the newline as we have validated there was not 
         * buffer overflow
         */
        line[strlen(line) - 1] = 0;
        return R_SUCCESS;
}

/*
 * parses a row / line from a file and tokenizes it. Each line of the file
 * is delimited by a comma as that is what is expected for this program. 
 * There should be exactly the number of tokens specified in num_tokens
 **/
BOOLEAN parse_row(/* out */char*** array, const char* line, int num_tokens)
{
        /* local copy of the 2d array we will write to */
        char ** larray, 
             /* a copy of the line passed in - as the line passed in is a 
              * const char array, we cannot use this with strtok
              */
        *linecpy;
        /* a pointer to the current token */
        char * tok;
        int count_tokens;

        /* allocate space for the array of pointers */
        larray = (char**)malloc(sizeof(char*) * num_tokens);
        if(!larray)
        {
                perror("failed to allocate memory");
                return FALSE;
        }
        /* copy the line passed in */
        linecpy = my_str_dup(line);

        /* start the tokenization session */
        tok = strtok ( linecpy , SEPARATOR);
        count_tokens = 0;
        /* iterate over each token we get - we will stop when we run
         * out of tokens to process
         */
        while( tok != NULL )
        {
                larray[count_tokens++] = tok;
                tok = strtok(NULL, SEPARATOR);
        }
        /* do we have the number of tokens we expected ? */
        if( count_tokens != num_tokens )
        {
                free(linecpy);
                free(larray);
                return FALSE;
        }
        /* otherwise, all is good so give access to this array to the
         * function that called it and return TRUE
         */
        *array = larray;
        return TRUE;
}

/*
 * gracefully open a file - when an open fails we quit the program.
 **/
FILE * gropen(const char * fname)
{
        FILE * fp = fopen(fname, "r");
        if(fp)
        {
                return fp;
        }
        perror("failed to open the file");
        exit(EXIT_FAILURE);
}

/*
 * convert the string passed in as 'input' to an ouput int, validating that
 * it is indeed a valid int
 **/
BOOLEAN str_to_int(const char *input, int * output)
{
        /* the extracted input value as a long - longs take up twice the
         * memory on current architectures and thus could easily overflow
         * the size of an int
         */
        long linput;
        char * end;
        /* convert the input value to a long */
        linput = strtol(input, &end, 10);

        /* was the string passed in containing wholly numeric contents? */
        if(*end)
        {
                fprintf(stderr, "Error: %s is not numeric.\n",
                                input);
                return FALSE;
        }

        /* is the number within the range of an int ? */
        if(linput < INT_MIN || linput > INT_MAX)
        {
                fprintf(stderr, "Error: %s is outside the range for "
                                "an integer.\n", input);
        }
        /* all the tests have worked so store the value for return and 
         * return TRUE
         **/
        *output = (int)linput;
        return TRUE;
}

/*
 * process the string input with the expectation of extracting a floating
 * point number and validating it
 **/
BOOLEAN str_to_float(const char * input, float * output)
{
        /* the incoming values converted to double */
        double dinput;
        char * end;
        /* extract the value contained in the string assuming it is a 
         * double precision floating point number
         */
        dinput = strtod(input, &end);
        if(*end)
        {
                fprintf(stderr, "Error: %s is not numeric.\n", 
                                input);
                return FALSE;
        }

        /* is it in the valid range for a float (single precision
         * floating point number)? 
         */
        if(dinput < FLT_MIN || dinput > FLT_MAX)
        {
                fprintf(stderr, "Error: %s is outside the range for a "
                                "float.\n", input);
        }
        /* the tests have been successful - return the number and an
         * indication that the conversion was successful
         */
        *output = (float) dinput;
        return TRUE;
}

/*
 * get a string of input from the user. Validate that the string
 * is no longer than outlen
 **/
enum io_result get_string( const char * prompt, /* out */ char * output, 
                int outlen)
{
        /* display the prompt to the user */
        printf("%s: ", prompt);
        /* get a string of input from the user */
        if(!fgets(output, outlen, stdin) || *output=='\n')
        {
                return R_EOF;
        }
        /* was the string shorter than the maximum length specified ? */
        if(output[strlen(output)-1] != '\n')
        {
                fprintf(stderr, "Error: line entered was too long.\n");
                read_rest_of_line();
                return R_FAILURE;
        }
        /* all was good so remove the newline because we now know there
         * was no buffer overflow
         */
        output[strlen(output)-1]=0;
        return R_SUCCESS;
}
