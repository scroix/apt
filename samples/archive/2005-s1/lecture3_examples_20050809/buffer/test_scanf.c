/* Demonstration of a program that handles strings badly. */

#include <stdio.h>
#include <stdlib.h>

#define MAX_STR_LEN 10

int main(void)
{
   char string1[MAX_STR_LEN + 1];
   char string2[MAX_STR_LEN + 1];
   
   printf("Enter string #1: ");
   scanf("%s", string1);
   
   printf("Enter string #2: ");
   scanf("%s", string2);
   
   printf("String #1 entered: '%s'\n", string1);
   printf("String #2 entered: '%s'\n", string2);
   
   return EXIT_SUCCESS;
}
