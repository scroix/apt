/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 27/1/2006
* 
* tree.c
* Practical example of constructing binary search tree.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 1
#define FAILURE 0

typedef struct node* IntBSTNodePtr;

typedef struct node
{
   int data;
   IntBSTNodePtr left, right;
} IntBSTNode;

typedef struct tree
{
   int size;
   IntBSTNodePtr root;
} IntBST;

void MakeBST(IntBST *pBST);
int InsertBST(IntBST *pBST, int data);
void inorderDisplay(IntBSTNodePtr pNode);
void preorderDisplay(IntBSTNodePtr pNode);
void postorderDisplay(IntBSTNodePtr pNode);
void postorderFree(IntBSTNodePtr pNode);

int main(void)
{
   IntBST tree;
   
   MakeBST(&tree);
   
   InsertBST(&tree, 40);
   InsertBST(&tree, 20);
   InsertBST(&tree, 60);
   InsertBST(&tree, 10);
   InsertBST(&tree, 30);
   InsertBST(&tree, 50);
   InsertBST(&tree, 70);
   
   printf("inorderDisplay():\n");
   inorderDisplay(tree.root);
   printf("\npreorderDisplay():\n");
   preorderDisplay(tree.root);
   printf("\npostorderDisplay():\n");
   postorderDisplay(tree.root);
   printf("\n");
   
   postorderFree(tree.root);
   
   return EXIT_SUCCESS;
}

void MakeBST(IntBST *pBST)
{
   pBST->size = 0;
   pBST->root = NULL;
}

int InsertBST(IntBST *pBST, int data) 
{ 
   IntBSTNodePtr current, previous, newNode; 

   previous = NULL; 
   current = pBST->root; 

   /* search for branch onto which to 
    * attach the new leaf node. 
    */ 
   while (current != NULL) 
   { 
      previous = current; 
      if (data < current->data) 
         current = current->left; 
      else /* ie. data >= current->data */ 
         current = current->right; 
   } 
   
   /* Create the new leaf node */ 
   newNode = malloc(sizeof(IntBSTNode)); 
   if (newNode == NULL) 
      return FAILURE; /* Memory allocation failed */
      
   newNode->data = data; 
   newNode->left = NULL; 
   newNode->right = NULL; 
   pBST->size += 1; 

   /* Our search (above) either revealed this insertion 
    * is the first; hence a root node is being created; 
    * or we found the branch on which to attach our new 
    * leaf node 
    */ 
   if (previous == NULL) 
   { 
      pBST->root = newNode; 
      return SUCCESS; 
   } 

   /* if attached to a branch, we need to know if it 
    * is attached as a left or right sub-tree. 
    */ 
   if (data < previous->data) 
   { 
      previous->left = newNode; 
   } 
   else /* ie. data >= previous->data */ 
   { 
      previous->right = newNode; 
   } 

   return SUCCESS; 

} /* End of InsertBST */

void inorderDisplay(IntBSTNodePtr pNode)
{
	if (pNode)
	{
		inorderDisplay(pNode->left);
      printf("%d ", pNode->data);
		inorderDisplay(pNode->right);
	}
}

void preorderDisplay(IntBSTNodePtr pNode)
{
	if (pNode)
	{
		printf("%d ", pNode->data);
      preorderDisplay(pNode->left);
		preorderDisplay(pNode->right);
	}
}

void postorderDisplay(IntBSTNodePtr pNode)
{
	if (pNode)
	{
		postorderDisplay(pNode->left);
		postorderDisplay(pNode->right);
      printf("%d ", pNode->data);
	}
}

void postorderFree(IntBSTNodePtr pNode)
{
   if (pNode)
   {
      postorderFree(pNode->left);
      postorderFree(pNode->right);
      free(pNode);
   }
}
