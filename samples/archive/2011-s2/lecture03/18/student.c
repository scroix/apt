/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 6/1/2006
* 
* student.c
* Examples of structures and nested structures.
* Citation: Adapted from slide 3-18 of PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ID_MAX 8

typedef struct dateStruct
{
   int day, month, year;
} DateType;

typedef struct studentType
{
   char sNum[ID_MAX + 1];
   int ass1, ass2, exam;
   float total;
   char grade;
   DateType dob;
} StudentType;

void displayStudent(StudentType student);
void failExam(StudentType* student);
void setDOB(StudentType* student);
void failAllExams(StudentType* studArray);

int main(void)
{
   StudentType student;
   StudentType studArray[10];  
 
   strcpy(student.sNum, "3001234");
   student.ass1 = 75;
   student.ass2 = 80;
   student.exam = 92;
   student.total = student.ass1 * 0.15 + 
                   student.ass2 * 0.25 +
                   student.exam * 0.60;
   student.grade = 'H';
   student.dob.day = 1;
   student.dob.month = 1;
   student.dob.year = 1980;
  
   failExam(&student); 
   setDOB(&student); 
   displayStudent(student);
   failAllExams(studArray);
   displayStudent(studArray[0]);
   
   return EXIT_SUCCESS;
}

void displayStudent(StudentType student)
{
   printf("StNum: %s\n", student.sNum);
   printf("Ass1 : %d\n", student.ass1);
   printf("Ass2 : %d\n", student.ass2);
   printf("Exam : %d\n", student.exam);
   printf("Total: %-6.2f\n", student.total);
   printf("Grade: %c\n", student.grade);
   printf("DOB  : %02d/%02d/%04d\n", 
           student.dob.day, student.dob.month, student.dob.year);
}

void failExam(StudentType* student)
{
   student->exam = 0;
   /* Equivalent to: (*student).exam = 0; */
}

void setDOB(StudentType* student)
{
   student->dob.day = 20;
   student->dob.month = 10;
   student->dob.year = 1979;
}

void failAllExams(StudentType* studArray)
{
   int i;

   for (i = 0; i < 10; i++)
   {
      studArray[i].exam = 0;
   }
}
