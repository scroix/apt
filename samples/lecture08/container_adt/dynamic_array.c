#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "container.h"

#define DEFAULT_SIZE 10

struct container {
    int *data;
    size_t count;
    size_t size;
};

struct container *container_new ()
{
    struct container *new_container;

    new_container = malloc(sizeof(struct container));
    if (new_container == NULL) {
        perror("container_new: new_container malloc");
        exit(EXIT_FAILURE);
    }

    new_container->count = 0;

    new_container->size = DEFAULT_SIZE;

    new_container->data = malloc(new_container->size * sizeof(int));
    if (new_container->data == NULL) {
        perror("container_new: new_container->data malloc");
        exit(EXIT_FAILURE);
    }

    return new_container;
}

int container_insert (struct container *container, int data)
{
    unsigned int i;

    assert(container != NULL);

    if (container->count == container->size) {
        container->size <<= 1; 
        container->data = realloc(container->data,container->size*sizeof(int));
        if (container->data == NULL) {
            perror("container_insert: malloc");
            exit(EXIT_FAILURE);
        }
    }

    /* ordered insertion to an array using a shuffle shift (week 7 tutorial) */

    i = container->count;

    while (i > 0 && data < container->data[i - 1]) {
        container->data[i] = container->data[i - 1];
        i--;
    }

    container->data[i] = data;

    container->count++;

    return 0;
}

void container_print (struct container *container)
{
    unsigned int i;

    assert(container != NULL);

    for (i = 0; i < container->count; i++)
        printf("%d ", container->data[i]);

    printf("\n");
}

void container_free (struct container *container)
{
    assert(container != NULL);

    free(container->data);

    free(container);
}

size_t container_size (struct container *container)
{
    assert(container != NULL);

    return container->count;
}
