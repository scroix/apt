/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 25/1/2006
* 
* intlist-dynarray.c
* Example of an interface for dynamically allocated integer array.
* Citation: Adapted from slides 10-20 to 10-22 of PP2A/PT lecture notes.
****************************************************************************/

/* IntList
 * -- simple unordered array implementation that uses dynamic memory
 */

#include <stdio.h>
#include <stdlib.h>
#include "intlist-dyn-array.h"

int MakeList(IntList* pil,int size)
{
   if ((pil->array = malloc(sizeof(int) * size)) == NULL)
   {
      return FAILURE;
   }
   pil->size = 0;
   return SUCCESS;
}

void FreeList(IntList* pil)
{
   free(pil->array);
   pil->size = 0;
}

int AddList(IntList* pil,int num)
{
   if (pil->size >= INTLISTSIZE)
   {
      return FAILURE;
   }

   pil->array[pil->size] = num;
   pil->size += 1;

   return SUCCESS;
}

void DisplayList(IntList* pil)
{
   int i, size, *array;

   size = pil->size;
   array = pil->array;

   for(i=0; i<size; i++)
   {
      printf("%d\n", array[i]);
   }
}

unsigned SizeList(IntList* pil)
{
   return pil->size;
}
