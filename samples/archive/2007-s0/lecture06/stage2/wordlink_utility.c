/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #2 - word link program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows
****************************************************************************/

#include "wordlink.h"
#include "wordlink_options.h"
#include "wordlink_utility.h"

/****************************************************************************
* Function readRestOfLine() is used for buffer clearing. Blackboard source: 
* "Course Documents" > "FAQ" > "Alternative to non-standard fflush(stdin);"
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}


/****************************************************************************
* Initialises the dictionary to a safe empty state.
****************************************************************************/
void systemInit(DictionaryType* dictionary)
{
}


/****************************************************************************
* Loads all data into the dictionary.
****************************************************************************/
int loadData(DictionaryType* dictionary, char* filename)
{
   return SUCCESS;
}


/****************************************************************************
* Deallocates memory used in the dictionary.
****************************************************************************/
void systemFree(DictionaryType* dictionary)
{
}
