#include <math.h>
#include "common.h"
#include "list.h"
#ifndef FLOATLIST
#define FLOATLIST
/* the length of an id in the file */
#define F_ID_LEN 2
/* the maximum length of a floating point number in the file */
#define F_FLOAT_LEN 12
/* how many tokens per line ? */
#define F_NUM_TOKENS 2
/* accuracy used for comparing floating point numbers*/
#define ACCURACY 1000
/* the length of each line of the file */
#define F_LINE_LEN F_ID_LEN + F_FLOAT_LEN + F_NUM_TOKENS
/* the tokens available for each line of a file */ 
enum f_tokens
{
        F_ID_TOK, F_FLOAT_TOK, F_INVALID_TOK
};

/* struct that represents the data on each line of the floats file */
struct float_row
{
        char id[F_ID_LEN + 1];
        float number;
};

struct float_row * make_float_node(const char * , float);
struct float_row * parse_float_row(const char *);
BOOLEAN read_float_file(const char *, struct list *);
int float_cmp(void *, void *);
int del_float_cmp(void*, char*);
void float_print(void*);
void float_free(void*);
#endif
