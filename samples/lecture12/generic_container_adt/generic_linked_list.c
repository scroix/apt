#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "container.h"

struct list {
    void *data;
    struct list *next;
};

struct container {
    struct list *head;

    size_t size;

    compare_fn cmpfn;

    free_fn freefn;

    print_fn printfn;
};

struct container *
container_new(compare_fn cmpfn, free_fn freefn, print_fn printfn)
{
    struct container *new_container;

    new_container = malloc(sizeof(struct container));
    if (new_container == NULL) {
        perror("container_new: malloc");
        exit(EXIT_FAILURE);
    }

    new_container->head = NULL;

    new_container->size = 0;

    new_container->cmpfn = cmpfn;

    new_container->freefn = freefn;

    new_container->printfn = printfn;

    return new_container;
}

int container_insert(struct container *container, void *data)
{
    struct list *new_node;

    struct list *prev;
    struct list *curr;

    assert(container != NULL);

    new_node = malloc(sizeof(struct list));
    if (new_node == NULL) {
        perror("container_insert: malloc");
        exit(EXIT_FAILURE);
    }

    new_node->data = data;
    new_node->next = NULL;

    /* linked list insertion (week 8 tutorial) */

    prev = NULL;
    curr = container->head;

    while (curr != NULL && container->cmpfn(curr->data, new_node->data) < 0) {
        prev = curr;
        curr = curr->next;
    }

    if (prev == NULL) /* we are still at the head node */
        container->head = new_node;
    else /* in middle somewhere of at end */
        prev->next = new_node;

    new_node->next = curr;

    container->size++;

    return 0;
}

int container_delete(struct container *container, void *data)
{
    struct list *previous;
    struct list *current;

    /* linked list deletion (week 9 tutorial) */

    previous = NULL;
    current = container->head;

    while (current != NULL && container->cmpfn(current->data, data) != 0) {
        previous = current;
        current = current->next;
    }

    if (current == NULL) /* data was not found */
        return -1;

    if (previous == NULL) /* Head deletion. */
        container->head = current->next;
    else
        previous->next = current->next;

    container->freefn(current->data);

    free(current);

    container->size--;

    return 0;
}

void container_print(struct container *container)
{
    struct list *curr;

    assert(container != NULL);

    curr = container->head;

    while (curr != NULL) {
        container->printfn(curr->data);

        curr = curr->next;
    }
}

void container_free(struct container *container)
{
    struct list *curr;
    struct list *next;

    assert(container != NULL);

    curr = container->head;

    while (curr != NULL) {
        next = curr->next;

        container->freefn(curr->data);

        free(curr);

        curr = next;
    }

    free(container);
}

size_t container_size(struct container *container)
{
    assert(container != NULL);

    return container->size;
}
