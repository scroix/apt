#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    char name[10];
    char colour[10];

    printf("Please enter your name: ");
    scanf("%s", name);
    printf("Please ebter your favorite colour: ");
    scanf("%s", colour);
    printf("Thank you, %s. ", name);

    if (!strcmp(colour, "Red"))
            printf("Red is a good colour.\n");
    else printf("%s is an okay colour.\n");

    return EXIT_SUCCESS;
}
