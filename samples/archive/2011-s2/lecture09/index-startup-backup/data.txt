char *strtok(char *s1, const char *s2);
The strtok() function can be used to break the string
pointed to by s1 into a sequence of tokens, each of which is
delimited by one or more characters from the string pointed
to by s2. The strtok() function considers the string s1 to
consist of a sequence of zero or more text tokens separated
by spans of one or more characters from the separator string
s2. The first call (with pointer s1 specified) returns a
pointer to the first character of the first token, and will
have written a null character into s1 immediately following
the returned token. The function keeps track of its position
in the string between separate calls, so that subsequent
calls (which must be made with the first argument being a
null pointer) will work through the string s1 immediately
following that token. In this way subsequent calls will work
through the string s1 until no tokens remain. The separator
string s2 may be different from call to call. When no token
remains in s1, a null pointer is returned.
