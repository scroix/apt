/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* Created December 2005
* COSC1098/1283/1284 Summer 2006 Exam Solutions
* Question 5 - 10 marks
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isPalindrome(char* string);
int printPalindromesFromFile(char* filename);

int main(int argc, char** argv)
{
   int i; 

   if (argc < 2)
   {
      fprintf(stderr, "Incorrect cmd line args");
      return EXIT_FAILURE;
   } 

   for (i = 1; i < argc; i++)
   {
      if (printPalindromesFromFile(argv[i]) == 0) 
      {
         fprintf(stderr, "Could not process file '%s'\n", argv[i]); 
      } 
   } 


/*   int result1, result2;

   result1 = isPalindrome("hello");
   result2 = isPalindrome("racecar");

   printf("result 1: %d. result 2: %d\n", result1, result2); 
*/
   return EXIT_SUCCESS;
}

int printPalindromesFromFile(char* filename)
{
}

int isPalindrome(char* string)
{
   int i, j;

   for (i = 0, j = strlen(string) - 1; i < j; i++, j--)
   {
      if (string[i] != string[j])
      {
         return 0;
      } 
   }

   return 1; 
}
