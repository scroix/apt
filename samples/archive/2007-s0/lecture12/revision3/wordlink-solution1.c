/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #2 - wordlink game
* Sample Exam Question
****************************************************************************/

/* System-wide header files. */
#include <stdio.h>
#include <stdlib.h>

/* System-wide constants. */
#define SUCCESS 1
#define FAILURE 0
#define TRUE 1
#define FALSE 0
#define ALPHABET_LEN 26

/* Structure definitions. */
typedef struct wordLinkStruct* WordLinkTypePtr;

typedef struct wordLinkStruct
{
   char* word;
   unsigned usedFlag;
   WordLinkTypePtr nextWord;
} WordLinkType;

typedef struct dictionaryStruct
{
   WordLinkTypePtr headWords[ALPHABET_LEN];
   unsigned listCounts[ALPHABET_LEN];
   unsigned totalWords;
} DictionaryType;

int isDictionarySorted(DictionaryType* dictionary);

/* Example (minimalist) driver. */
int main(void)
{
   DictionaryType dictionary;
   char* word1 = "abcde";
   char* word2 = "afghi";
   char* word3 = "aaaaa";
   int i;

   /* Initialise empty linked lists. */
   for (i = 0; i < ALPHABET_LEN; i++)
   {
      dictionary.headWords[i] = NULL;
   }

   /* Add one word. */
   dictionary.headWords[0] = malloc(sizeof(WordLinkType));
   dictionary.headWords[0]->word = calloc(strlen(word1) + 1, sizeof(char));
   strcpy(dictionary.headWords[0]->word, word1);

   /* Add a second word. */
   dictionary.headWords[0]->nextWord = malloc(sizeof(WordLinkType));
   dictionary.headWords[0]->nextWord->word = 
      calloc(strlen(word2) + 1, sizeof(char));
   strcpy(dictionary.headWords[0]->nextWord->word, word2);
   dictionary.headWords[0]->nextWord->nextWord = NULL;

   /* Check current sorted status. */
   if (isDictionarySorted(&dictionary) == TRUE)
   {
      printf("SORTED\n");
   }
   else
   {
      printf("UNSORTED\n");
   }

   /* Add a third (unsorted) word. */
   dictionary.headWords[0]->nextWord->nextWord = malloc(sizeof(WordLinkType));
   dictionary.headWords[0]->nextWord->nextWord->word = 
      calloc(strlen(word3) + 1, sizeof(char));
   strcpy(dictionary.headWords[0]->nextWord->nextWord->word, word3);
   dictionary.headWords[0]->nextWord->nextWord->nextWord = NULL;

   /* Check current sorted status. */
   if (isDictionarySorted(&dictionary) == TRUE)
   {
      printf("SORTED\n");
   }
   else
   {
      printf("UNSORTED\n");
   }
   
   return EXIT_SUCCESS;
}

/* Function definition. (1 mark) */
int isDictionarySorted(DictionaryType* dictionary)
{
   /* Variable declarations. (3 marks) */
   int i;
   WordLinkTypePtr current, previous;

   /* Loop over alphabet. (3 marks) */
   for (i = 0; i < ALPHABET_LEN; i++)
   {
      /* Initialise helper pointers. (2 marks) */
      current = dictionary->headWords[i];
      previous = NULL;

      /* Iterate past first node. (3 marks) */
      if (current != NULL)
      {
         previous = current;
         current = current->nextWord;
      }

      /* Loop over current linked list. (3 marks) */
      while (current != NULL)
      {
         /* Return FALSE when unsorted word is found. (2 marks) */
         if (strcmp(previous->word, current->word) > 0)
         {
            return FALSE;
         }

         /* Increment helper pointers. (2 marks) */
         previous = current;
         current = current->nextWord;
      }
   }

   /* Return TRUE. (1 mark) */
   return TRUE;
}
