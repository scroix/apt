/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include "game.h"

/**
 * @file game.c contains the functions that relate to the management of
 * the game.
 **/

/**
 * @param current the current player who has just completed their turn
 * @param other the next player whose turn it will be
 **/
static void swap_players(struct player ** current, struct player ** other)
{
    /* implement a classic swap using a temporary pointer */
    struct player temp =  ** current;
    ** current = ** other;
    ** other = temp;
}

/**
 * @param human a pointer to details about the human player
 * @param computer a pointer to details about the computer player
 **/
struct player * play_game(struct player * human , 
        struct player* computer)
{
    /* declaration that allocates the board for the game */
    enum cell_contents board[BOARDHEIGHT][BOARDWIDTH];

    /* alternative declaration of players based on which turn they are on */
    struct player * current;
    struct player * other;

    /* result of game */
    enum game_state outcome;

    /* prepare board with empty cells, and randomise player colours */
    initialise_board(board);
    randomise_colours(human, computer);

    /* determine who goes first based on the colour of their pieces */
    if (human -> thiscolor == C_WHITE) {
       current = human;
       other = computer;
    }
    else {
        current = computer;
        other = human;
    }

    /* loop turns until winner is determined */
     do {
            display_board(board);
            
            /* FAILURE check as result of RTM call by user */   
            if(take_turn(current, board) == FAILURE) {
                return NULL;
            }
            
            swap_players(&current, &other);

            outcome = test_for_winner(board);

         } while (outcome == G_NO_WINNER);
   
    display_board(board);

    /* return outcome of game */
    if (outcome == G_DRAW) {
        printf("\n\nDRAW! Everbody was a winner.\n");
    }
    else {
        printf("\n%s WINS! ", (outcome == C_RED ? "RED" : "WHITE"));
        printf("With a final score of %d.\n", other -> counters);
    }

    printf("\nPress any key to continue...");
    getchar();

    return other;
}

/**
 * @param board the gameboard to test for a winner
 **/
enum game_state test_for_winner(enum cell_contents board[][BOARDWIDTH])
{
    /* values to store the outcome of the check functions */
    enum game_state horizontal, vertical, draw, diagLeft, diagRight;

    /* run through all the possible ways of winning con4 and store result */
    horizontal = checkHorizontal(board);
    vertical = checkVertical(board);
    diagLeft = checkDiagLeft(board);
    diagRight = checkDiagRight(board);
    draw = checkDraw(board);
    
    /* if any checks resulted in a win condition, return that winner */
    if ( horizontal != G_NO_WINNER ) {
        return horizontal;
    }

    if ( vertical != G_NO_WINNER ) {
        return vertical;
    }

    if ( diagLeft != G_NO_WINNER ) {
        return diagLeft;
    }

    if ( diagRight != G_NO_WINNER ) {
        return diagRight;
    }

    if ( draw != G_NO_WINNER ) {
        return draw;
    }
   
    return G_NO_WINNER;
}

/**
 * iterate through board and check if there are any empty cells left to play
 **/
enum game_state checkDraw(enum cell_contents board[][BOARDWIDTH])
{
    int i, j;
    int emptyCount = 0;

    for (i = 0; i < BOARDHEIGHT; i++)
    {
        for (j = 0; j < BOARDWIDTH; j++)
        {
            if (board[i][j] == C_EMPTY) {
                emptyCount++;
            }
        }
    }
    /* if there are any empty tokens left this value will be > 1 */
    if (emptyCount == 0)
        return G_DRAW;

    return G_NO_WINNER;

}

/**
 * Scan through array horizontally, checking the cell previous.
 **/
enum game_state checkHorizontal(enum cell_contents board[][BOARDWIDTH])
{ 
    int i, j;
    int conseq = 0;
    enum cell_contents last_found = -1;
    enum falsetrue winner_found = FALSE;

    /* Loop the rows */ 
    for ( i = 0; (i < BOARDHEIGHT) && !winner_found; i++ )
    {
        /* Go across each column */
        for ( j = 0; (j < BOARDWIDTH) && !winner_found; j++ )
        {
            /* If we've encountered this token before */
            if((board[i][j] == last_found) && (board[i][j] != C_EMPTY)) {
                conseq++;
            /* If token different to previous */
            } else {
                last_found = board[i][j];
                conseq = 1;
            }
            /* Found 4 in a row so we have a winner */
            if(conseq == CON4){
                winner_found = TRUE;
                return last_found;
            }
        }
        /* Reset when moving on to next row */
        last_found = -1;
        conseq = 0;
    }

    /* Didn't find a winner */
    return G_NO_WINNER;
}

/**
 * Scan through array vertically, checking the cell previous.
 **/
enum game_state checkVertical(enum cell_contents board[][BOARDWIDTH])
{ 
    int i, j;
    int conseq = 0;
    enum cell_contents last_found = -1;
    enum falsetrue winner_found = FALSE;

    /* Loop the columns */ 
    for ( i = 0; (i < BOARDWIDTH) && !winner_found; i++ )
    {
        /* Go down each row */
        for ( j = 0; (j < BOARDHEIGHT) && !winner_found; j++ )
        {
            /* If we've encountered this token before */
            if((board[j][i] == last_found) && (board[j][i] != C_EMPTY)) {
                conseq++;
            /* If token different to previous */
            } else {
                last_found = board[j][i];
                conseq = 1;
            }
            /* Found 4 in a row so we have a winner */
            if(conseq == CON4){
                return last_found;
            }
        }
        /* Reset when moving on to next row */
        last_found = -1;
        conseq = 0;
    }

    /* Didn't find a winner */
    return G_NO_WINNER;
}

/**
 * Scan through diaganolly, from left to right, in two parts.
 **/
enum game_state checkDiagLeft(enum cell_contents board[][BOARDWIDTH])
{ 
   int col = 0;
   int row = 0;
   int tempColumn;
   int tempRow;
   int conseq = 0;
   enum falsetrue winner_found = FALSE;
   enum cell_contents last_found = -1;

   /* first scan the top half */
   for(col=0; (col < BOARDWIDTH) && (row < BOARDHEIGHT) && !winner_found; col++) {
      
      tempColumn = col;

      for (row = 0; (tempColumn < BOARDWIDTH) && (row < BOARDHEIGHT) && !winner_found; row++) {

            /* If we've encountered this token before */
            if((board[row][tempColumn] == last_found) && (board[row][tempColumn] != C_EMPTY)) {
            conseq++;
            /* If token different to previous */
            } else {
                last_found = board[row][tempColumn];
                conseq = 1;
            }
            /* Found 4 in a row so we have a winner */
            if(conseq == CON4){
                winner_found = TRUE;
                return last_found;
            }
            tempColumn++;
      }
      last_found = -1;
      conseq = 0;
   }

   /* now go down the bottom side */
   for(row=1; (col < BOARDWIDTH) && (row < BOARDHEIGHT) && !winner_found;row++) {
      
      tempRow = row;
      
      for(col=0; (tempRow < BOARDHEIGHT ) && (col < BOARDWIDTH) && !winner_found; col++) {
           
            /* If we've encountered this token before */
            if((board[tempRow][col] == last_found) && (board[tempRow][col] != C_EMPTY)) {
            conseq++;
            /* If token different to previous */
            } else {
                last_found = board[tempRow][col];
                conseq = 1;
            }
            /* Found 4 in a row so we have a winner */
            if(conseq == CON4){
                winner_found = TRUE;
                return last_found;
            }
            tempRow++;
      }
      last_found = -1;
      conseq = 0;
   }

    /* Didn't find a winner */
   return G_NO_WINNER;
}

/**
 * Scan through diaganolly, from right to left, in two parts.
 **/
enum game_state checkDiagRight(enum cell_contents board[][BOARDWIDTH])
{ 
   int col = 0;
   int row = 0;
   int tempColumn;
   int tempRow;
   int conseq = 0;
   enum falsetrue winner_found = FALSE;
   enum cell_contents last_found = -1;

   /* first scan the top half */
   for(col=0; (col < BOARDWIDTH) && (row < BOARDHEIGHT) && !winner_found; col++) {
      
      tempColumn = col;

      for (row = 0; (-1 < tempColumn) && (row < BOARDHEIGHT) && !winner_found; row++) {

            /* If we've encountered this token before */
            if((board[row][tempColumn] == last_found) && (board[row][tempColumn] != C_EMPTY)) {
            conseq++;
            /* If token different to previous */
            } else {
                last_found = board[row][tempColumn];
                conseq = 1;
            }
            /* Found 4 in a row so we have a winner */
            if(conseq == CON4){
                winner_found = TRUE;
                return last_found;
            }
            tempColumn++;
      }
      last_found = -1;
      conseq = 0;
   }

   /* now go down the opposite side */
   for(row=1; (-1 < col) && (row < BOARDHEIGHT) && !winner_found;row++) {
      
      tempRow = row;
      
      for(col = BOARDHEIGHT; (tempRow < BOARDWIDTH ) && (-1 < col) && !winner_found; col--) {
           
            /* If we've encountered this token before */
            if((board[tempRow][col] == last_found) && (board[tempRow][col] != C_EMPTY)) {
            conseq++;
            /* If token different to previous */
            } else {
                last_found = board[tempRow][col];
                conseq = 1;
            }
            /* Found 4 in a row so we have a winner */
            if(conseq == CON4){
                winner_found = TRUE;
                return last_found;
            }
            tempRow++;
      }
      last_found = -1;
      conseq = 0;
   }

    /* Didn't find a winner */
   return G_NO_WINNER;
}

