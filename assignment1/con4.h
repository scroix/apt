/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "board.h"
#include "helpers.h"
#include "scoreboard.h"
#include "player.h"
#include "game.h"
#include "bool.h"
/**
 * @file con4.h simply puts together all the other header files for 
 * inclusion in @ref con4.c
 **/

#define FALSE 0
#define SIZE 1
