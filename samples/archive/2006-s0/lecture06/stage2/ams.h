/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Summer 2006 Assignment #2 - Academic Management System
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows - sdb@cs.rmit.edu.au
*****************************************************************************
* Documentation for structure definitions below:
*
* These structure definitions represent a two dimensional linked list
* structure. The AMSType structure has a linked list of courses and each
* CourseType structure has a linked list of student results.
*
* In the CourseType structure, the assignmentWeights variable points to
* a dynamically allocated array. There is one array position for each
* assignment task. The numAssignments variable represents the length of this
* array. All assignments weights and the exam weight added together
* must add up to 100.
*
* In the StudentResultType structure, the exam score and assignment scores
* for each student are stored. This structure also has a dynamically
* allocated array (for the assignmentScores variable). The exam score cannot
* exceed the course exam weight. Similarly, each assignment score cannot
* exceed the course assignment weights.
*
* Example:
* The COSC1098 course has an exam with weight 60, and 2 assignments with 
* weights 15 and 25. 60 + 15 + 25 = 100. In this example, each student in
* COSC1098 can get an exam score between 0-60, and assignment scores between
* 0-15 and 0-25 respectively.
****************************************************************************/

#ifndef AMS_H
#define AMS_H

/* System-wide header files. */
#include <stdio.h>
#include <stdlib.h>

/* System-wide constants. */
#define STUDENTID_MAX 8
#define STUDENTNAME_MAX 25
#define COURSECODE_LEN 8
#define COUSENAME_MAX 25

/* Structure definitions. */
typedef struct studentResult
{
   char studentID[STUDENTID_MAX + 1];
   char studentName[STUDENTNAME_MAX + 1];
   unsigned examScore;
   unsigned* assignmentScores;
   struct studentResult* nextResult;
} StudentResultType;

typedef struct course
{
   char courseCode[COURSECODE_LEN + 1];
   char courseName[COUSENAME_MAX + 1];
   unsigned examWeight;
   unsigned numAssignments;
   unsigned* assignmentWeights;
   struct course* nextCourse;
   struct studentResult* headResult;
   unsigned numResults;
} CourseType;

typedef struct ams
{
   struct course* headCourse;
   unsigned courseCount;
} AMSType;

#endif
