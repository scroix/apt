/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* Created December 2005
* COSC1098/1283/1284 Summer 2006 Exam Solutions
* Question 5 - 10 marks
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isPalindrome(char* string);
int printPalindromesFromFile(char* filename);

/* Function definition. (1 mark) */
int main(int argc, char** argv)
{
   /* Necessary variable. (1 mark) */
   int i;
   
   /* Check number of cmd line args. (2 marks) */
   if (argc <= 1)
   {
      printf("Error: Supply at least one filename on the command line.\n");
   }
   
   /* Looping construct. (3 marks) */
   for (i = 1; i < argc; i++)
   {
      /* Process a text file. (2 marks) */
      if (printPalindromesFromFile(argv[i]) == 0)
      {
         printf("Error: Could not open file '%s'\n", argv[i]);
      }
   }
   
   /* Return success. (1 mark) */
   return EXIT_SUCCESS;
}

int printPalindromesFromFile(char* filename)
{
   FILE* fp;
   char line[80];
   char* tok;
   
   if ((fp = fopen(filename, "r")) == NULL)
   {
      return 0;
   }

   while (fgets(line, 80, fp) != NULL)
   {
      tok = strtok(line, " \n");
      
      if (strlen(tok) == 0)
      {
         continue;
      }
      
      if (isPalindrome(tok) == 1)
      {
         printf("%s\n", tok);
      }
      
      while((tok = strtok(NULL, " \n")) != NULL)
      {
         if (isPalindrome(tok) == 1)
         {
            printf("%s\n", tok);
         }
      }
   }

   return 1;
}

int isPalindrome(char* string)
{
   int i, j;

   for (i = 0, j = strlen(string) - 1; i < strlen(string) / 2; i++, j--)
   {
      if (string[i] != string[j])
      {
         return 0;
      }
   }
   
   return 1;
}
