/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Julien de-Sainte-Croix
 * Student Number   : s3669242
 * Course Code      : COSC1076
 * Program Code     : BP096
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_menu.h"
/*not sure if this is a mistake! */
#include "ppd_options.h"
/**
 * @file ppd_menu.c handles the initialised and management of the menu 
 * array
 **/

/**
 * @param menu the menu item array to initialise
 **/
void init_menu( struct menu_item* menu)
{
       int i;
       const char *menu_text[] = {
           "Display Items", "Purchase Items", "Save and Exit", "Add Item",
           "Remove Item", "Display Stock", "Reset Stock", "Reset Coins", 
           "Abort Program" };

       BOOLEAN (*functions_array[MENU_SIZE])(struct ppd_system * system);
       functions_array[0] = display_items;
       functions_array[1] = purchase_item;
       functions_array[2] = save_system;
       functions_array[3] = add_item;
       functions_array[4] = remove_item;
       functions_array[5] = display_coins;
       functions_array[6] = reset_stock;
       functions_array[7] = reset_coins;
       functions_array[8] = NULL;
       
       /* assign functions to menu */ 
       for (i = 0; i < MENU_SIZE; i++)
       {    
           strcpy(menu[i].name, menu_text[i]);
           menu[i].function = functions_array[i];
       }
}

void display_menu( struct menu_item* menu)
{
    int i;
    
    printf("Main Menu:\n");

    for(i = 0; i < MENU_SIZE; i++) {
        
        printf("%*s%d. %s\n",3, " ", i + 1, menu[i].name);
        
        if(i == ADMIN_MENU_LOC)
        {
            printf("Administrator-Only Menu: \n");
        }
    }
}


