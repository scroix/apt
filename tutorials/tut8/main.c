/* Sample application using a list (of integers) module */
#include <stdio.h>
#include <stdlib.h>
#include "intArrayList.h"

int main()
{
    IntList il;
    int anInt;
    ListMake(&il);

    while (scanf("%d", &anInt) == 1)
    {
        if (!ListInsert(&il, anInt))
        {
            fprintf(stderr,"\nListInsert Failed! Aborting data entry!");
            break;
        }
    }
        ListDisplay(&il);
        ListFree(&il);
        return EXIT_SUCCESS;
}
