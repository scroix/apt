/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdlib.h>
#include "player.h"
#include "time.h"
#include "board.h"
#include "helpers.h"

#define CON4 4
/**
 * the various possibilities when we check for a winner
 **/
enum game_state
{
    /**
     * there was no winner (the game is still in progress
     * - set to -1 so the other values increment from 0
     **/
    G_NO_WINNER=-1,
    /**
     * the game was a draw - there are no valid moves that can be made
     **/
    G_DRAW,
    /**
     * the red token won the game
     **/
    G_RED,
    /**
     * the white token won the game
     **/
    G_WHITE
};

/**
 * Manages the game loop. See the documentation in the .c file for full
 * details. 
  **/
struct player * play_game(struct player *, struct player*);

/**
 * tests to see if the game has been won. A win is defined as four 
 * tokens in a row in any direction, however you should not wrap 
 * around the board.
 **/
enum game_state test_for_winner(
        enum cell_contents board[][BOARDWIDTH]);

enum game_state checkHorizontal(enum cell_contents board[][BOARDWIDTH]);

enum game_state checkDraw(enum cell_contents board[][BOARDWIDTH]);

enum game_state checkVertical(enum cell_contents board[][BOARDWIDTH]);

enum game_state checkDiagLeft(enum cell_contents board[][BOARDWIDTH]);

enum game_state checkDiagRight(enum cell_contents board[][BOARDWIDTH]);
