/**************************************************************************
 * created by Paul Miller for COSC1076 - Advanced Programming Techniques.
 * This material is provided to you for the purposes of learning about 
 * function pointers. For more information about the purpose of this 
 * please see the readme.txt file. If you have any questions about this 
 * program please post in the discussion board about it.
 *************************************************************************/
#include "filemanip.h"
/* clear the buffer when there is too much input */
void read_rest_of_line(void)
{
        int ch;
        while((ch = getc(stdin)), ch != EOF && ch != '\n')
                ;
        clearerr(stdin);
}

/* is this an even offset into the line */
BOOLEAN is_even(int i)
{
        /* we add 1 here as 0 is the first character index */
        return (i+1)%EVENNESS == 0;
}

int main(void)
{
        /* filenames used for this program */
        const char * infile = "input.txt";
        const char * output = "out.txt";
        char line[LINELEN + EXTRACHARS];
        enum choice choice;
        FILE * in, *out;
        /* array of function pointers */
        writefunc funcs[NUMOPTIONS] = 
        {
                every_second, reverse, random
        };
        /* array of strings */
        const char * options[NUMOPTIONS] = 
        {
                "write every second word to a file",
                "reverse each line of the file",
                "randomise each line of the file"
        };
        /* we get back from this function the option the 
         * user wishes to choose as output to the second 
         * file
         */
        /* try to open the files and exit if either fails */
        in = fopen(infile, "r");
        out = fopen(output, "w");
        if(in == NULL || out == NULL)
        {
                return EXIT_FAILURE;
        }
        choice = INVALID;
        /* get the choice of option from the user */
        while( (choice = display_menu_options(options)) && 
                        choice == INVALID)
        {
                fprintf(stderr, "Error: invalid input. "
                                " Please try again.\n");
        }
        /* read each line from the file */
        while(fgets(line, LINELEN + EXTRACHARS, in) != NULL)
        {
                if(line[strlen(line) - 1] != '\n')
                {
                        fprintf(stderr, "Error: line too long"
                                        " in input file.\n");
                        return EXIT_FAILURE;
                }
                line[strlen(line)-1]=0;
                /* process it with the choice of function */
                if(!funcs[choice](out, line))
                {
                        printf("processing failed.\n");
                        return EXIT_FAILURE;
                }
        }
        fprintf(out, "\n");
        fclose(in);
        fclose(out);
        return EXIT_SUCCESS;
}
/*
 * prints every second character in the string to a file
 **/
BOOLEAN every_second(FILE* fp, const char * line)
{
        int count;
        int len = strlen(line);
        /* iterate over the line */
        for(count = 0; count < len; ++count)
        {
                /* if it is an even offset into the line */
                if(is_even(count))
                {
                        /* print the character */
                        if(putc(line[count], fp) == EOF)
                        {
                                return FALSE;
                        }
                }
        }
        /* don't forget to print the newline */
        if(putc('\n', fp) == EOF)
                return FALSE;
        return TRUE;

}

/* reverses a line passed in */
BOOLEAN reverse(FILE* fp, const char * line)
{
        int count;
        /* cache the string length so we aren't repeatedly 
         * asking for it
         */
        int len = strlen(line);
        /* iterate over the line backwards */
        for(count = len -1; count >= 0; --count)
        {
                /* print the character to the file */
                if(putc(line[count], fp) == EOF)
                {
                        return FALSE;
                }
        }
        /* don't forget to print the newline */
        if(putc('\n', fp) == EOF)
        {
                return FALSE;
        }
        return TRUE;
}

/* shuffle a line then print it to a file */
BOOLEAN random(FILE* fp, const char * line)
{
        int count;
        int len = strlen(line);
        /* a copy of the line as the line passed in is const */
        char linecpy[LINELEN + 1];
        strcpy(linecpy, line);
        srand(time(NULL));
        /* iterate over the letters */
        for(count = 0; count < len; ++count)
        {
                /* find a random spot for swapping */
                int randletter = rand() % len;
                int temp;
                /* perform the swap */
                temp = linecpy[randletter];
                linecpy[randletter]=linecpy[count];
                linecpy[count]=temp;
        }
        /* put the line to the file */
        if(fputs(linecpy, fp) == EOF)
        {
                return FALSE;
        }
        return TRUE;
}

/* get the choice of the option from the user */
enum choice display_menu_options(const char *options[NUMOPTIONS])
{
        int count;
        char line[LINELEN + EXTRACHARS];
        char * end;
        enum choice choice;

        /* print out the menu */
        printf("Please select what you want to do with the file:\n");
        for(count = 0; count < NUMOPTIONS; ++count)
        {
                printf("%d: %s\n", count+1, options[count]);
        }
        /* get the user's choice */
        printf("what is your pleasure, sir/madam?: ");
        /* validate the line */
        if((fgets(line, LINELEN + EXTRACHARS, stdin)) == NULL)
                return INVALID;
        if(line[strlen(line)-1] != '\n')
        {
                read_rest_of_line();
                return INVALID;
        }
        /* remove the newline character */
        line[strlen(line)-1]=0;
        choice = strtol(line, &end, 0);
        if(*end)
        {
                return INVALID;
        }
        /* decrement the choice as array indices start at 0 */
        --choice;
        /* check it will be within the bound of the array */
        if(choice < 0 || choice >= INVALID)
        {
                return INVALID;
        }
        return choice;
}
