#include <stdlib.h>
#include <stdio.h>

int strtol_wrapper(const char *text)
{
    int value;
    char *ptr;

    value = strtol(text, &ptr, 10);

    if (*ptr != '\0') {
        fprintf(stderr, "Error: Ignoring invalid input '%s'\n", text);
        return 0;
    }

    return value;
}

int main (int argc, char **argv)
{
    int sum;
    int i;

    printf("binary name: %s\n", argv[0]);

    sum = 0;
    for (i = 1; i < argc; i++)
        sum += strtol_wrapper(argv[i]);

    printf("sum: %u\n", sum);

    return EXIT_SUCCESS;
}
