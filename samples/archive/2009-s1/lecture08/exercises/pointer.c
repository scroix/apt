#include <stdio.h>

int main(void)
{
   int num[] = {3, 4, 6, 2, 1};
   int *p = num;
   int *q = num + 2;
   int *r = &num[1];
   
   printf("\n%d %d", num[2], *(num + 2));
   printf("\n%d %d", *p, *(p + 1));
   printf("\n%d %d", *q, *(q + 1));
   printf("\n%d %d", *r, *(r + 1));

   return 0;
}
