/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Semester 2 2006 Assignment #2 - index program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows & Daryl D'Souza
****************************************************************************/

#include "index.h"
#include "index_options.h"
#include "index_utility.h"

/****************************************************************************
* Function readRestOfLine() is used for buffer clearing. Source: 
* https://inside.cs.rmit.edu.au/~sdb/teaching/C-Prog/CourseDocuments/
* FrequentlyAskedQuestions/#alternative
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}


/****************************************************************************
* Initialises the index to a safe empty state.
****************************************************************************/
void systemInit(IndexType* menu)
{
}


/****************************************************************************
* Loads all data into the index.
****************************************************************************/
int loadData(IndexType* index, char* trecFile)
{
   return SUCCESS;
}


/****************************************************************************
* Deallocates memory used in the index.
****************************************************************************/
void systemFree(IndexType* menu)
{
}
