/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 13/1/2006
* 
* driver.c
* Example of makefiles.
* Citation: Slide 6-16 of PP2A/PT course notes.
****************************************************************************/

#include "driver.h"

int main(void)
{
   /* Dummy function calls. */
   get();
   calcTotal();
   calcGrade();
   put();
   
   return EXIT_SUCCESS;
}
