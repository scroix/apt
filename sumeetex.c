#include <stdio.h>
#include <stdlib.h>


int main ()
{
   int num = 1;

   myfunc(&num);
 
   printf("Num is: %d\n", num );
    
   return 0;
}

void myfunc(int *pointer) {
  *pointer = 3;
  myfunc2(pointer);
}

void myfunc2(int *pointer) {
  *pointer = 5;
  
}

CONSOLE OUTPUT:

Hermes:Documents sumeet$ gcc main.c 
Hermes:Documents sumeet$ ./a.out 
Num is: 5