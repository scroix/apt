#include <stdlib.h>

#include "file1.h"
#include "file2.h"

int main()
{
    a_function_from_file1();

    a_function_from_file2();

    return EXIT_SUCCESS;
}
