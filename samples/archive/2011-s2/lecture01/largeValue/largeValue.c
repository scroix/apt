#include <stdio.h>

int main(void)
{
   unsigned long val = -1;
   char c1 = 256, c2 = 257;   /* too big */
   printf("c1 = %d\nc2 = %d\n", c1, c2);

   printf("The biggest integer value: %lu\n", val);

   return 0;

}
