/* Printing an address, or location
 * this example is taken from page 250 of "A Book on C"
 */ 

#include <stdio.h>

int main(void)
{
   int i = 7, *p = &i;
   
   printf("%s%d\n%s%p\n", "  Value of i: ", *p,
          "Location of i: ", p);
   return 0;
}
