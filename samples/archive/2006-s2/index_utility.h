/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Semester 2 2006 Assignment #2 - index program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows & Daryl D'Souza
****************************************************************************/

#ifndef INDEX_UTILITY_H
#define INDEX_UTILITY_H

/* Function prototypes. */
void readRestOfLine();
void systemInit(IndexType* menu);
int loadData(IndexType* index, char* trecFile);
void systemFree(IndexType* menu);

#endif
