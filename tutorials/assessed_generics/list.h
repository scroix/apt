#include <assert.h>
#include <stdlib.h>
#include "common.h"
#ifndef LIST_H
#define LIST_H
#define NAMELEN 20

struct list_node
{
        void * data; /* made a void pointer to hold any type of data */
        struct list_node * next;

};

struct list
{
        struct list_node * head;
        unsigned count;
        /* we assume that the data in each node is the same so it makes 
         * sense to store pointers to functions for comparison and printing
         */
        int (*insert_cmp)(void *, void*);
        int (*del_cmp)(void*, char*);
        void (*print)(void*);
        void (*datafree)(void*);
};

void list_init(struct list * list, int(*)(void*,void*), int(*)(void*,char*),
                void (*)(void*), void(*)(void*));
BOOLEAN list_add(struct list * list, void * data);
void list_print(struct list * list);
BOOLEAN list_remove(struct list * list, char * id);
void list_free(struct list * list);
#endif
