/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include "board.h"
#include "player.h"

/**
 * @file board.c contains implementations of functions related to the game 
 * board.
 **/

/**
 * Initalise the game board.
 **/
 void initialise_board(enum cell_contents board[][BOARDWIDTH])
{
    /* loop through array and declare everything as empty */
    int i, j;

    for (i = 0; i < BOARDHEIGHT; i++)
    {
        for (j = 0; j < BOARDWIDTH; j++)
        {
           board[i][j] = C_EMPTY;
        }
    }
     
}

/**
 * Display the game board. 
 **/
void display_board(enum cell_contents board[][BOARDWIDTH])
{
    int i, j;
   
    /* DISABLE THIS LINE FOR DEBUGGING */
    system("clear");

    printf("This is the current state of the board:\n\n");
    printf(" 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n");
    printf(" ---------------------------\n"); 

    /* loop through array changing contents displayed based on datatype stored */
    for (i = 0; i < BOARDHEIGHT; i++)
    {
        for (j = 0; j < BOARDWIDTH; j++)
        {
            if (board[i][j] == (C_WHITE))
            {
              printf(" %s ", WHITE_TOKEN);
              printf("|");
            }
            else if (board[i][j] == (C_RED))
            {
                printf(" %s ", RED_TOKEN);
                printf("|");
            }
            else
            {
                printf("   ");
                printf("|");
            }
        }
        printf("\n ---------------------------\n");      
    }
}

