
/*
 * This is an example developed during the afternoon lecture
 * of 13th Sept. 2005 to simply demonstrate some basic concepts
 * of linked list construction and traversal. It is an incomplete
 * example, and represents just the first step towards writing a
 * set of functions to make, add, display, and free a linked list.
 */

#include <stdio.h>
#include <stdlib.h>

/*
   Linked Lists consist of a "head of the list" variable
   which points to the first node in the linked list.
   All nodes are dynamically allocated (and therefore have
   no variable name as such). Each node contains some data
   plus a pointer to the next node in the list. The last
   node in the list contains a NULL pointer to indicate it 
   is the last node in the list 

   head   (node#1)   (node#2)
   +---+  +-------+  +----------+	data == data stored in node
   | ---->| 2 | ---->| 1 | NULL |       next == pointer to next node
   +---+  +-------+  +----------+
          data|next  data|next

*/

typedef struct listnode * ListNodePtr;  /* using a forward reference */
 
typedef struct listnode
{
   int data;
   ListNodePtr next;
} ListNode;

int main(void)
{
   ListNodePtr head, current;

   head = malloc(sizeof(ListNode));
   if (head == NULL)
   {
      fprintf(stderr, "Memory allocation failure. Program terminating.\n");
      return EXIT_FAILURE;
   }

   head->data=2;
   head->next=NULL;

   /* second node */

   head->next = malloc(sizeof(ListNode));
   if (head->next == NULL)
   {
      fprintf(stderr, "Memory allocation failure. Program terminating.\n");
      return EXIT_FAILURE;
   }

   head->next->data=1;
   head->next->next=NULL;
  

   for(current=head; current != NULL; current=current->next)
   {
      printf("%d\n", current->data);
   }


   /* do other things with linked list */

   /* code to free the list goes here */
 
   return EXIT_SUCCESS;
}






