/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 20/1/2006
* 
* example2.c
* Example of command line args and pointer arithmetic.
* What does this program print when invoked as follows:
* ./example2 command line arguments are great fun
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
   printf("a '%s'\n", *argv);
   printf("b '%s'\n", argv[0]);
   printf("c '%s'\n", *argv + 1);
   printf("d '%s'\n", *(argv + 1));
   printf("e '%s'\n", argv[0] + 1);
   printf("f '%c'\n", **argv);
   printf("g '%c'\n", *argv[0]);
   printf("h '%c'\n", argv[0][0]);
   printf("i '%c'\n", *argv[2] + 1);
   printf("j '%c'\n", *(argv[2] + 1));

   return EXIT_SUCCESS;
}
