/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 11/1/2006
* 
* macro.c
* Example of a simple macro.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define SQ(x) ((x) * (x))

int main(void)
{
   int num = 3;
   int square;
   
   square = SQ(3);
   
   printf("Square of %d is %d\n", num, square);
   
   return EXIT_SUCCESS;
}
