/* intArrayList.h */
#define LISTSIZE 32

struct intList
{
    int list[LISTSIZE];
    unsigned size;
};

typedef struct intList IntList;
void ListMake(IntList *);
int ListInsert(IntList *, int);
void ListDisplay(IntList *);
void ListFree(IntList *);
