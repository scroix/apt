/* exercise 6 
 * from page 312 of "A Book on C"
 */
#include <stdio.h>

int main(void)
{
   int a=1, b=2, c=3;
   char *p, *q, *r;
   int d[3] = {1,2,3};
   p = &a; 
   printf("%s%p\n%s%p\n%s%p\n",
         "&a = ", &a,
         "&b = ", &b,
         "&c = ", &c);
   printf("%s%p\n%s%lu\n%s%lu\n",
         "p = ", p,
         "q = ", q,
         "r = ", r);
  
   printf("%s%lu\n", "&d[0] = ",d);
   printf("%s%lu\n", "&d[1] = ",d+1);
   printf("%s%lu\n", "&d[2] = ",d+2);

   return 0;
}
