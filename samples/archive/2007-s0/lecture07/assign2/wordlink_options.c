/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #2 - word link program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows
****************************************************************************/

#include "wordlink.h"
#include "wordlink_options.h"
#include "wordlink_utility.h"

/****************************************************************************
* Menu option #1: 1 Player Game
* Game between human and computer controlled players.
* 
* Note: Additional modularisation is strongly recommended here.
* Extra candidate functions include playerGuess(), resetGame(), etc.
****************************************************************************/
void onePlayerGame(DictionaryType* dictionary)
{
}


/****************************************************************************
* Menu option #2: 2 Player Game
* Game between two human-controlled players.
* 
* Note: Additional modularisation is strongly recommended here.
* Extra candidate functions include playerGuess(), resetGame(), etc.
****************************************************************************/
void twoPlayerGame(DictionaryType* dictionary)
{
}


/****************************************************************************
* Menu option #3: Dictionary Summary
* Displays statistics on the words in the dictionary.
****************************************************************************/
void dictionarySummary(DictionaryType* dictionary)
{
}


/****************************************************************************
* Menu option #4: Search Dictionary
* Prompts the user for a dictionary word and reports if the word is in the
* dictionary.
****************************************************************************/
void searchDictionary(DictionaryType* dictionary)
{
}


/****************************************************************************
* Menu option #5: Add Dictionary Word
* Prompts the user for a new dictionary word to be added to the data
* structure.
****************************************************************************/
void addDictionaryWord(DictionaryType* dictionary)
{
}


/****************************************************************************
* Menu option #6: Save Dictionary
* Writes the contents of the dictionionary back to the original file.
****************************************************************************/
int saveDictionary(DictionaryType* dictionary, char* filename)
{
   return SUCCESS;
}
