/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 25/1/2006
* 
* intlist-main.c
* Example of an interface for an integer array.
* Citation: Adapted from slides 7-8 to 7-10 of PT lecture notes.
****************************************************************************/

/* 
One of these lines is needed if not supplied in the makefile:
#define INTLIST "intlist-array.h"
#define INTLIST "intlist-dyn-array.h"
#define INTLIST "intlist-linked-list.h"
*/

/* file: intlist-main.c
 */
#include <stdio.h>
#include <stdlib.h>

#ifdef INTLIST  
#include INTLIST
#else
#error INTLIST must be defined with the quoted string header file name
#endif

#define SIZE 10

int main(void)
{
   IntList il;
   int i;

   if (MakeList(&il, SIZE) == FAILURE)
   {
      fprintf(stderr, "MakeList(): failed\n");
      return EXIT_FAILURE;
   }

   /* Fill the IntList with random numbers. */
   for(i=0; i<SIZE; i++)
   {
      if (AddList(&il, rand() % 10) == FAILURE)
      {
         fprintf(stderr, "AddList(): failed\n");
         break;
      }
   }

   printf("IntList size is %u\n", SizeList(&il));

   DisplayList(&il);

   FreeList(&il);

   return EXIT_SUCCESS;
}
