#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#define N 10

void create_array_copy_by_value(int *x)
{
    printf("%s\n", __func__);
    printf("x addr: %p\n", (void*)&x);
    printf("x value: %p\n\n",  (void*)x);
    printf("IMPORTANT NOTE The value of x is the "
           "/value/ of main 'array' pointer.\n\n");

    x = malloc(N * sizeof(int));
    assert(x);
}

void create_array_copy_by_reference(int **x)
{
    printf("%s\n", __func__);
    printf("x addr: %p\n",  (void*)&x);
    printf("x value: %p\n\n",  (void*)x);
    printf("IMPORTANT NOTE The value of x is the "
           "/address/ of main 'array' pointer.\n");

    *x = malloc(N * sizeof(int));
    assert(*x);
}

int main()
{
    int *array = NULL;

    printf("%s\n", __func__);
    printf("array addr: %p\n",  (void*)&array);
    printf("array value: %p\n\n",  (void*)array);

    create_array_copy_by_value(array);

    assert(array == NULL);

    create_array_copy_by_reference(&array);

    assert(array != NULL);

    free(array);

    return EXIT_SUCCESS;
}
