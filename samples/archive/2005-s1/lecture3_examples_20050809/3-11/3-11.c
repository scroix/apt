/* String functions from <string.h> */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR_LEN 20

int main(void)
{
   char  string1[MAX_STR_LEN + 1] = "hello ";
   char  string2[MAX_STR_LEN + 1] = {'w', 'o', 'r', 'l', 'd', '\0'};
   char* string3 = "again";

   /* strcpy() example. */
   strcpy(string2, string3);
   printf("strcpy: %s %s\n", string2, string3);
   
   /* strcmp() example. */
   if (strcmp(string1, string2) == 0)
   {
      printf("strcmp: strings are the same\n");
   }
   else
   {
      printf("strcmp: strings are NOT the same\n");
   }
   
   /* strcat() example. */
   strcat(string1, string2);
   printf("strcat: %s\n", string1);

   return EXIT_SUCCESS;
}
