/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 3/1/2006
* 
* doWhile.c
* Simple do-while-loop examples.
* Citation: Adapted from slide 2-20 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int j;
   
   j = 10;
   do
   {
      printf("%2d ", j);
      j--;
   } while (j > 0);
   printf("\n");

   j = 10;
   do
   {
      printf("%2d ", j);
   }
   while (j-- > 0);
   printf("\n");

   j = 10;
   do
   {
      printf("%2d ", j);
   }
   while (j--);
   printf("\n");

   return EXIT_SUCCESS;
}
