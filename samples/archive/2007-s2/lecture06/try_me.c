/* see exercise 1 on page396 of "A Book on C" */ 
#include <stdio.h>

/* #define PRN(x) printf("x\n") */
#define PRN(x) printf(#x "\n") 

int main(void)
{
  PRN(Hello from main());
  return 0;
}
