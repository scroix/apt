
#include "common.h"
#include "list.h"
#ifndef COLOR
#define COLOR
/* the length of a color row id */
#define C_ID_LEN 2
/* the maximum length of a name */
#define C_NAME_LEN 50
/* the maximum length of a color */
#define C_COLOR_LEN 50
/* the maximum length of an age */
#define C_AGE_LEN 2
/* how many delimiters per line ? */
#define C_NUM_DELIMS 3
/* how many tokens per line */
#define C_NUM_TOKENS 4
/* the maximum length of a line */
#define C_LINE_LEN C_ID_LEN + C_COLOR_LEN + C_AGE_LEN + C_NUM_TOKENS
/* an enumeration of the tokens for each line - I have an invalid value at
 * the end which is one larger than the largest token allowed
 */
enum c_tokens
{
        C_ID_TOK, C_NAME_TOK, C_COLOR_TOK, C_AGE_TOK, C_INVALID_TOK
};

/* each row of the color file will have and id, name, color and age */
struct color_row
{
        char id[C_ID_LEN + 1];
        char name[C_NAME_LEN + 1];
        char color[C_COLOR_LEN + 1];
        int age;
};

struct color_row * make_color_node(const char * , const char * , 
                const char * , int );
struct color_row * parse_color_row(const char *);
BOOLEAN read_color_file(const char *, struct list *);
int color_cmp(void *, void *);
int del_color_cmp(void*, char*);
void color_print(void*);
void color_free(void*);
#endif
