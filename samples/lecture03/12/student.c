/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 5/1/2006
* 
* student.c
* A simple but poor example of how to use structures.
* Citation: Adapted from slide 3-12 of PT course notes.
****************************************************************************/

#include "student.h"

int main(void)
{
   struct studentRecord student1, student2;
   
   return EXIT_SUCCESS;
}

float calcTotal(int ass1, int ass2, int exam)
{
   float total = 0.0;

   return total;
}

char calcGrade(float total)
{
   char grade = '\0';

   return grade;
}
