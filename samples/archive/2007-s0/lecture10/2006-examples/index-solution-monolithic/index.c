/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 24/1/2006
* 
* index.c
* Practical example of constructing a simple index using a linked list.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 80
#define WORD_LEN 20
#define SUCCESS 1
#define FAILURE 0
#define FILENAME "data.txt"
#define DELIM " \n\r"
#define PAGE "<PAGE>"

typedef struct pageStruct
{
	int page;
	struct pageStruct* nextPage;
} PageType;

typedef struct wordStruct
{
   char word[WORD_LEN + 1];
   int count;
   struct wordStruct* next;
	struct pageStruct* headPage;
} WordType;

typedef struct indexStruct
{
   int distinctWords;
   struct wordStruct* head;
} IndexType;

int loadIndex(IndexType* index, char* filename);
void displayIndex(IndexType* index);
int addPage(WordType* wordPtr, int page);

int main(void)
{
   IndexType index;
   
   if (loadIndex(&index, FILENAME) == FAILURE)
   {
      return EXIT_FAILURE;
   }
   
   displayIndex(&index);
   
   return EXIT_SUCCESS;
}

int loadIndex(IndexType* index, char* filename)
{
   FILE* fp;
   char line[LINE_LEN + 1];
   char* tok;
   WordType *new = NULL, *curr = NULL, *prev = NULL;
   int found;
	int page = 1;
   
   if ((fp = fopen(filename, "r")) == NULL)
   {
      printf("Could not open file %s.\n", filename);
      return FAILURE;
   }
   
   while (fgets(line, LINE_LEN + 1, fp) != NULL)
   {
      tok = strtok(line, DELIM);

		/* New page found? */
		if (strcmp(tok, PAGE) == 0)
		{
			page++;
			continue;
		}
      
      while (tok != NULL)
      {
         curr = index->head;
         prev = NULL;
         found = FAILURE;
         
         while (curr != NULL)
         {
            if (strcmp(curr->word, tok) == 0)
            {
               found = SUCCESS;
               break;
            }
            prev = curr;
            curr = curr->next;
         }
         
         if (found == SUCCESS)
         {
            curr->count++;
				if (addPage(curr, page) == FAILURE)
				{
					return FAILURE;
				}
         }
         else
         {
            if ((new = malloc(sizeof(WordType))) == NULL)
            {
               fprintf(stderr, "Could not allocate %d bytes of memory.\n",
                       sizeof(WordType));
               return FAILURE;
            }

            strcpy(new->word, tok);
            new->count = 1;
				new->headPage = NULL;

            if (prev == NULL)
            {
               new->next = index->head;
               index->head = new;
            }
            else
            {
               prev->next = new;
               new->next = curr;
            }

				if (addPage(new, page) == FAILURE)
				{
					return FAILURE;
				}
         }

         tok = strtok(NULL, DELIM);
      }
   }
   
   return SUCCESS;
}

void displayIndex(IndexType* index)
{
   WordType* curr = index->head;
	PageType* currPage = NULL;
   
   while (curr != NULL)
   {
      printf("Count: %2d   Word: %-20s\n", curr->count, curr->word);

		currPage = curr->headPage;
		printf("   -> Pages: ");
		while (currPage != NULL)
		{
			printf("%d ", currPage->page);
			currPage = currPage->nextPage;
		}
		putchar('\n');

      curr = curr->next;
   }
}

int addPage(WordType* wordPtr, int page)
{
	PageType *new = NULL, *curr = NULL, *prev = NULL;

	curr = wordPtr->headPage;

	while (curr != NULL)
	{
		prev = curr;
		curr = curr->nextPage;
	}

   if (prev == NULL)
	{
		if ((new = malloc(sizeof(PageType))) == NULL)
		{
			fprintf(stderr, "Could not allocate %d bytes of memory.\n", 
				     sizeof(PageType));
			return FAILURE;
		}

		new->page = page;
		new->nextPage = NULL;
		wordPtr->headPage = new;
	}
	else if (prev->page != page)
	{
		if ((new = malloc(sizeof(PageType))) == NULL)
		{
			fprintf(stderr, "Could not allocate %d bytes of memory.\n", 
				     sizeof(PageType));
			/* Memory leaks here: */
			exit(EXIT_FAILURE);
		}

		new->page = page;
		new->nextPage = NULL;
		prev->nextPage = new;
	}

	return SUCCESS;
}
