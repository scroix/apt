#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "container.h"
#include "person.h"

struct container {
    struct person *head;
    size_t size;
};

struct container *container_new()
{
    struct container *new_container;

    new_container = malloc(sizeof(struct container));
    if (new_container == NULL) {
        perror("container_new: malloc");
        exit(EXIT_FAILURE);
    }

    new_container->head = NULL;

    new_container->size = 0;

    return new_container;
}

int container_insert(struct container *container, const char *name, int age)
{
    struct person *new;

    struct person *prev;
    struct person *curr;

    assert(container != NULL);

    new = person_new(name, age);

    /* linked list insertion (week 8 tutorial) */

    prev = NULL;
    curr = container->head;

    while (curr != NULL && person_cmp(curr, new) < 0) {
        prev = curr;

        curr = curr->next;
    }

    if (prev == NULL) /* we are still at the head node */
        container->head = new;
    else /* in middle somewhere of at end */
        prev->next = new;

    new->next = curr;

    container->size++;

    return 0;
}

void container_print(struct container *container)
{
    struct person *curr;

    assert(container != NULL);

    curr = container->head;

    while (curr != NULL) {
        person_print(curr, stdout);

        curr = curr->next;
    }
}

void container_free(struct container *container)
{
    struct person *curr;
    struct person *next;

    assert(container != NULL);

    curr = container->head;

    while (curr != NULL) {
        next = curr->next;

        person_free(curr);

        curr = next;
    }

    free(container);
}

size_t container_size(struct container *container)
{
    assert(container != NULL);

    return container->size;
}
