/**************************************************************************
 * created by Paul Miller for COSC1076 - Advanced Programming Techniques.
 * This material is provided to you for the purposes of learning about 
 * function pointers. For more information about the purpose of this 
 * please see the readme.txt file. If you have any questions about this 
 * program please post in the discussion board about it.
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* define the BOOLEAN type */
typedef enum
{
        FALSE, TRUE
} BOOLEAN;

#define NUMOPTIONS 3
#define LINELEN 80
#define EXTRACHARS 2
#define EVENNESS 2

enum choice
{
        EVERY_SECOND, REVERSE, RANDOM, INVALID
};
/* function that processes the file */
BOOLEAN process(const char * infile_name, 
                const char * outfile_name);
/* typedef that creates a writefunc type which is a function
 * pointer that points to one of the below functions 
 */
typedef BOOLEAN (*writefunc)(FILE*, const char *);
/* functions that perform various writing tasks */
BOOLEAN every_second(FILE*, const char *);
BOOLEAN reverse(FILE*, const char *);
BOOLEAN random(FILE*, const char *);
enum choice display_menu_options(const char *[NUMOPTIONS]);
