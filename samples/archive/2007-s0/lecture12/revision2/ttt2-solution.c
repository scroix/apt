/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #1 - tic tac toe program
* Sample Exam Questions
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define ROW_LEN 3
#define COL_LEN 3
#define TRUE 1
#define FALSE 0
#define NAUGHT 'O'
#define CROSS 'X'
#define CELL_MIN 1
#define CELL_MAX 9

char** allocateGrid();

int main(void)
{
   char** tttGrid;
   int row, col;

   tttGrid = allocateGrid();

   /* Initialise grid with some data for testing. */
   tttGrid[0][0] = '1';
   tttGrid[0][1] = '2';
   tttGrid[0][2] = CROSS;
   tttGrid[1][0] = '4';
   tttGrid[1][1] = CROSS;
   tttGrid[1][2] = CROSS;
   tttGrid[2][0] = '7';
   tttGrid[2][1] = NAUGHT;
   tttGrid[2][2] = NAUGHT;
   
   /* Print the grid */
   for (row = 0; row < ROW_LEN; row++)
   {
      for (col = 0; col < COL_LEN; col++)
      {
         printf("%c", tttGrid[row][col]);
      }
      putchar('\n');
   }
   
   return EXIT_SUCCESS;
}

/* Function definition. (1 mark) */
char** allocateGrid()
{
   /* Variables. (2 marks) */
   char** newMemory;
   int row;
   
   /* Allocate memory for rows. (4 marks) */
   if ((newMemory = calloc(ROW_LEN, sizeof(char*))) == NULL)
   {
      return NULL;
   }
   
   /* Allocate memory for columns in rows. (7 marks) */
   for (row = 0; row < ROW_LEN; row++)
   {
      if ((newMemory[row] = calloc(COL_LEN, sizeof(char))) == NULL)
      {
         return NULL;
      }
   }
   
   /* Return new memory. (1 mark) */
   return newMemory;
}
