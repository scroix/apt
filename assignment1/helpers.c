/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include "helpers.h"

/**
 * @file helpers.c contains various functions that help in the
 * implementation of the program. You should put functions you create
 * here unless they logically belong to another module (such as
 * player, board or game
 **/

void read_rest_of_line(void)
{
    int ch;
    while(ch = getc(stdin), ch!=EOF && ch != '\n')
        ; /* gobble each character */
    /* reset the error status of the stream */
    clearerr(stdin);
}

/*******************************
* Original code adapted from:
*
* Steven Burrows
* sdb@cs.rmit.edu.au
* getString-basic.c
********************************/
int getString(char* string, unsigned length, char* prompt)
{
   int finished = FALSE;
   char tempString[STRING_TEMP_LENGTH + 2];

   /* Continue to interact with the user until the input is valid. */
   do
   {
       /* Provide a custom prompt. */
       printf("%s", prompt);

       /* Accept input. "+2" is for the \n and \0 characters. */
       if (fgets(tempString, length + 2, stdin) == NULL) {
           return RTM;
       }

       /* A string that doesn't have a newline character is too long */
       if (tempString[0] == '\n')
       {
           return RTM;
       }
       else if (tempString[strlen(tempString) - 1] != '\n')
       {
           printf("Input was too long.\n");
           read_rest_of_line();
       }
       else
       {
           finished = TRUE;
       }

   } while (finished == FALSE);

   /* Overwrite the \n character with \0. */
   tempString[strlen(tempString) - 1] = '\0';

   /* Make the result string available to calling function. */
   strcpy(string, tempString);

   return SUCCESS;
}

/**********************************
* Original code adapted from:
*
* Steven Burrows
* sdb@cs.rmit.edu.au
* getInteger-basic.c
***********************************/
int getInteger(int* integer, unsigned length, char* prompt, int min, int max)
{
    int finished = FALSE;
    char tempString[INT_TEMP_LENGTH + 2];
    int tempInteger = 0;
    char* endPtr;

    /* Continue to interact with the user until the input is valid. */
    do
    {
        /* Provide a custom prompt. */
        printf("%s", prompt);

        /* Accept input. "+2" is for the \n and \0 charaters. */
        if(fgets(tempString, length + 2, stdin) == NULL) {
            return RTM;
        }

        /* A string that doesn't have a newline character is too long. */
        if (tempString[0] == '\n')
        {
            return RTM;
        }
        else if (tempString[strlen(tempString) - 1] != '\n')
        {
            printf("Input was too long.\n");
            read_rest_of_line();
        }
        else
        {
            /* Overwrite the \n character with \0. */
            tempString[strlen(tempString) - 1] = '\0';

            /* Convert string to an integer. */
            tempInteger = (int) strtol(tempString, &endPtr, 10);

            /* Validate integer result. */
            if (strcmp(endPtr, "") != 0)
            {
                printf("Input was not numeric.\n");
            }
            else if (tempInteger < min || tempInteger > max)
            {
                printf("Input was not within range %d - %d.\n", min, max);
            }
            else
            {
                finished = TRUE;
            }
        }
    } while (finished == FALSE);

    /* Make the result integer available to calling function. */
    *integer = tempInteger;

    return SUCCESS;
}
