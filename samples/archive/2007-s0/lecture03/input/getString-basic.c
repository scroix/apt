/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* getString-basic.c
*
* This program demonstrates a reusable function for accepting strings.
* If you wish to use this code, you must acknowledge it for plagiarism 
* reasons.
*
* Created June 2006.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_LENGTH 5
#define PROMPT_LENGTH 80
#define TRUE 1
#define FALSE 0
#define SUCCESS 1
#define FAILURE 0
#define TEMP_STRING_LENGTH 5

void readRestOfLine();
int getString(char* string, unsigned length, char* prompt);

int main(void)
{
   char string[INPUT_LENGTH + 1];
   char prompt[PROMPT_LENGTH + 1];

   sprintf(prompt, "Enter a string (max %d characters): ", INPUT_LENGTH);

   getString(string, INPUT_LENGTH, prompt);

   printf("You entered: '%s'\n", string);

   return EXIT_SUCCESS;
}


/****************************************************************************
* getString(): An interactive string input function.
* This function prompts the user for a string using a custom prompt. A line
* of text is accepted from the user using fgets() and stored in a temporary
* string. When the function detects that the user has entered too much text,
* an error message is given and the user is forced to reenter the input. The
* function also clears the extra text (if any) with the readRestOfLine()
* function.
* When a valid string has been accepted, the unnecessary newline character
* at the end of the string is overwritten. Finally, the temporary string is
* copied to the string variable that is returned to the calling function.
****************************************************************************/
int getString(char* string, unsigned length, char* prompt)
{
   int finished = FALSE;
   char tempString[TEMP_STRING_LENGTH + 2];

   /* Continue to interact with the user until the input is valid. */
   do
   {
      /* Provide a custom prompt. */
      printf("%s", prompt);
      
      /* Accept input. "+2" is for the \n and \0 characters. */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if (tempString[strlen(tempString) - 1] != '\n')
      {
         printf("Input was too long.\n");
         readRestOfLine();
      }
      else
      {
         finished = TRUE;
      }

   } while (finished == FALSE);

   /* Overwrite the \n character with \0. */
   tempString[strlen(tempString) - 1] = '\0';
   
   /* Make the result string available to calling function. */
   strcpy(string, tempString);

   return SUCCESS;
}


/****************************************************************************
* readRestOfLine(): A function for buffer handling.
* This function reads remaining characters in the standard input buffer
* up to the next newline or EOF. Source:
* https://inside.cs.rmit.edu.au/~sdb/teaching/C-Prog/CourseDocuments/
* FrequentlyAskedQuestions/#alternative
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* read until the end of the line or end-of-file */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* clear the error and end-of-file flags */
   clearerr(stdin);
}
