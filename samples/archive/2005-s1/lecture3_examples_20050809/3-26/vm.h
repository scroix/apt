/****************************************************************************
* COSC1097/1098/1283 - Assignment #2 - Vending Machine
* Programming Principles 2A/Programming Techniques
* Author           : <insert name here>
* Student Number   : <insert student number here>
* Yallara Username : <insert username here>
* Start up code provided by Steven Burrows - stburrow@cs.rmit.edu.au
****************************************************************************/

#ifndef _VM_H
#define _VM_H

/* System-wide header files. */
#include <stdio.h>
#include <stdlib.h>

/* Constants. */
#define COIN_DEFAULT 5
#define STOCK_DEFAULT 5
#define PRODUCT_NAME_MAX 40
#define PRODUCT_BRAND_MAX 20
#define DISTINCT_COINS 6
/* Constants for coin array indexes. */
#define TWO_DOLLAR 0
#define ONE_DOLLAR 1
#define FIFTY_CENTS 2
#define TWENTY_CENTS 3
#define TEN_CENTS 4
#define FIVE_CENTS 5

/* Structure definitions. */
typedef struct coin
{
   unsigned qty;
   unsigned value;  /* Stored in cents (not dollars). */
} CoinType;

typedef struct productNode
{
   char name[PRODUCT_NAME_MAX + 1];
   char brand[PRODUCT_BRAND_MAX + 1];
   unsigned price;  /* Stored in cents (not dollars). */
   unsigned qty;
   struct productNode* nextProduct;
} ProductNodeType;

typedef struct vendingMachine
{
   CoinType coins[DISTINCT_COINS];
   unsigned totalCoins;
   ProductNodeType* headProduct;
   unsigned totalProducts;
} VendingMachineType;

#endif
