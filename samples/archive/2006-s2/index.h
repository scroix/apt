/****************************************************************************
* COSC1098/1283/1284 - Programming Principles 2A/Programming Techniques
* Semester 2 2006 Assignment #2 - index program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows & Daryl D'Souza
****************************************************************************/

#ifndef INDEX_H
#define INDEX_H

/* System-wide header files. */
#include <stdio.h>
#include <stdlib.h>

/* System-wide constants. */
#define SUCCESS 1
#define FAILURE 0

/* Structure definitions. */
typedef struct wordStruct* WordTypePtr;

typedef struct wordStruct
{
   char* word;
   unsigned count;
   WordTypePtr nextWord;
} WordType;

typedef struct indexStruct
{
   WordTypePtr headWord;
   WordTypePtr longestWord;
   WordTypePtr mostCommonWord;
   unsigned totalWords;
   unsigned uniqueWords;
} IndexType;

#endif
