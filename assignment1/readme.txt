/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/

 - There is a system("clear") included in as part of the display_board function in board.c:44, disable this for debugging purposes.
 - Beware double-tapping the return key, as part of fulfilling the Return To Menu requirement means it's very easy to return back to the menu accidently.