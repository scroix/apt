/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 2/1/2006
* 
* sizeof.c
* Experimenting with sizeof() operator.
* Citation: Adapted from slide 1-25 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
 
int main(void)
{
    int x;
    char c;
    float f;
    double d;

    printf("Size of type int = %d bytes\n", sizeof(x));   
    /* nb. sizeof(int) is fine too ie. a type name */
    printf("Size of type char = %d bytes\n", sizeof(c)); 
    printf("Size of type float = %d bytes\n", sizeof(f)); 
    printf("Size of type double = %d bytes\n", sizeof(d));   

    return EXIT_SUCCESS;
}
