/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 24/1/2007
* 
* index.c
* Practical example of constructing a simple index using a linked list.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 80
#define WORD_LEN 20
#define SUCCESS 1
#define FAILURE 0
#define FILENAME "data.txt"
#define DELIM " \n\r"

typedef struct wordStruct
{
   unsigned count;
   char word[WORD_LEN + 1];
   struct wordStruct* next; 
} WordType;

typedef struct indexStruct
{
   unsigned distinctWords;
   unsigned totalWords;
   struct wordStruct* head; 
} IndexType;

void initIndex(IndexType* index);
int loadIndex(IndexType* index, char* filename);
int addWord(IndexType* index, char* word);
void displayIndex(IndexType* index);

int main(void)
{
   IndexType index;
  
   initIndex(&index);
 
   if (loadIndex(&index, FILENAME) == FAILURE)
   {
      return EXIT_FAILURE;
   }
   
   displayIndex(&index);
   
   return EXIT_SUCCESS;
}

void initIndex(IndexType* index) 
{
   index->head = NULL;
   index->totalWords = 0;
   index->distinctWords = 0;
}

int loadIndex(IndexType* index, char* filename)
{
   FILE* fp;
   char line[LINE_LEN + 1];
   char* tok;

   if ((fp = fopen(filename, "r")) == NULL)
   {
      fprintf(stderr, "Could not open file %s.\n", filename);
      return FAILURE;
   }    

   while (fgets(line, LINE_LEN + 1, fp) != NULL)
   {
      tok = strtok(line, DELIM);

      while (tok != NULL)
      {
         addWord(index, tok); 
         tok = strtok(NULL, DELIM);
      }
   } 

   fclose(fp);

   return SUCCESS;
}

int addWord(IndexType* index, char* word)
{
   WordType *curr, *new;

   curr = index->head;
   new = NULL;

   while (curr != NULL && strcmp(curr->word, word) != 0)
   {
      curr = curr->next; 
   }

   if (curr != NULL)
   {
      curr->count++;
   }
   else
   {
      if ((new = malloc(sizeof(WordType))) == NULL)
      {
         fprintf(stderr, "Could not allocate %d bytes.\n", sizeof(WordType));
         return FAILURE; 
      }  

      strcpy(new->word, word);
      new->count = 1;
      
      new->next = index->head;
      index->head = new;
   }

   index->totalWords++; 

   return SUCCESS;
}

void displayIndex(IndexType* index)
{
   WordType* curr = index->head;

   while (curr != NULL)
   {
      printf("Word: %-20s Count: %2d\n", curr->word, curr->count);
      curr = curr->next;
   }
}
