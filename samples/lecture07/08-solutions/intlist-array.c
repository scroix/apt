/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 17/1/2006
* 
* intlist-array.c
* Example of an interface for an integer array.
* Citation: Adapted from slides 7-15 to 7-17 of PT lecture notes.
****************************************************************************/

/* file: intlist-array.c
 *
 * IntList -- simple unordered array implementation
 */
 
#include <stdio.h>
#include "intlist-array.h"

int MakeList(IntList* pil,int size)
{
   if (size > INTLISTSIZE)
   {
      return FAILURE;
   }
   pil->size = 0;
   return SUCCESS;
}

void FreeList(IntList* pil)
{
   pil->size = 0;
}

int AddList(IntList* pil,int num)
{
   if (pil->size >= INTLISTSIZE)
   {
      return FAILURE;
   }
   pil->array[pil->size] = num;
   pil->size += 1;
   return SUCCESS;
}

void DisplayList(IntList* pil)
{
   int i, size, *array;

   size = pil->size;
   array = pil->array;

   for(i=0; i<size; i++)
   {
      printf("%d\n", array[i]);
   }
}

unsigned SizeList(IntList* pil)
{
   return pil->size;
}





/* Lecture 7 practice question. */
void SelectionSortList(IntList* pil)
{
   int i, j, smallPos, temp;
   
   for (i = 0; i < pil->size - 1; i++)
   {
      /* Assume that the smallest integer is the first one in the 
       * unsorted portion of the array. */
      smallPos = i;
      
      /* Search for the smallest integer. */
      for (j = i; j < pil->size; j++)
      {
         if (pil->array[j] < pil->array[smallPos])
         {
            smallPos = j;
         }
      }
      
      /* Swap two integers in the array. */
      temp = pil->array[i];
      pil->array[i] = pil->array[smallPos];
      pil->array[smallPos] = temp;
   }
}

/* Tute 7 practice question. */
int AddListOrdered(IntList* pil,int num)
{
   int i, j;
   
   if (pil->size >= INTLISTSIZE)
   {
      return FAILURE;
   }
   
   for (i = 0; i < pil->size; i++)
   {
      if (pil->array[i] > num)
      {
         break;
      }
   }
   
   for (j = pil->size; j > i; j--)
   {
      pil->array[j] = pil->array[j-1];
   }
   
   pil->array[i] = num;
   pil->size += 1;
   
   return SUCCESS;
}
