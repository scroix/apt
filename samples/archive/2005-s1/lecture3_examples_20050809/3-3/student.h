/* Modified example of slide 3-13. */
/* Note: This is a simple but poor example of how to best use structures. */

#include <stdio.h>
#include <stdlib.h>

#define STNUM_LEN 8

struct studentRecord
{
   char  stNum[STNUM_LEN + 1];
   int   ass1, ass2, exam;
   float total;
   char  grade;
};

struct studentRecord student1, student2;

float calcTotal(int, int, int);
char  calcGrade(float); 
