/* ragged arrays 
 * this example is taken from page 292 of "A Book on C"
 */ 

#include <stdio.h>

int main(void)
{
   char a[][15] = {"abc", "a is for apple"};
   char *p[2] = {"abc", "a is for apple"};

   printf("%c%c%c %s %s\n", a[0][0], a[0][1], a[0][2], a[0], a[1]);
   printf("%c%c%c %s %s\n", p[0][0], p[0][1], p[0][2], p[0], p[1]);
   return 0;
}
