#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct record
{
    char *processor;
    int MHz;
};

typedef struct record Record;

void print_machines(Record *records, size_t n)
{
    size_t i;

    for(i = 0; i < n; i++)
        printf("%s %d\n", records[i].processor, records[i].MHz);

    printf("\n");
}

int record_compare(const void *vptr1, const void *vptr2)
{
    const Record *a = (const Record*)vptr1;
    const Record *b = (const Record*)vptr2;

    return strcmp(a->processor, b->processor);
}

int main(int argc, char **argv)
{
    Record machines[] = {
        { "386SX", 25 },
        { "486DX", 33 },
        { "Pentium", 166 },
        { "Pentium II", 266 },
        { "Celeron", 400 },
        { "Alpha 21164PC", 500}
    };

    size_t nmachines = sizeof(machines) / sizeof(Record);

    Record target;
    Record *ptr;

    if (argc != 2) {
        fprintf(stderr, "usage: %s <target processor>\n", argv[0]);
        return EXIT_FAILURE;
    }

    target.processor = argv[1];

    print_machines(machines, nmachines);

    qsort(machines, nmachines, sizeof(Record), record_compare);

    print_machines(machines, nmachines);

    ptr = bsearch(&target, machines, nmachines, sizeof(Record), record_compare);

    if (ptr)
        printf("Found %s.\n", ptr->processor);
    else
        printf("No match found.\n");


    return EXIT_SUCCESS;
}

