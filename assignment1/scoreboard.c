/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include "scoreboard.h"

/**
 * @param board the scoreboard to initialise
 **/
void init_scoreboard(scoreboard board)
{
    int i;

    for (i = 0; i < SCOREBOARDSIZE; i++)
    {
     strcpy(board[i].name, "");
     board[i].counters = 0;  
    }
}

/** For this requirement, you will need to display the scores in the
 * @param board the scoreboard to display
 **/
void display_scores(const scoreboard board)
{
    int i;
    int namelen;

    system("clear");

    printf("Player               |Score\n");
    printf("---------------------------------------------\n"); 

    for (i = 0; i < SCOREBOARDSIZE; i++)
    {
        namelen = strlen(board[i].name);

        printf("%s%*s|%d\n", board[i].name,(21-namelen)," ", board[i].counters);
    }

    printf("\nPress any key to return...");
    getchar();
}

/**
 * @param board the scoreboard to add the score to @param sc the score
 * to add to the scoreboard
 **/ 
 BOOLEAN add_to_scoreboard(scoreboard board, const score * sc) 
 {
    char const *newentry;
    int i, newscore, position, lowscore = 100;

    newentry = sc -> name;
    newscore = sc -> counters;

    /* Find the lowest score on the scoreboard */
    for (i = 0; i < SCOREBOARDSIZE; i++){
        if (board[i].counters < lowscore){
            lowscore = board[i].counters;
            position = i;
            }
    }

    /* Compare score of recent victory with the lowest entry on the scoreboard */
    if (newscore > lowscore) {
        strcpy(board[position].name, newentry);
        board[position].counters = newscore;
        sort_scoreboard(board);
        return TRUE;
    }
    else {
        return FALSE;
    }
}

/**
 * use a temporary swap technique to reorder the scoreboard
 **/ 
void sort_scoreboard(scoreboard board) {
    struct player swapper;
    int i, j;
    
    for (i = 0; i < SCOREBOARDSIZE; i++) {
        for (j = i + 1; j < SCOREBOARDSIZE; j++) {
            if (board[i].counters < board[j].counters)
            {
                swapper = board[i];

                board[i] = board[j];

                board[j] = swapper;
            }
        }
    }
}

