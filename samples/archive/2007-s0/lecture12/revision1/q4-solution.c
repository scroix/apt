/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* Created December 2005
* COSC1098/1283/1284 Summer 2006 Exam Solutions
* Question 4 - 20 marks
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isPalindrome(char* string);
int printPalindromesFromFile(char* filename);

int main(void)
{
   printf("%d\n", printPalindromesFromFile("non-existent-file.txt"));
   printf("%d\n", printPalindromesFromFile("palindrome-file.txt"));
   
   return EXIT_SUCCESS;
}

int isPalindrome(char* string)
{
   int i, j;

   for (i = 0, j = strlen(string) - 1; i < strlen(string) / 2; i++, j--)
   {
      if (string[i] != string[j])
      {
         return 0;
      }
   }
   
   return 1;
}

/* Function definition. (1 mark) */
int printPalindromesFromFile(char* filename)
{
   /* Necessary variables. (3 marks) */
   FILE* fp;
   char line[80];
   char* tok;
   
   /* Check that file exists. (2 marks) */
   if ((fp = fopen(filename, "r")) == NULL)
   {
      return 0;
   }
   
   /* Outer looping construct. (3 marks) */
   while (fgets(line, 80, fp) != NULL)
   {
      /* Tokenise first word of line. (1 mark) */
      tok = strtok(line, " \n");
      
      /* Check for empty lines. (2 marks) */
      if (strlen(tok) == 0)
      {
         continue;
      }
      
      /* Check palindrome of first word. (2 marks) */
      if (isPalindrome(tok) == 1)
      {
         printf("%s\n", tok);
      }
      
      /* Inner looping construct. (3 marks) */
      while((tok = strtok(NULL, " \n")) != NULL)
      {
         /* Check palindrome of remaining words. (2 marks) */
         if (isPalindrome(tok) == 1)
         {
            printf("%s\n", tok);
         }
      }
   }
   
   /* Return on success. (1 mark) */
   return 1;
}
