#include <stdio.h>

int main(void)
{
   float x;
   int y = 5, z = 10;
   x  = y / z;     	/* problem! */
   x = (float) y / (float) z;   /* solution */
   printf("The result is %f\n", x);
   return 0;
}
