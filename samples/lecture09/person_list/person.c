#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "person.h"

struct person *person_new(const char *name, int age)
{
    struct person *person = malloc(sizeof(struct person));
    assert(person != NULL);

    if (strlen(name) > NAME_MAX) {
        fprintf(stderr, "Error: name length must be <= %d\n", NAME_MAX);

        free(person);

        return NULL;
    }

    if (age < AGE_MIN || age > AGE_MAX) {
        fprintf(stderr, "Error: %d < age < %d\n", AGE_MIN, AGE_MAX);

        free(person);

        return NULL;
    }

    strcpy(person->name, name);

    person->age = age;

    person->next = NULL;

    return person;
}

void person_free(struct person *person)
{
    free(person);
}

void person_print(const struct person *person, FILE *stream)
{
    fprintf(stream,
            "{\n"
            "    name: \"%s\",\n"
            "    age: %u\n"
            "}\n",
            person->name,
            person->age);
}

/*
  Sort person struct first by name then age with the following semantics
  if lhs == rhs return 0
  if lhs < rhs return -1
  if rhs > rhs return 1
 */
int person_cmp(const struct person *lhs, const struct person *rhs)
{
    int result = strcmp(lhs->name, rhs->name);

    if (result == 0)
        return lhs->name < rhs->name;

    return result;
}
