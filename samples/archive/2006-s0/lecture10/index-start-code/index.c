/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 25/1/2006
* 
* index.c
* Practical example of constructing a simple index using a linked list.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LINE_LEN 80
#define WORD_LEN 20
#define SUCCESS 1
#define FAILURE 0
#define FILENAME "data.txt"
#define DELIM " \n\r"

/* Structure #3 goes here (later). */

typedef struct wordStruct
{
   /* What goes here? */
} WordType;

typedef struct indexStruct
{
   /* What goes here? */
} IndexType;

int loadIndex(IndexType* index, char* filename);
void displayIndex(IndexType* index);

int main(void)
{
   IndexType index;
   
   if (loadIndex(&index, FILENAME) == FAILURE)
   {
      return EXIT_FAILURE;
   }
   
   displayIndex(&index);
   
   return EXIT_SUCCESS;
}

int loadIndex(IndexType* index, char* filename)
{
   /* Implementation goes here. */
}

void displayIndex(IndexType* index)
{
   /* Implementation goes here. */
}
