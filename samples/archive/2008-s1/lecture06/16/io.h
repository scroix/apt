/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 13/1/2006
* 
* io.h
* Example of makefiles.
* Citation: Slide 6-16 of PP2A/PT course notes.
****************************************************************************/

/* Function prototypes. */
void put();
void get();
