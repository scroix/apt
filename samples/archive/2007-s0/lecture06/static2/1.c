/****************************************************************************
* Steven Burrows  
* sdb@cs.rmit.edu.au
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

static void func1();
static void func2();

int main(void)
{
   func1();
   func2();
   return EXIT_SUCCESS;
}

static void func1()
{
   /* ... */
}
