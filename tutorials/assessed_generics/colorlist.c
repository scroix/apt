#include "colorlist.h"
int main(void)
{
        struct list list;
        BOOLEAN quit = FALSE;
        /* initialise the list, passing in the function pointers that 
         * will be stored in the lead header for various tasks done 
         * by the list*/
        list_init(&list, color_cmp, del_color_cmp, color_print, color_free);
        /* load in data from a file */
        if(!read_color_file("colors.txt", &list))
        {
                return EXIT_FAILURE;
        }
        /* repeatedly display the list of items in the list and 
         * give the user the ability to select one to delete
         * until they choose to quit the program by pressing enter or
         * ctrl-d
         */
        while(!quit)
        {
                char input_line[LINE_LEN + 1];
                list_print(&list);
                /* get input from the user  - if they press enter
                 * on a new line or press ctrl-d the we quit 
                 * the program
                 **/
                if(get_string("Please enter the id "
                                        "of an element to remove",
                                        input_line, LINE_LEN) 
                                == R_EOF)
                {
                        printf("all done! we are exiting.\n");
                        quit = TRUE;
                }
                else
                {
                        /* try to delete the item chosen from the 
                         * list
                         */
                        if(!list_remove(&list, input_line))
                        {
                                /* display error message if we fail */
                                fprintf(stderr, 
                                                "Error: failure to "
                                                "delete item with id:"
                                                " %s\n", 
                                                input_line);
                        }
                }

        }
        list_free(&list);
        return EXIT_SUCCESS;
}

/*
 * creates a data node based on the data read in from the file 
 **/
struct color_row * make_color_node(const char * id, const char * name
                , const char * color, const int age)
{
        /* allocate space for the color node */
        struct color_row * new = (struct color_row*) malloc(
                        sizeof(struct color_row)
                        );
        if(!new)
        {
                return NULL;
        }
        /* copy data into the node */
        strcpy(new->id, id);
        strcpy(new->name, name);
        strcpy(new->color, color);
        new->age = age;
        return (new);
}

/*
 * parses a row / line passed in that has been read from the 
 * data file
 **/
struct color_row * parse_color_row(const char * line)
{
        char ** elements;
        struct color_row * row = NULL;
        char id[C_ID_LEN + 1], name[C_NAME_LEN + 1], 
             color[C_COLOR_LEN + 1];
        int age;
        /* calles parse_row which tokenizes a line from a file and 
         * sets the elements variable declared above to a 
         * 2-dimensional array of chars, each string in the elements
         * array is one token from the file
         */
        if(!parse_row(&elements, line, C_NUM_TOKENS))
        {
                return NULL;
        }
        /* store the elements in local variables that will be passed
         * into create_color_node() to allocate the node for insertion
         * into the linked list
         */
        strcpy(id, elements[C_ID_TOK]);
        strcpy(name, elements[C_NAME_TOK]);
        strcpy(color, elements[C_COLOR_TOK]);
        if(!str_to_int(elements[C_AGE_TOK], &age))
        {
                fprintf(stderr, "Error: %s is not a valid number.\n", 
                                elements[C_AGE_TOK]);
                return NULL;
        }
        /* now we have copied the data we can free the pointers 
         * allocated with malloc()
         */
        free(elements[C_ID_TOK]);
        free(elements);
        /* create the node for insertion into the linked list and 
         * return it
         */
        row = make_color_node(id, name, color, age);
        return row;
}

/*
 * reads in the color file, processes the data and stores the 
 * data into the linked list provided
 **/
BOOLEAN read_color_file(const char * fname, struct list * list)
{
        /* open the file - if this fails, the program will quit */
        FILE * reader = gropen(fname);
        char line[C_LINE_LEN + EXTRA_CHARS];
        struct color_row * new_row;
        /*
         * read each line in from the file 
         */
        while(read_line(reader, C_LINE_LEN, line) > 0)
        {
                /* parse each row to create a color node that will
                 * be stored in the linked list 
                 */
                new_row = NULL;
                if(new_row = parse_color_row(line), new_row != NULL)
                {
                        if(!list_add(list, new_row))
                        {
                                return FALSE;
                        }
                }
        }
        /* we may fail to read due to getting to the end of the file
         * and that's ok but other errors are not ok
         */
        if(!feof(reader))
        {
                fclose(reader);
                return FALSE;
        }
        fclose(reader);
        return TRUE;
}

/* 
 * frees a color row
 **/
void color_free(void * data)
{
        free(data);
}

/*
 * defines how to print a color row
 **/
void color_print(void * data)
{
        /* assign the pointer to a pointer to the actual data
         * type
         */
        struct color_row * c_data = data;
        /* print the data */
        printf("id: %s\tname: %s\t color: %s\tage: %d\n", 
                        c_data->id, c_data->name, c_data->color, 
                        c_data->age);
}

/*
 * comparison function used when inserting data into the list
 **/
int color_cmp(void *first, void * second)
{
        /* assign the data to pointers of the correct type */
        struct color_row * c_first = first, * c_second = second;
        /* sort based on the name of each person in the color list */
        int cmp;
        cmp = strcmp(c_first->name, c_second->name);
        /* if the names are the same then sort by age */
        if(cmp == 0)
        {
                cmp = c_first->age - c_second->age;
        }
        return cmp;
}

/*
 * comparison function for deletion from the linked list
 **/
int del_color_cmp(void * data, char * id)
{
        struct color_row * mydata = data;
        /* are the ids the same ?  */
        return strcmp(mydata->id, id);
}
