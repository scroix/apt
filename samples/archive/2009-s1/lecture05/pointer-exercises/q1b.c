#include <stdio.h>

void printOne(int *x);
void printTwo(int *x);
void printThree(int *x);

int main(void)
{
    /* Prototype declaration */
    void printOne(int *);
    void printTwo(int *);
    void printThree(int *);

    /* Local definitions */
    int num[5] = {3, 4, 6, 2, 1};
    
    /* Statements */
    printOne(num);
    printTwo(num+2);
    printThree(&num[2]);
    return 0;

}

void printOne(int *x)
{
    printf("\n%d", x[2]);
}

void printTwo(int *x)
{
    printf("\n%d", x[2]);
}

void printThree(int *x)
{
    printf("\n%d", x[2]);
}   

   
  
