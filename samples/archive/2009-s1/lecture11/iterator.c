/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 30/1/2006
* 
* iterator.c
* Practical example of function pointers.
* Citation: Slides 11-6 to 11-8 of PP2A/PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int testAllChars(char *, int (*test)(int));
/* Prototype for testAllChars, a function that traverses
 * a string and applies a "test" to each char. The strnig
 * is provided via the first parameter, and the "test"
 * function is provided by the second "function pointer"
 * parameter. Any string that "passes the test" for all
 * characters prior to the nul-terminator will return in
 * testAllChars() returning 1, otherwise 0 will be returned.
 * ie. if any single character "fails the test" 0 will be
 * returned
 */

int main(void)
{
   char *s = "ab1cdef";

   /* int isalpha(int) and int isdigit(int) come from ctype.h */

   if (testAllChars(s, isalpha))
   {
      printf("yes -- isalpha\n"); 
   }
   else
   {
      printf("no -- isalpha\n");
   }

   if (testAllChars(s, isprint))
   {
      printf("yes -- isprint\n"); 
   }
   else
   {
      printf("no -- isprint\n");
   }

   return EXIT_SUCCESS;
}

int testAllChars(char *s, int (*test)(int))
{
   int flag = 1;
   
   for ( ; *s; s++)
   {
      if (!test(*s))
      {
         flag = 0;
      }
   }
   
   return flag;
}
