
/*
 * This is an example developed during the afternoon lecture
 * of 13th Sept. 2005 to simply demonstrate the use of malloc/free
 * for the allocation/deallocation of a dynamic array.
 */

#include <stdio.h>
#include <stdlib.h>

#define ARR_SIZE 10

int main(void)
{

   int * dynArray;
   int i;

   dynArray = malloc(sizeof(int)*ARR_SIZE); 
   if (dynArray == NULL)
   {
      fprintf(stderr, "Memory allocation failed. Program terminating\n");
      return EXIT_FAILURE;
   }

   for(i=0; i<ARR_SIZE; i++)
      dynArray[i] = i;

   for(i=0; i<ARR_SIZE; i++)
      printf("%d\n", dynArray[i]);

   free(dynArray); 

   return EXIT_SUCCESS;
}
