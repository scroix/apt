/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #1 - tic tac toe program
* Sample Exam Questions
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define ROW_LEN 3
#define COL_LEN 3
#define TRUE 1
#define FALSE 0
#define NAUGHT 'O'
#define CROSS 'X'
#define CELL_MIN 1
#define CELL_MAX 9

void loadTTTString(char* s, char tttGrid[ROW_LEN][COL_LEN]);

int main(void)
{
   char tttGrid[ROW_LEN][COL_LEN];
   int row, col;

   loadTTTString("12X4XX7OO", tttGrid);

   /* Print the grid */
   for (row = 0; row < ROW_LEN; row++)
   {
      for (col = 0; col < COL_LEN; col++)
      {
         printf("%c", tttGrid[row][col]);
      }
      putchar('\n');
   }
   
   return EXIT_SUCCESS;
}

void loadTTTString(char* s, char tttGrid[ROW_LEN][COL_LEN])
{
   /*int row, col;*/
   int i;
   char* ptr = &tttGrid[0][0];

   for (i = 0; i < CELL_MAX; i++)
   {
      *(ptr + i) = *s++; 
   } 
   
   /*
   for (row = 0; row < ROW_LEN; row++)
   {
      for (col = 0; col < COL_LEN; col++)
      {
         tttGrid[row][col] = *s++; 
         tttGrid[row][col] = s[row * ROW_LEN + col]; 
      }
   } 
   */
}
