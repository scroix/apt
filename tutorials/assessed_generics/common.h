#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>

#ifndef COMMON
#define COMMON
/* the two characters at the end of each line - the newline and the nul
 * terminator
 */
#define EXTRA_CHARS 2
/* the comma is used for separating of tokens in each of the files used
 * by this program
 */
#define SEPARATOR ","
/* the maximum length of a line of user input is 80 characters */
#define LINE_LEN 80

/* definition of the boolean datatype */
typedef enum
{
        FALSE, TRUE
} BOOLEAN;

/* values that can be returned from io functions - success, failure and 
 * end of file
 */
enum io_result
{
        R_FAILURE, R_SUCCESS, R_EOF = EOF
};
char * my_str_dup(const char *);
enum io_result read_line(FILE* , int, char *);
BOOLEAN parse_row(char***, const char*, int);
FILE * gropen(const char *);
BOOLEAN str_to_int(const char *, int *);
BOOLEAN str_to_float(const char *, float *);
enum io_result get_string( const char *, /* out */ char *, int);
#endif
