#include "floatlist.h"
int main(void)
{
        /* the list we are useing for this application */
        struct list list;
        BOOLEAN quit = FALSE;
        /* initialise the list passing in the list as well as the
         * function pointers to assign in the list structure
         */
        list_init(&list, float_cmp, del_float_cmp, float_print, float_free);
        /* read in the data from a file - quit if there is an error reading
         * in data from the file
         **/
        if(!read_float_file("floats.txt", &list))
        {
                return EXIT_FAILURE;
        }
        /* prompt the user to delete an item from the list until they decide
         * to quit
         */
        while(!quit)
        {
                char input_line[LINE_LEN + 1];
                /* print out the list */
                list_print(&list);
                /* read in the id that the user wishes to delete */
                if(get_string("Please enter the id "
                                        "of an element to remove",
                                        input_line, LINE_LEN) == R_EOF)
                {
                        printf("all done! we are exiting.\n");
                        quit = TRUE;
                }
                else
                {
                        /* did we succeed in deleting the item? If not 
                         * display an error message 
                         */
                        if(!list_remove(&list, input_line))
                        {
                                fprintf(stderr, "Error: failure to delete"
                                        " item with id: %s\n", 
                                        input_line);
                        }
                }

        }
        /* free all memory */
        list_free(&list);
        return EXIT_SUCCESS;
}

/*
 * allocate space for a node and assign the values passed in
 **/
struct float_row * make_float_node(const char * id, float number)
{
        /* allocate space */
        struct float_row * new = (struct float_row*) malloc(
                        sizeof(struct float_row)
                        );
        /* check that the allocation worked */
        if(!new)
        {
                return NULL;
        }
        /* copy the values into the space allocated */
        strcpy(new->id, id);
        new->number = number;
        return (new);
}

/*
 * parses a row from the float file and returns a pointer to a node 
 * allocated with make_float_node()
 **/
struct float_row * parse_float_row(const char * line)
{
        /* a 2d array of char that holds the tokenized data from a 
         * line in the float file
         */
        char ** elements;
        struct float_row * row = NULL;
        char id[F_ID_LEN + 1];
        float number;
        /* tokenize the string passed in */
        if(!parse_row(&elements, line, F_NUM_TOKENS))
        {
                return NULL;
        }
        /* process and copy the elements so they can be passed into 
         * make_float_node()
         */
        strcpy(id, elements[F_ID_TOK]);
        if(!str_to_float(elements[F_FLOAT_TOK], &number))
        {
                fprintf(stderr, "Error: %s is not a valid number.\n", 
                                elements[F_FLOAT_TOK]);
        }
        /* free the pointers allocated in parse_row() */
        free(elements[F_ID_TOK]);
        free(elements);
        /* allocate a mode based on the data we have parsed */
        row = make_float_node(id, number);
        return row;
}

/*
 * reads in the file specified and reads each line with the read_line()
 * function defined in common.c. it then parses each line to a float_row
 * using parse_float_row(). Finally it inserts each row into the provided
 * linked list
 **/
BOOLEAN read_float_file(const char * fname, struct list * list)
{
        FILE * reader = gropen(fname);
        char line[F_LINE_LEN + EXTRA_CHARS];
        struct float_row * new_row;
        /* read in each line */
        while(read_line(reader, F_LINE_LEN, line) > 0)
        {
                new_row = NULL;
                /* parse the line into a float_row struct */
                if(new_row = parse_float_row(line), new_row != NULL)
                {
                        /* insert into the list */
                        if(!list_add(list, new_row))
                        {
                                return FALSE;
                        }
                }
        }
        /* when we have an io error, just double check that we have not
         * reached the end of the file
         */
        if(!feof(reader))
        {
                /* if we have not, there was some error so we return 
                 * FALSE
                 */
                fclose(reader);
                return FALSE;
        }
        /* at the end of the file so all is good */
        fclose(reader);
        return TRUE;
}

/*
 * free the data for a float_row
 **/
void float_free(void * data)
{
        free(data);
}

/*
 * print out a float_row. Note that we need to convert the void pointer to
 * a float_row pointer before we can print out the data 
 **/
void float_print(void * data)
{
        struct float_row * f_data = data;
        printf("id: %s\tnumber: %f\n", f_data->id, f_data->number);
}

/*
 * compares two float rows in terms of how they should be sorted - in this
 * case it is based on the "number" element of the float_row struct. Now
 * It would not make sense to just compare two floats based on the 
 * difference between them - this will be rounded up to the nearest int. 
 * Note: this comparison function is used for insertion.
 **/
int float_cmp(void *first, void * second)
{
        struct float_row * f_first = first;
        struct float_row * f_second = second;
        /* take the difference between the two floating point numbers, 
         * muliply this by some number to give us the accuracy then round
         * it up to the nearest int using ceil and then convert to int
         */
        return ((int)ceil((f_first->number - f_second->number) * ACCURACY));
}

/*
 * is the id of this number the same as the one specified? Note: this is 
 * used as a comparison function for deletion.
 **/
int del_float_cmp(void * data, char * id)
{
        struct float_row * mydata = data;
        return strcmp(mydata->id, id);
}
