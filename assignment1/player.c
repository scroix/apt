/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include "player.h"

/**
 * @param human the human player to initialise
 **/
enum input_result get_human_player(struct player* human)
{
    char userinput[NAMELEN + 1];
    char prompt[PROMPT_LENGTH + 1];

    /* prompt and update user's name */
    sprintf(prompt, "Please enter your name (max %d characters): ", NAMELEN);
    
    /* End early if receiving a RTM message from user */
    if(getString(userinput, NAMELEN, prompt) == RTM) {
        return FAILURE;
    }

    strcpy(human -> name, userinput);
    
    /* set player to human */
    human -> type = HUMAN;

    /* init counters */
    human -> counters = 0;
    
    return SUCCESS;
}

/**
 * @param computer the computer player to initialise
 **/
enum input_result get_computer_player(struct player* computer)
{
    computer -> type = COMPUTER;
    computer -> counters = 0;
    strcpy(computer -> name, "Computer");

    return SUCCESS;
}

/**
 * @param current the current player
 * @param board the game board that we will attempt to insert a token into
 * @return enum @ref input_result indicating the state of user input (in 
 * case this run involved user input
 **/
enum input_result take_turn(struct player * current,
        enum cell_contents board[][BOARDWIDTH])
{
    int result = 0;
    char prompt[PROMPT_LENGTH + 1];
    int min = 1, max = 7;
    int i;
    enum falsetrue moveValid = FALSE;
    enum falsetrue resultValid = FALSE;

    srand(time(NULL));

    /* LOGIC FOR HUMAN PLAYER */
    if (current -> type == HUMAN)
    {
        /* Receive input from user, validation included */
        do { 
            sprintf(prompt, "Enter a location to drop your token (%d - %d): ", min, max);

            /* Input validation built into function, requires min, max and converts to numeric type */
            if (getInteger(&result, INT_INPUT_LENGTH, prompt, min, max) == RTM) {
                return FAILURE;
            }

            /* Check if token already exists at top of board in this position */
            if (board[0][result - 1] != (C_EMPTY))
            {
                printf("\nColumn '%d' has no more room.\n\n", result);
            }
            else
            {
                resultValid = TRUE;
            }

        } while (resultValid == FALSE);

        /* Replace token at board location corresponding to user input with their token type */
        for (i = BOARDHEIGHT - 1; i >= 0; i--)
        {
            if (board[i][result - 1] == (C_EMPTY))
            {
                board[i][result - 1] = current -> thiscolor;

                moveValid = TRUE;
                break;
            }
        }
    }
    /* LOGIC FOR COMPUTER PLAYER */
    else if (current -> type == COMPUTER)
    {

        do
        {
            result = (rand() % (BOARDWIDTH - 0));

            for (i = BOARDHEIGHT - 1; i >= 0; i--)
            {   
                if (board[i][result] == (C_EMPTY))
                {
                    board[i][result] = current -> thiscolor;
                    moveValid = TRUE;
                    break;
                }
            }

        } while (moveValid == FALSE);

    }

    /* If the turn was valid then the counter rises */
    current -> counters++;
    return SUCCESS;
}

/**
 * randomise the player colours using a time based seed
 **/
struct player * randomise_colours(struct player *human, struct player *computer)
{   
    int x;

    /* Our random seed is based off time */
    srand(time(NULL));

    /* Return either 0 or 1, to interpret as TRUE/FALSE */
    x = (rand() % 2);

    if (x)
    {
        human -> thiscolor = C_WHITE;
        computer -> thiscolor = C_RED;
    }
    else
    {
        human -> thiscolor = C_RED;
        computer -> thiscolor = C_WHITE;
    }

    return SUCCESS;
}
