/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 2/1/2006
* 
* student.c
* Calculates the overall score of a student based upon an exam score and two
* assignment scores (each out of 100%).
* Citation: Adapted from slide 1-16 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int ass1, ass2, exam;
   float total;

   if (scanf("%d%d%d", &ass1, &ass2, &exam) != 3)
   {
      printf("Unable to find 3 integers on standard input.\n");
      return EXIT_FAILURE;
   }

   total = ass1 * 0.2 + ass2 * 0.3 + exam * 0.5;

   printf("Assignment 1 = %3d\n", ass1);
   printf("Assignment 2 = %3d\n", ass2);
   printf("Exam         = %3d\n", exam);
   printf("Total        = %6.2f\n", total);

   return EXIT_SUCCESS;
}
