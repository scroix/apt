
#include <stdio.h>
#include <stdlib.h>

 /* Header file for IntList type module */

#define SUCCESS 1
#define FAILURE 0

 typedef struct listnode *   ListNodePtr;
 
 typedef struct listnode {
    int data;
    ListNodePtr next;
 } ListNode;

 typedef struct
 {
   ListNodePtr head;
   int size;
 } IntList;


int MakeList( IntList * );

int AddList ( IntList * , int  );

void PrintList(IntList *);

void FreeList(IntList *);


/* Client Program that uses the IntList type module */

 int main(void)
 {

    IntList il;

    if (MakeList(&il) == FAILURE)
    {
      fprintf(stderr, "... deal with error ...\n");
      return EXIT_FAILURE;
    }

    if (AddList(&il, 1) == FAILURE)
    {
      fprintf(stderr, "... deal with error ...\n");
      return EXIT_FAILURE;
    }

    /*

       il             first (and only) linked list node
      +------+      +-----------+
      | head |      | data next |
      |   ---------->  1   NULL |
      | size |      |           |
      |   1  |      +-----------+
      +------+


     */

    if (AddList(&il, 2) == FAILURE)
    {
      fprintf(stderr, "... deal with error ...\n");
      return EXIT_FAILURE;
    }

    /*

       il             new node         original node
      +------+      +-----------+     +-----------+
      | head |      | data next |     | data next |
      |   ---------->  2     -------->|  1   NULL |
      | size |      |           |     |           |
      |   1  |      +-----------+     +-----------+
      +------+


     */


    PrintList(&il);

    FreeList(&il); 


    return EXIT_SUCCESS;
 }

int MakeList( IntList * pil)
{

   pil->head = NULL;
   pil->size = 0;
 
   return SUCCESS;  /* 1  == SUCCESS, needs to be defined */
}

int AddList ( IntList * pil, int data )
{
     ListNodePtr newNode;

     newNode = malloc(sizeof(ListNode));
     if (newNode == NULL)
     {
       return FAILURE;
     }
     
     /* Make the new list node point to what ever existing node
        the head of the list currently points to */

     newNode->next = pil->head;

     /* Now update the head of the list pointer, so that it 
        points to the new node */

     pil->head = newNode;


     newNode->data = data;  

     
     pil->size += 1;

     return SUCCESS;

}

void PrintList(IntList * pil)
{
   ListNodePtr current;

   current = pil->head;

   while(current != NULL)
   {
     printf("%d\n", current->data);
     current = current->next;   /* conceptually similar to i++ */
   }
}

void FreeList(IntList * pil)
{
   ListNodePtr current, next;

   current = pil->head;

   while (current != NULL)
   {
      next = current->next;
      free(current);
      current = next;
   }
}
