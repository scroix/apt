#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

typedef FILE* TYPE;

void fn(TYPE *type)
{
    *type = fopen("test", "r");

    assert(*type);
}

int main()
{
    TYPE type;

    fn(&type);

    fclose(type);

    return EXIT_SUCCESS;
}
