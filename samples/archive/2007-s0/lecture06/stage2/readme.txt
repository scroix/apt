/****************************************************************************
* COSC1283/1284 - Programming Techniques
* Summer 2007 Assignment #2 - word link program
* Full Name        : EDIT HERE
* Student Number   : EDIT HERE
* Yallara Username : EDIT HERE
* Course Code      : EDIT HERE
* Program Code     : EDIT HERE
* Start up code provided by Steven Burrows
****************************************************************************/

-----------------------------------------------------------------------------
If selected, do you grant permission for your assignment to be released as an
anonymous student sample solution?
-----------------------------------------------------------------------------

Yes/No

-----------------------------------------------------------------------------
Documentation of my implemented requirements:
-----------------------------------------------------------------------------

Requirement  1 - Command line args   : Yes/No
Requirement  2 - Load data           : Yes/No
Requirement  3 - Main menu           : Yes/No
Requirement  4 - 1 player game       : Yes/No
Requirement  5 - 2 player game       : Yes/No
Requirement  6 - Dictionary summary  : Yes/No
Requirement  7 - Search dictionary   : Yes/No
Requirement  8 - Add dictionary word : Yes/No
Requirement  9 - Delete stop words   : Yes/No
Requirement 10 - Exit                : Yes/No
Requirement 11 - Makefile            : Yes/No
Requirement 12 - Memory leaks        : Yes/No
Requirement 13 - Buffer handling     : Yes/No
Requirement 14 - Input validation    : Yes/No
Requirement 15 - Coding conventions  : Yes/No

-----------------------------------------------------------------------------
Known bugs:
-----------------------------------------------------------------------------

None/EDIT HERE

-----------------------------------------------------------------------------
Assumptions:
-----------------------------------------------------------------------------

None/EDIT HERE

-----------------------------------------------------------------------------
Any other notes for the marker:
-----------------------------------------------------------------------------

None/EDIT HERE
