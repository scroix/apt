/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 3/1/2006
* 
* for.c
* Simple for-loop examples.
* Citation: Adapted from slide 2-22 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int j, k, count = 7;
   
   for (j = 10; j > 0; j--)
   {
      printf("%2d ", j);
   }
   printf("\n");

   for (j = 0; j < count; j++)
   {
      printf("%2d ", j);
   }
   printf("\n");

   for (j = 0, k = 5; j < count && k > 0; j++, k--)
   {
      printf("%2d ", j);
   }
   printf("\n");

   return EXIT_SUCCESS;
}
