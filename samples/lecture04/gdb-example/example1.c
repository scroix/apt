#include <stdlib.h>
#include <stdio.h>

#include "example1.h"

void example1()
{
    char string[] = "Programming Techniques";

    printf("'%s'\n", string);

    string[0] = 'T';

    string[12] = 'P';

    printf("'%s'\n", string);
}
