#include <stdio.h>
#include <string.h>

/*Q1d
 *
 */
/*
int main(void)
{
   char s1[20], s2[20];
   
   strcpy(s1, "computer");
   strcpy(s2, "science");
   if (strcmp(s1, s2) < 0)
      strcat(s1, s2);
   else
      strcat(s2, s1);
   s2[strlen(s2) - 6] = '\0';
   
   printf("%s\n", s1);
   printf("%s\n", s2);
   
   return 0;
}
*/

/*Q1f
 *
 */

int main(void)
{
    char alpha[] = "abcdefghi";
    char ch[80], *pch = &ch[0];
   
    strcpy(ch, alpha);
    putchar(*pch++);
    putchar((*pch)++);
    putchar(*++pch);
    putchar(++(*pch));
    printf("\n%s", ch);
    
    return 0;
}

  
