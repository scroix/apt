/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 20/1/2006
* 
* example1.c
* Example of command line args.
* Citation: Adapted from slide 8-12 of PP2A/PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
   int sum = 0, i;
   
   for (i = 1; i < argc; i++)
   {
      sum += atoi(argv[i]);
   }
   
   printf("Command line argument sum is %d\n", sum);

   return EXIT_SUCCESS;
}
