#include <stdio.h>

int main(void)
{
   char *a[5] = {"very low", "low", "medium", "high", "very high"};
   
   printf("%s\n", a[0]);
   printf("%s\n", *(a + 2));
   printf("%s\n", a[3]);
   printf("%c\n", *(a[3] + 2));
   printf("%c\n", *(*(a + 4) + 3));
   
   return 0;
}
   
  
