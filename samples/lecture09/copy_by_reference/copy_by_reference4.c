#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define NAME_MAX 80

struct person {
    char name[NAME_MAX + 1];
    unsigned int age;
    /* ... */
    struct person *next;
};

void person_new(struct person **person, const char *name, unsigned int age)
{
    *person = malloc(sizeof(struct person));
    assert(*person);

    strcpy((*person)->name, name);

    (*person)->age = age;
}

void person_free(struct person *person)
{
    free(person);
}

int main()
{
    struct person *person;

    person_new(&person, "Chris", 42);

    person_free(person);

    return EXIT_SUCCESS;
}
