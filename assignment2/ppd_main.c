/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Julien de-Sainte-Croix
 * Student Number   : s3669242
 * Course Code      : COSC1076
 * Program Code     : BP096
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_main.h"
#include "ppd_menu.h"
#include "ppd_options.h"
#include "ppd_utility.h"

/**
 * @file ppd_main.c contains the main function implementation and any 
 * helper functions for main such as a display_usage() function.
 **/

/**
 * manages the running of the program, initialises data structures, loads
 * data and handles the processing of options. The bulk of this function
 * should simply be calling other functions to get the job done.
 **/
int main(int argc, char **argv)
{
    BOOLEAN finished = FALSE;
    int choice = 0;
    char prompt[PROMPT_LENGTH + 1];
    int min = 1, max = MENU_SIZE;

    /* represents the data structures to manage the system */
    struct ppd_system system;
    struct menu_item menu[MENU_SIZE];

    /* init the system */
    system.coin_file_name = argv[COIN_LOC];
    system.stock_file_name = argv[STOCK_LOC];
    if(!system_init(&system)) {
        fprintf(stderr, "Error: failed to init system.\n");
        return EXIT_FAILURE;
    }

    /* validate command line arguments */
    if(argc != NUMARGS)
    {
        fprintf(stderr, "Error: invalid arguments passed in.\n");
        printf("Correct arguments are:\n%*s./ppd <stockfile> <coinfile>\n", 
            ERROR_WIDTH, " ");
        printf("Where <stockfile> and <coinfile> are two valid files in \
            the expected format.\n");
        return EXIT_FAILURE;
    }

    /* load data */
    if(!load_data(&system, system.coin_file_name, system.stock_file_name)) {
        fprintf(stderr, "Error: failed to load data from file.\n:");
        return EXIT_FAILURE;
    }

    /* initialise the menu system */
    init_menu(menu);

    /* loop, asking for options from the menu */
    strcpy(prompt, "Select your option (1-9): ");
    do
    {
        display_menu(menu);
        getInteger(&choice, INT_INPUT_LENGTH, prompt, min, max);
        
    /* run each option selected */
    if(choice > 0 && choice < MENU_SIZE) {
        if(!menu[choice - 1].function(&system)) {
            fprintf(stderr, "The task %s failed to run succesfully.\n", 
                menu[choice - 1].name);
        }
    } 
        
    /* until the user quits */
    if (choice == EXIT_LOC || choice == SAVE_EXIT_LOC)
        finished = TRUE;

    } while (!finished);
    
    /* make sure you always free all memory and close all files 
     * before you exit the program
     */
    system_free(&system);

    return EXIT_SUCCESS;
}
