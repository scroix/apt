/* intArrayList.c */
#include "intArrayList.h"
#include <assert.h>
#include <stdlib.h>

void ListMake(IntList * pil)
{
    assert(pil != NULL);

/* Nothing much to be done here for this particular
 * implementation of a list. Merely setting the
 * ''size'' member of the IntList object to 0 would
 * suffice.
 *
 * This interface routine exists because it is
 * anticipated that other implementations of a
 * list may need to do some initialisation before
 * the list can be manipulated
 *
 */

}

int ListInsert(IntList * pil, int num)
{
/* any required local vars go here */

    assert(pil != NULL);

/* if the list is already full then indicate
 * insertion failure witha return value
 *
 * Check to see how this function was used in
 * main() to determine what value to return
 * for a FAILURE
 */

/*
 * Here you'll need to write some code to
 * search for the correct place within the
 * array to insert the new element.
 * Finally return a value indicating SUCCESS
 *
 */

}

void ListDisplay(IntList * pil)
{
/* any required local vars go here */

assert(pil != NULL);

/* loop through all the existing elements within
 * the array and display them one per line
 */
}

void ListFree(IntList * pil)
{

assert(pil != NULL);

/* Nothing much to bedone here for this particular
 * implementation of a list. Merely setting the
 * size attribute to 0 would suffice
 */
}
