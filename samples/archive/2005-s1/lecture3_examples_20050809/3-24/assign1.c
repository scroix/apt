/****************************************************************************
* COSC1098/1283 - Assignment #1
* Programming Principles 2A/Programming Techniques
* Author           : <insert name here>
* Student Number   : <insert student number here>
* Yallara Username : <insert username here>
* Start up code provided by Steven Burrows - stburrow@cs.rmit.edu.au
****************************************************************************/

#include "assign1.h"

/****************************************************************************
* Function main() is the entry point for the program and provides an 
* interactive menu for the user.
****************************************************************************/
int main(void)
{


   return EXIT_SUCCESS;
}

/****************************************************************************
* This function prompts the user for a positive integer. It reports whether 
* or not this number is a perfect square. Additionally, it prints the perfect 
* squares immediately before and after this number.
****************************************************************************/
void perfectSquares()
{
}

/****************************************************************************
* This function prompts the user for a positive integer N. The first N 
* integers of the Fibonacci sequence are printed in a simple tabular format. 
* Additionally, the sum and average of these numbers are printed.
****************************************************************************/
void fibonacciNumbers()
{
}

/****************************************************************************
* This function prompts the user for a 10-character date of birth in 
* dd/mm/yyyy format. The system date is extracted with the assistance of the 
* time.h header file. This function reports how many days old the user is. 
* Dates of birth after the current date are rejected.
****************************************************************************/
void ageCalculator()
{
}

/****************************************************************************
* This function implements a simple version of the tic tac toe game for two
* players.
****************************************************************************/
void ticTacToeGame()
{
   char ticTacToeGrid[GRID_SIZE][GRID_SIZE] = {{'1','2','3'},
                                               {'4','5','6'},
                                               {'7','8','9'}};
                               
                               
}

/****************************************************************************
* This function implements a simple version of the blackjack game for two
* players.
****************************************************************************/
void blackjackGame()
{
                               /* Spades, Clubs, Diamonds, Hearts. */
   char* deckOfCards[DECK_SIZE] = {"2S",   "2C",   "2D",   "2H",
                                   "3S",   "3C",   "3D",   "3H",
                                   "4S",   "4C",   "4D",   "4H",
                                   "5S",   "5C",   "5D",   "5H",
                                   "6S",   "6C",   "6D",   "6H",
                                   "7S",   "7C",   "7D",   "7H",
                                   "8S",   "8C",   "8D",   "8H",
                                   "9S",   "9C",   "9D",   "9H",
                                   "10S",  "10C",  "10D",  "10H",
                                   "JS",   "JC",   "JD",   "JH",  /* Jack. */
                                   "QS",   "QC",   "QD",   "QH",  /* Queen */
                                   "KS",   "KC",   "KD",   "KH",  /* King. */
                                   "AS",   "AC",   "AD",   "AH"}; /* Ace.  */
                            
                            
}

/****************************************************************************
* Function readRestOfLine() is used for buffer clearing. Source: 
* https://inside.cs.rmit.edu.au/~pmcd/teaching/C-Prog/CourseDocuments/
* FrequentlyAskedQuestions/
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}
