/****************************************************************************
* COSC1098/1283 - Assignment #2 - Tennis Store
* Programming Principles 2A/Programming Techniques
* Author           : <insert name here>
* Student Number   : <insert student number here>
* Yallara Username : <insert username here>
* Start up code provided by Steven Burrows - stburrow@cs.rmit.edu.au
****************************************************************************/

#ifndef TS_H
#define TS_H

/* System-wide header files. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Constants for customer information. */
#define CUSTID_LEN 5
#define SURNAME_MAX 12
#define FIRSTNAME_MAX 12
#define ADDRESS_MAX 20
#define SUBURB_MAX 12
#define POSTCODE_LEN 4
#define PHONENUM_LEN 8
/*Constants for stock information. */
#define STOCKID_LEN 5
#define DESCRIPTION_MAX 40
#define PRICE_COLWIDTH 7
#define STOCKLEVEL_COLWIDTH 3
#define STOCKLEVEL_MAX 100

typedef struct customerNode* CustomerNodePtr;
typedef struct stockNode* StockNodePtr;

/* Structure definitions. */
typedef struct customerNode
{
   char custID[CUSTID_LEN + 1];
   char surname[SURNAME_MAX + 1];
   char firstName[FIRSTNAME_MAX + 1];
   char address[ADDRESS_MAX + 1];
   char suburb[SUBURB_MAX + 1];
   unsigned postCode;
   unsigned phoneNum;
   CustomerNodePtr nextCust;
} CustomerNodeType;

typedef struct stockNode
{
   char stockID[STOCKID_LEN + 1];
   char description[DESCRIPTION_MAX + 1];
   float unitPrice;
   unsigned stockLevel;
   StockNodePtr nextStock;
} StockNodeType;

typedef struct tennisStore
{
   CustomerNodePtr headCust;
   unsigned customerCount;
   StockNodePtr headStock;
   unsigned stockCount;
} TennisStoreType;

#endif
