/* compile with gcc -lgmp fib_mpz.c */

#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>

int main( int argc, char **argv )
{
    int i, n;

    mpz_t a, b, sum;

    if( argc != 2 ) {
        fprintf(stderr, "usage %s <n>\n", *argv);
        return EXIT_FAILURE;
    }

    mpz_init_set_ui( a, 0 );
    mpz_init_set_ui( b, 0 );
    mpz_init_set_ui( sum, 1 );

    i = 0;
    n = strtol( argv[1], (char **)NULL, 10 );

    while( i++ < n ) {
        printf( "%-3d ", i );

        mpz_out_str( stdout, 10, sum );

        printf( "\n" );

        mpz_set( a, b);
        mpz_set( b, sum);
        mpz_add( sum, a, b );
    }

    mpz_clear( a );
    mpz_clear( b );
    mpz_clear( sum );

    return EXIT_SUCCESS;
}
