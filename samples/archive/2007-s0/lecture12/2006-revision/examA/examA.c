/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 1/2/2006
* 
* examA.c
* Sample exam question.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SUCCESS 1
#define FAILURE 0

void outputLine(FILE* fpOut, char* line);
int interleave(char* fname1, char* fname2, char* fnameOut);

int main(void)
{
   interleave("file1.txt", "file2.txt", "file3.txt");

   return EXIT_SUCCESS;
}


/* IMPLEMENT outputLine() function here (12 marks). */


/* IMPLEMENT interleave() function here (25 marks). */
