#include <stdlib.h>
#include <stdio.h>

enum day_id {
    sunday, monday, tuesday, wednesday, thrsday, friday, saturday, ndays
};

const char *day_text[] = {
    "sunday", "monday", "tuesday", "wednesday", "thrsday", "friday", "saturday"
};

enum day_id next_day(enum day_id day_id)
{
    return (day_id + 1) % ndays;
}

int main (int argc, char **argv)
{
    enum day_id today, tomorrow;
    int value;

    if (argc != 2) {
        fprintf(stderr, "usage: %s <day_id>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    value = atoi(argv[1]);

    if (value >= ndays) {
        fprintf(stderr, "Error: Invalid input '%d'\n", value);
        return EXIT_SUCCESS;
    }

    today = value;

    tomorrow = next_day(today);

    printf("today    : id %d, '%s'\n", today, day_text[today]);

    printf("tomorrow : id %d, '%s'\n", tomorrow, day_text[tomorrow]);

    return EXIT_SUCCESS;
}
