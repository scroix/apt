#include <stdio.h>
#include <stlib.h>

#define SIZE 10

void rainfall_calc(int rainfall[SIZE], int * sum, double * avg)
{
    int i;

    for (i = 0; i < SIZE; i++)
    {
        *sum += rainfall[i];
    }

    *avg = *sum / (double) SIZE;

}
