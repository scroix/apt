/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 10/1/2007
* 
* stderr.c
* Example to facilitate the difference between stdout and stderr
* streams.
* Run program with:
* ( ./stderr > output.txt ) >& errors.txt
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   printf("Normal output\n");
   fprintf(stderr, "Error message\n");
 
   return EXIT_SUCCESS;
}
