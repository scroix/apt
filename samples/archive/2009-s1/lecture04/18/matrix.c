/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 9/1/2006
* 
* matrix.c
* Two-dimensional array example.
* Citation: Adapted from slide 4-18 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define ROWS 3
#define COLS 4

int matrixSum1(int mat[][COLS]);
int matrixSum2(int (*mat)[COLS]);

int main(void)
{
   int matrix[ROWS][COLS] = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
   int sum = 0;
   
   sum = matrixSum1(matrix);
   printf("Sum is: %d\n", sum);
   
   sum = matrixSum2(matrix);
   printf("Sum is: %d\n", sum);
   
   return EXIT_SUCCESS;
}

int matrixSum1(int mat[][COLS])
{
   int i, j, total = 0;
   
   for (i = 0; i < ROWS; i++)
   {
      for (j = 0; j < COLS; j++)
      {
         total += mat[i][j];
      }
   }
   
   return total;
}

int matrixSum2(int (*mat)[COLS])
{  
   int i, j, total = 0;
   
   for (i = 0; i < ROWS; i++)
   {
      for (j = 0; j < COLS; j++)
      {
         total += mat[i][j];
      }
   }
   
   return total;
}
