/***********************************************************************
* COSC1076 - Advanced Programming Techniques
* Semester 2 2015 Assignment #1 
* Full Name        : Julien de-Sainte-Croix
* Student Number   : s3369242
* Course Code      : COSC1076
* Program Code     : BP096
* Start up code provided by Paul Miller
***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "bool.h"

/**
 * @file helpers.h contains the datastructures used for various helper 
 * functioons and their function prototypes
 **/
#ifndef HELPERS_H
#define HELPERS_H

#define INT_INPUT_LENGTH 1
#define PROMPT_LENGTH 80
#define INT_TEMP_LENGTH 1
#define STRING_INPUT_LENGTH 20
#define STRING_TEMP_LENGTH 20


/**
 * the possible values that can be returned from custom input handling 
 * functions
 **/
enum input_result
{
    /**
     * the input was valid
     **/
    SUCCESS,
    /**
     * the input was not valld
     **/
    FAILURE,
    /**
     * the user requested to return to the menu either by pressing enter on
     * an empty line or pressing ctrl-d on an empty line
     **/
    RTM=-1
};

/**
 * call this function whenever you detect buffer overflow but only call this
 * function when this has happened.
 **/
void read_rest_of_line(void);

/* inital declaration of two get functions */
int getInteger(int* integer, unsigned length, char* prompt, int min, int max);
int getString(char* string, unsigned length, char* prompt);

#endif
