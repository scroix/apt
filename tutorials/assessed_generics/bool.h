#ifndef BOOL_H
#define BOOL_H
/* definition of the boolean datatype */
typedef enum {FALSE, TRUE} BOOLEAN;
#endif
