/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2015 Assignment #2
 * Full Name        : Julien de-Sainte-Croix
 * Student Number   : s3669242
 * Course Code      : COSC1076
 * Program Code     : BP096
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_options.h"

/**
 * @file ppd_options.c this is where you need to implement the main 
 * options for your program. You may however have the actual work done
 * in functions defined elsewhere. 
 * @note if there is an error you should handle it within the function
 * and not simply return FALSE unless it is a fatal error for the 
 * task at hand. You want people to use your software, afterall, and
 * badly behaving software doesn't get used.
 **/

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this particular function should never fail.
 **/
BOOLEAN display_items(struct ppd_system * system)
{   
    int colwidth_head = 45;
    int colwidth_avail;
    
    StockNode *curr;

    printf("Items Menu\n");
    printf("--------\n");
    printf("ID%*s|Name%*s|Available |Price \n", ID_WIDTH, " ", NAME_WIDTH, " ");
    while(colwidth_head--)printf("-");
    printf("\n");

    curr = system->item_list->head;

    /* iterative print */
    while (curr != NULL) {
        /* crude column width adjustment */
        if (curr->data->on_hand >= HIGH_STOCK)
            colwidth_avail = LOW_STOCK_WIDTH;
        else
            colwidth_avail = HIGH_STOCK_WIDTH;
        /* print each piece of data from the system struct */
        printf("%s |", curr->data->id);
        printf("%s%*s|", curr->data->name,
            LARGE_NAME_WIDTH - (int) strlen(curr->data->name), " "); 
        printf("%u%*s|", curr->data->on_hand, colwidth_avail, " ");
        printf("$ %u.%u", curr->data->price.dollars, curr->data->price.cents);
        printf("\n");
        /* update curr to next item in linked list */
        curr = curr->next;
    }
    
    return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a purchase succeeds and false when it does not
 **/
BOOLEAN purchase_item(struct ppd_system * system)
{
    char id[IDLEN + 1];
    char prompt[PROMPT_LENGTH + 1];
    int inserted, inserted_total = 0, owedCents;
    double owedDollars;
    BOOLEAN id_match;

    StockNode *curr;

    printf("Purchase Item\n");
    printf("------------\n");
   
    /* Search for matching id */
    id_match = FALSE;
    while(!id_match)
    {
        strcpy(prompt, "Please enter the id of the item you wish to purchase: ");
        if(getString(id, STRING_INPUT_LENGTH, prompt) == RTM) {
            printf("Cancelling 'purchase item' at user's request\n");
            return FALSE;
        }
        
        curr = system->item_list->head;
        
        /* iterate through item list to point at matching id */
        while (curr != NULL && strcmp(curr->data->id, id) != 0)
        {
            curr = curr->next;
        }
        
        /* id match found */
        if(curr != NULL) {
            id_match = TRUE;
        }
        /* id not found */
        else {
            fprintf(stderr, "Error: you did not enter a valid id. Please try again.\n");
        }
    }

    if (curr->data->on_hand == 0) {
        fprintf(stderr, "Error: out of stock.\n");
        return FALSE;
    }

    printf("You have selected (%s - %s).\n", curr->data->name,
                                                curr->data->desc);
    printf("This will cost you %u.%u.\n", curr->data->price.dollars,
                                                curr->data->price.cents);
    printf("Please hand over the money - ");
    printf("type in the value of each note/coin in cents.\n");
    printf("Press enter or ctrl-d on a new line to cancel this purchase: \n");
     
    /* CALCULATE COST AND HANDLE MONEY */
    
    owedCents = (curr->data->price.dollars * CENT_CONV) + (curr->data->price.cents);
    owedDollars = owedCents / CENT_CONV;
   
    while (owedCents > 0)
    {
        owedDollars = (double) owedCents / CENT_CONV;
        strcpy(prompt, "");
        printf("You still need to give us $%.2f: ", owedDollars);
        if(getInteger(&inserted, COIN_INPUT_LENGTH, prompt, 0, MAX_CENT) == RTM) {
            giveChange(system, inserted_total, 0);
            printf("Cancelling 'purchase item' at user's request\n");
            return FALSE;
        }
        if(insertCoin(system, inserted)){
           owedCents -= inserted;
        }
        inserted_total += inserted;
    }

    /* pass last inserted coin, calculate and return change */
    giveChange(system, inserted, owedDollars * CENT_CONV);

    printf("Thank you. Here is your %s\n", curr->data->name);
    curr->data->on_hand--;

    return TRUE;
}

BOOLEAN insertCoin(struct ppd_system * system, int coin) {

    /* check coin type is valid */
    /* using cent values */
    switch(coin)
    {
        case TEN_DOLLARS:
            system->cash_register[0].count++;
            break;
        case FIVE_DOLLARS:
            system->cash_register[1].count++;
            break;
        case TWO_DOLLARS:
            system->cash_register[2].count++;
            break;
        case ONE_DOLLAR:
            system->cash_register[3].count++;
            break;
        case FIFTY_CENTS:
            system->cash_register[4].count++;
            break;
        case TWENTY_CENTS:
            system->cash_register[5].count++;
            break;
        case TEN_CENTS:
            system->cash_register[6].count++;
            break;
        case FIVE_CENTS:
            system->cash_register[7].count++;
            break;
        default:
        /* coin doesn't match denomination types */
        fprintf(stderr,
        "\nError: $%.2f is not a valid denomination of money. Please try again.\n",
        (double) coin / CENT_CONV);
        return FALSE;
    }

return TRUE;

}

BOOLEAN giveChange(struct ppd_system * system, int coin, int balance) {
    
    int i, change, tot_change, error;
    int denoms[] = {1000, 500, 200, 100, 50, 20, 10, 5};
    int change_array[] = {0, 0, 0, 0, 0, 0, 0, 0};
 
    tot_change = coin - balance;
    change = tot_change;
    /* when error is 0 there are no coins left */
    error = NUM_DENOMS;
    
    /* cycle through available change */
    while(change > 0) {
        /* keep track of change */
        for (i = 0; i < NUM_DENOMS; i++) {
            if((change >= denoms[i]) && (system->cash_register[i].count > 0)) {
                change_array[i]++;
                change -= denoms[i];
                system->cash_register[i].count--;
                i = NUM_DENOMS - 1;
            }
        }
        /* return error if not enough change availale */
        error = NUM_DENOMS;

        for (i = 0; i < NUM_DENOMS; i++) {
            if(system->cash_register[i].count == 0) {
                error--;
            }
            if (error == 0) {
                /* no coins left */
                fprintf(stderr, "no available change, see administrator\n");
                return FALSE;
            }
        }
    }

    /* inform user of total change */
    if (tot_change >= ONE_DOLLAR) {
        printf("Your change is $%.2f: ", (double) tot_change / 100);
    }
    else {
        printf("Your change is %.2fc: ", (double) tot_change / 100);
    }
    /* specific coins */
    for (i = 0; i < NUM_DENOMS; i++) {
        if (change_array[i] > 0) {
            if (i <= CENTS_LOC) {
                printf("$%.2f ", (double) denoms[i] / 100);
                if (change_array[i] > 1)
                    printf("x %d ", change_array[i]);
            }
            else {
                printf("%dc ", denoms[i]);
                if(change_array[i] > 1)
                    printf("x %d ", change_array[i]);
            }
        }
    }
    printf("\n");

    return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a save succeeds and false when it does not
 **/
BOOLEAN save_system(struct ppd_system * system)
{
    const char * coins_name, * stock_name;
    FILE * fpCoins, * fpStock;

    char id[IDLEN + 1], name[NAMELEN + 1], desc[DESCLEN + 1];
    unsigned dollars, cents, onhand;

    int i, denomination, count;

    StockNode *curr;

    coins_name = system->coin_file_name;
    stock_name = system->stock_file_name;

    /* Open files for writing */
    if((fpCoins = fopen(coins_name, "w+")) == NULL)
    {
        perror("failed to open file");
        exit(EXIT_FAILURE);
    }
    if((fpStock = fopen(stock_name, "w+")) == NULL)
    {
        perror("failed to open file");
        exit(EXIT_FAILURE);
    }

    /* Write to stock */
    curr = system->item_list->head;

    while (curr != NULL) {
        /* Pull values from link list */
        strcpy(id, curr->data->id);
        strcpy(name, curr->data->name);
        strcpy(desc, curr->data->desc);
        dollars = curr->data->price.dollars;
        cents = curr->data->price.cents;
        onhand = curr->data->on_hand;
        /* Print to new line */
        fprintf(fpStock, "%s|%s|%s|%u.%u|%u\n", id, 
            name, desc, dollars, cents, onhand);

        curr = curr->next;
    }

    /* Write to coins */
    for (i = 0; i < NUM_DENOMS; i++) {
        denomination = system->cash_register[i].denom;
        count = system->cash_register[i].count;
        fprintf(fpCoins, "%d,%d\n", denomination, count);
    }

    fclose(fpCoins);
    fclose(fpStock);
    
    return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when adding an item succeeds and false when it does not
 **/
BOOLEAN add_item(struct ppd_system * system)
{
    int item_count, i, delim_count = 0;
    char id[IDLEN + 1], name[NAMELEN + 1], desc[DESCLEN + 1];
    char prompt[PROMPT_LENGTH + 1];
    unsigned dollars;
    unsigned cents;
    char priceStr[10];
    char * token, * pEnd;
    BOOLEAN valid_input;

    StockNode *curr, *new_node, *previous;
    
    item_count = system->item_list->count;

    /* establish item id based on preexisting inventory */
    item_count = system->item_list->count;
    
    /* adjust printing based on id number */
    if (item_count < 10)
        sprintf(id, "I000%d", item_count + 1);
    else if (item_count < 100)
        sprintf(id, "I00%d", item_count + 1);
    else if (item_count < 1000)
        sprintf(id, "I0%d", item_count + 1);
    else
        sprintf(id, "I%d", item_count + 1);

    printf("The id of the new stock will be: %s\n", id);

    /* ASK USER FOR INPUT ON ITEM VALUES */
    strcpy(prompt, "Enter the item name: ");
    if(getString(name, NAMELEN, prompt) == RTM) {
        printf("Cancelling 'add item' at user's request\n");
        return FALSE;
    }

    strcpy(prompt, "Enter the item description: ");
    if(getString(desc, DESCLEN, prompt) == RTM) {
        printf("Cancelling 'add item' at user's request\n");
        return FALSE;
    }
    
    /* receive and validate item price */
    valid_input = FALSE;
    while(!valid_input)
    {
        strcpy(prompt, "Enter the price for this item: ");
        if(getString(priceStr, STRING_INPUT_LENGTH, prompt) == RTM) {
            printf("Cancelling 'add item' at user's request\n");
            return FALSE;
        }
        /* confirm valid input */
        if(!strtod(priceStr, &pEnd)) {
            fprintf(stderr, "Error: the price is not valid.\n");
        }
        else {
            valid_input = TRUE;
        }
        /* confirm double deliminator */
        for (i = 1; i < (strlen(priceStr) - 1); i++) {
            if (priceStr[i] == '.')  {
                delim_count++;
            }
        }

        if (delim_count != 1) {
            valid_input = FALSE;
            fprintf(stderr, "Error: the price is not valid.\n");
        }
    }
    
        /* convert string to correct format correctly */
        token = strtok(priceStr, stock_delim);
        dollars = strtol(token, &pEnd, 10);
        token = strtok(NULL, stock_delim);
        if(token != NULL) {
            cents = strtol(token, &pEnd, 10);
        }
        else {
            cents = 0;
        }

    /* iterate to end of list and add newly entered information */
    curr = system->item_list->head;
    new_node = NULL;
    previous = NULL;

    /* allocate memory for new item */
    if ((new_node = malloc(sizeof(StockNode))) == NULL)
    {
        fprintf(stderr, "Could not allocate %lu bytes.\n", sizeof(StockNode));
        return FALSE;
    }

    if ((new_node->data = malloc(sizeof(StockData))) == NULL)
    {
        fprintf(stderr, "Could not allocate %lu bytes.\n", sizeof(StockData));
        return FALSE;
    }

    /* copy user inputed information to node */
    strcpy(new_node->data->id, id);
    strcpy(new_node->data->name, name);
    strcpy(new_node->data->desc, desc);
    new_node->data->price.dollars = dollars;
    new_node->data->price.cents = cents;
    new_node->data->on_hand = DEFAULT_STOCK_LEVEL;
    new_node->next = NULL;

    /* place node in list */
    while (curr != NULL)
    {   
        /* search for clear node to place data */
        previous = curr;
        curr = curr->next;
    }
    if(curr == NULL)
    {
        /* add to the end of the list */
        previous->next = new_node;
    }
    else
    {
        /* insert in the middle of the list */
        previous->next = new_node;
        new_node->next = curr;
    }

    system->item_list->count++;

    sort_list(system);
    
    printf("\nThe item \"%s - %s\" has now been added to the menu.\n", name, desc);

    return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when removing an item succeeds and false when it does not
 **/
BOOLEAN remove_item(struct ppd_system * system)
{
    char id[IDLEN + 1];
    char prompt[PROMPT_LENGTH + 1];
    StockNode *curr, *previous, *remove;
    StockData * data = NULL;

    printf("Enter the item id of the item to remove from the menu: ");
    
    if(getString(id, STRING_INPUT_LENGTH, prompt) == RTM) {
        printf("Cancelling 'remove item' at user's request\n");
        return FALSE;
    }
    
    curr = system->item_list->head;
    /* search through list to find matching id */
    while(curr != NULL && strcmp(curr->data->id, id) != 0)
    {
        previous = curr;
        curr = curr->next;
    }
    /* item not found */
    if(curr == NULL || strcmp(curr->data->id, id) != 0)
    {
        /* item isn't in the list */
        fprintf(stderr, "Error: the item was not found.\n");
        return FALSE;
    }
    /* item to be deleted located at beginning of the list */
    if(previous == NULL)
    {
        data = system->item_list->head->data;
        remove = system->item_list->head;
        system->item_list->head = system->item_list->head->next;
    }
    else
    {
        /* delete from elsewhere, end included */
        remove = curr;
        data = remove->data;
        previous->next = curr->next;
    }

    /* inform user */
    printf("\n%s - %s - %s has been removed from the system.\n\n", 
        remove->data->id, remove->data->name, remove->data->desc);
    
    /* free the data and reduce list size */
    free(remove);
    system->item_list->count--;
        
    return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_stock(struct ppd_system * system)
{
    StockNode * curr;

    curr = system->item_list->head;

    /* iterate through stock, updating stock level to default */
    while (curr != NULL) {
        curr->data->on_hand = DEFAULT_STOCK_LEVEL;
 
        curr = curr->next;
    }

    printf("All stock has been reset to the default level of %u\n",
            DEFAULT_STOCK_LEVEL);

    return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_coins(struct ppd_system * system)
{
    int i;
    
    /* iterate through coins, update count level to default */
    for (i = 0; i < NUM_DENOMS; i++) {
        system->cash_register[i].count = DEFAULT_COIN_COUNT;
    }

    printf("All coins have been reset to the default level of %u\n",
            DEFAULT_COIN_COUNT);

    return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail
 **/
BOOLEAN display_coins(struct ppd_system * system)
{
    int i;
    const char* denoms[] = {"10 dollar", "5 dollar", "2 dollar", 
                            "1 dollar", "50 cents", "20 cents", 
                            "10 cents", "5 cents"};
    
    printf("Coins Summary\n");
    printf("------------\n");
    printf("Denomination   |  Count\n");
    printf("-----------------------\n");
    
    /* cycle cash register and display denomination/amount */
    for (i = NUM_DENOMS - 1; i >= 0; i--)
    {  
        printf("%s%*s|", denoms[i], 15 - (int) strlen(denoms[i]), " ");
        printf("%*s%u\n", 4, " ", system->cash_register[i].count);
    }

    return TRUE;
}
