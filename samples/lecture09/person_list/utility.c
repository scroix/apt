#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void clear_input_buffer()
{
    int c;

    /* read until the end of the line or end-of-file */
    while ((c = fgetc(stdin)) != '\n' && c != EOF)
        ;

    /* clear the error and end-of-file flags */
    clearerr(stdin);
}

int get_integer(const char* prompt, int *integer)
{
    char buf[BUFSIZ] = {0};
    char *ptr;

    int result;

    while (1)
    {
        printf("%s", prompt);

        fgets(buf, BUFSIZ, stdin);

        if (buf[0] == '\n' || buf[0] == 0)
            return 0;

        /* Overwrite the \n character with \0. */
        buf[strlen(buf) - 1] = '\0';

        /* Convert string to an integer. */
        result = (int)strtol(buf, &ptr, 10);

        if (*ptr == '\0')
            break;

        fprintf(stderr, "Error: Input was not numeric.\n");
    }

    /* Make the result string available to calling function. */
    *integer = result;

    return 1;
}

int get_string(const char *prompt, char *string)
{
    char buf[BUFSIZ];

    while (1)
    {
        /* Provide a custom prompt. */
        printf("%s", prompt);

        fgets(buf, BUFSIZ, stdin);

        if (buf[0] == '\n' || buf[0] == 0)
            return 0;

        /* A string that doesn't have a newline character is too long. */
        if (buf[strlen(buf) - 1] != '\n')
        {
            printf("Input was too long.\n");
            clear_input_buffer();
        }
        else
            break;
    }

    /* Overwrite the \n character with \0. */
    buf[strlen(buf) - 1] = '\0';

    /* Make the result string available to calling function. */
    strcpy(string, buf);

    /* Note: There is no string array length check here */

    return 1;
}

