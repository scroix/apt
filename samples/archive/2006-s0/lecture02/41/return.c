/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 4/1/2006
* 
* return.c
* Simple example of using the return keyword.
* Citation: Adapted from slide 2-41 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

/*void sum(int a, int b, int* sum);*/

int main(void)
{
   int x, y, z;
   
   x = 10;
   y = 4;
   sum(x, y, &z);
   printf("%d + %d = %d\n", x, y, z);

   return EXIT_SUCCESS;
}

void sum (int a, int b, int* sum)
{
   *sum = a + b;
}
