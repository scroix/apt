/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 23/1/2006
* 
* calloc.c
* Example of calloc function.
* Citation: Adapted from slide 8-12 of PT lecture notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
   int* ptr;
   const int numItems = 10;
   int i;
   
   ptr = calloc(numItems, sizeof(int));
   
   if (ptr == NULL)
   {
      printf("Unable to get %d bytes.\n", numItems * sizeof(int));
      return EXIT_FAILURE;
   }
   

   for (i = 0; i < numItems; i++)
   {
      printf("Value at index %d is %d.\n", i, ptr[i]);
   }

   return EXIT_SUCCESS;
}

/* Task: Create a function that returns dynamically allocated memory. */
