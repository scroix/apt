/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 3/1/2006
* 
* students.c
* Calculates the overall score of a several students based upon an exam score
* and two assignment scores (each out of 100%). Scores can be redirected from
* a text file. This example is used to demonstrate the substitution of
* if/else statements with switch statements.
* Citation: Adapted from slide 1-15 of PP2A/PT course notes.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define ASS1_WORTH    20
#define ASS2_WORTH    30
#define EXAM_WORTH    50

int main(void)
{
   int ass1, ass2, exam;
   int ass1Sum = 0, ass2Sum = 0, examSum = 0, studentCount = 0;
   float total, totalSum = 0.0;
   char grade;

   printf("\nLine      Assignment Scores    Exam      Total\n");
   printf("Number      1          2       Score     Score\n");
   printf("======    =================    ======    =====\n");
   
   while (scanf("%d%d%d", &ass1, &ass2, &exam) == 3)  
   {
      studentCount++;

      total = ass1 * ASS1_WORTH / 100.0 +
              ass2 * ASS2_WORTH / 100.0 +
              exam * EXAM_WORTH / 100.0;

      if (total < 50.0)
      {
         grade = 'N';
      }
      else if (total < 60.0)
      {
         grade = 'P';
      }
      else if (total < 70.0)
      {
         grade = 'C';
      }
      else if (total < 80.0)
      {
         grade = 'D';
      }
      else
      {
         grade = 'H';
      }
      
      printf("     %d       %3d       %3d        %3d   %6.2f     %c",
             studentCount, ass1, ass2, exam, total, grade);
      
      if (grade == 'H' || grade == 'D')
      {
         printf(" <- good score!\n");
      }
      else if (grade == 'C' || grade == 'P')
      {
         printf(" <- ok score!\n");
      }
      else
      {
         printf(" <- bad score!\n");
      }
      
      /*switch (grade)
      {
         case 'H':
         case 'D':
            printf(" <- good score!\n");
            break;
         case 'C':
         case 'P':
            printf(" <- ok score!\n");
            break;
         case 'N':
            printf(" <- bad score!\n");
            break;
      }*/ /* Add default statement... */
         
      ass1Sum += ass1;
      ass2Sum += ass2;
      examSum += exam;
      totalSum += total;
   } /* End of while loop. */

   if (studentCount == 0)
   {
      printf("\n   Empty input file\n");
   }
   else  
   {
      printf("          ================    =======   ======\n");
      printf("Averages: %6.2f    %6.2f     %6.2f   %6.2f",
           ass1Sum / (float) studentCount,
           ass2Sum / (float) studentCount,
           examSum / (float) studentCount,
           totalSum / studentCount );
      printf("    Total no. of students = %d\n", studentCount);
   }
   
   return EXIT_SUCCESS;
}
