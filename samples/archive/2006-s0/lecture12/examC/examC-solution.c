/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 6/2/2006
* 
* examC-solution.c
* Sample exam question.
****************************************************************************/

#include "poker.h"

int handcmp(CardType hand1[HAND_SIZE], CardType hand2[HAND_SIZE]);

int main(void)
{
   /* This hand has "four of a kind". */
   CardType hand1[HAND_SIZE] = 
   {
      {1, "2", 'S'},
      {2, "2", 'C'},
      {3, "2", 'D'},
      {4, "2", 'H'},
      {5, "3", 'S'}
   };
   /* This hand has "three of a kind". */
   CardType hand2[HAND_SIZE] = 
   {
      {13, "5", 'S'},
      {14, "5", 'C'},
      {15, "5", 'D'},
      {17, "6", 'S'},
      {18, "6", 'C'}
   };
   /* This hand has "three of a kind". */
   CardType hand3[HAND_SIZE] = 
   {
      {21, "7", 'S'},
      {22, "7", 'C'},
      {23, "7", 'D'},
      {17, "6", 'S'},
      {18, "6", 'C'}
   };

   switch (handcmp(hand1, hand2))
   {
      case  1: 
         printf("1\n");  
         break;
      case -1: 
         printf("-1\n");  
         break;
      case  0: 
         printf("0\n"); 
         break;
   }
   
   switch (handcmp(hand2, hand1))
   {
      case  1: 
         printf("1\n");  
         break;
      case -1: 
         printf("-1\n");  
         break;
      case  0: 
         printf("0\n"); 
         break;
   }

   switch (handcmp(hand2, hand3))
   {
      case  1: 
         printf("1\n");  
         break;
      case -1: 
         printf("-1\n");  
         break;
      case  0: 
         printf("0\n"); 
         break;
   }

   return EXIT_SUCCESS;
}


const char* pips[NUM_PIPS] = { "2", "3",  "4", "5", "6", "7", "8", 
                               "9", "10", "J", "Q", "K", "A" };

/* MARK ALLOCATION FOR handcmp() FUNCTION (28 marks): */

/* 1 mark. */
int handcmp(CardType hand1[HAND_SIZE], CardType hand2[HAND_SIZE])
{
   /* 4 marks. */
   int hand1Pips[NUM_PIPS] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
   int hand2Pips[NUM_PIPS] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
   int i, j, hand1Value = 0, hand2Value = 0;
   
   /* Populate hand1Pips and hand2Pips arrays. */
   /* 3 + 3 + 2 + 2 = 10 marks. */
   for (i = 0; i < HAND_SIZE; i++)
   {
      for (j = 0; j < NUM_PIPS; j++)
      {
         if (strcmp(hand1[i].pip, pips[j]) == 0)
         {
            hand1Pips[j] += 1;
         }
         if (strcmp(hand2[i].pip, pips[j]) == 0)
         {
            hand2Pips[j] += 1;
         }
      }
   }
   
   /* Assign a weight to each hand. */
   /* 3 + 2 + 2 = 7 marks. */
   for (i = 0; i < NUM_PIPS; i++)
   {
      if (hand1Value < hand1Pips[i])
      {
         hand1Value = hand1Pips[i];
      }
      if (hand2Value < hand2Pips[i])
      {
         hand2Value = hand2Pips[i];
      }
   }
   
   /* Process return value. */
   /* 2 + 2 + 2 = 6 marks */
   if (hand1Value > hand2Value)
   {
      return 1;
   }
   else if (hand1Value < hand2Value)
   {
      return -1;
   }
   else
   {
      return 0;
   }
}
