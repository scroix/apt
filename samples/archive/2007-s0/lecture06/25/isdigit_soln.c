/****************************************************************************
* Steven Burrows
* sdb@cs.rmit.edu.au
* 16/1/2006
* 
* isdigit.c
* Example of implementing a macro.
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define MY_ISDIGIT(c) c >= '0' && c <= '9' ? 1 : 0

int main(void)
{
   char var1 = 'a';
   char var2 = '5';
   
   if (MY_ISDIGIT(var1) == TRUE)
   {
      printf("var1 is a digit.\n");
   }
   else
   {
      printf("var1 is NOT a digit.\n");
   }
   
   if (MY_ISDIGIT(var2) == TRUE)
   {
      printf("var2 is a digit.\n");
   }
   else
   {
      printf("var2 is NOT a digit.\n");
   }
   
   return EXIT_SUCCESS;
}

