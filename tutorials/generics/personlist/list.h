#include <assert.h>
#include <stdlib.h>
#include "safemalloc.h"


struct list_node
{
    void * data; /* struct person * data; */
    struct list_node * next;
};

struct list
{
    struct list_node * head;
    unsigned count;
};

int (*cmp)(void*, void*) = person_cmp;

void list_init(struct list * list);
BOOLEAN list_add(struct list * list, void * data/* struct person * data */);
void list_free(struct list * list);
int person_cmp(void*, void*); /* int person_cmp(const struct person*,const struct person*); */
