#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

struct list {
    int data;
    struct list *next;
};

struct list *list_insert(struct list *list, int data)
{
    struct list *curr;
    struct list *prev;
    struct list *node;

    node = malloc(sizeof(struct list));
    assert(node);

    node->data = data;
    node->next = NULL;

    if (list == NULL) /* empty list */
        return node;

    prev = NULL;
    curr = list;

    while (curr != NULL && curr->data < node->data) {
        prev = curr;
        curr = curr->next;
    }

    if (prev == NULL) /* we are still at the head node */
        list = node;
    else /* in middle somewhere or at end */
        prev->next = node;

    node->next = curr;

    return list;
}

void list_free(struct list *list)
{
    struct list *next;

    assert(list);

    while (list != NULL) {
        next = list->next;

        free(list);

        list = next;
    }
}

void print(int data)
{
    printf("%d ", data);
}

void list_foreach(struct list *list, void (*fn)(int))
{
    assert(list);

    while (list) {
        fn(list->data);

        list = list->next;
    }

    printf("\n");
}

int main(int argc, char **argv)
{
    struct list *list = NULL;
    int i;

    if (argc < 2) {
        fprintf(stderr, "usage: %s <integer1 integer2 ...>\n", argv[0]);
        return EXIT_SUCCESS;
    }

    for (i = 1; i < argc; i++)
        list = list_insert(list, atoi(argv[i]));

    list_foreach(list, print);

    list_free(list);

    return EXIT_SUCCESS;
}
